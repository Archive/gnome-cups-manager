msgid ""
msgstr ""
"Project-Id-Version: pl\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-02-19 20:53+0100\n"
"PO-Revision-Date: 2007-02-19 21:44+0100\n"
"Last-Translator: wadim dziedzic <nikdo@aviary.pl>\n"
"Language-Team: POLISH <pl@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#: ../gnome-cups-add/add-printer.c:154
msgid "Couldn't add printer"
msgstr "Nie można dodać drukarki"

#: ../gnome-cups-add/add-printer.c:350
msgid "Print version and exit"
msgstr "Wyświetl wersję i zakończ"

#: ../gnome-cups-add/add-printer.c:352
msgid "CUPS Printer URI"
msgstr "Adres URI drukarki CUPS"

#: ../gnome-cups-add/add-printer.c:374
#: ../gnome-cups-add/gnome-cups-add.glade.h:1
msgid "Add a Printer"
msgstr "Dodawanie drukarki"

#: ../gnome-cups-add/gnome-cups-add.glade.h:2
msgid "Step 1 of 2: Printer Connection"
msgstr "Krok 1 z 2: połączenie drukarki"

#: ../gnome-cups-add/gnome-cups-add.glade.h:3
msgid "Step 2 of 2: Printer Driver"
msgstr "Krok 2 z 2: sterownik drukarki"

#: ../gnome-cups-add/gnome-cups-add.glade.h:4
msgid "This assistant helps you set up a printer."
msgstr "Niniejszy asystent pomaga w ustawieniu drukarki."

#: ../gnome-cups-manager/eggtrayicon.c:118
msgid "Orientation"
msgstr "Położenie"

#: ../gnome-cups-manager/eggtrayicon.c:119
msgid "The orientation of the tray."
msgstr "Położenie obszaru powiadamiania."

#: ../gnome-cups-manager.desktop.in.in.h:1
msgid "Configure your printers"
msgstr "Konfiguracja drukarek"

#: ../gnome-cups-manager.desktop.in.in.h:2
msgid "Printing"
msgstr "Drukowanie"

#: ../gnome-cups-manager/gnome-cups-icon.c:72
msgid "Could not start the printer tray icon, because the CUPS server could not be contacted."
msgstr "Nie można pokazać ikony powiadamiania z powodu braku połączenia z serwerem CUPS."

#: ../gnome-cups-manager/gnome-cups-icon.c:129
msgid "Print Monitor"
msgstr "Monitor wydruku"

#: ../gnome-cups-manager/gnome-cups-manager.c:61
msgid "The CUPS server could not be contacted."
msgstr "Nie można połączyć się z serwerem CUPS."

#: ../gnome-cups-manager/gnome-cups-manager.c:97
#, c-format
msgid "Printer not found: %s"
msgstr "Nie znaleziono drukarki: %s"

#: ../gnome-cups-manager/gnome-cups-manager.c:211
msgid "Show printer properties for printers listed on the command line"
msgstr "Pokazuje własności drukarek podanych w linii poleceń"

#: ../gnome-cups-manager/gnome-cups-manager.c:214
msgid "View the queues of printers listed on the command line"
msgstr "Wyświetla kolejki drukarek podanych w linii poleceń"

#: ../gnome-cups-manager/gnome-cups-manager.c:217
msgid "[PRINTER...]"
msgstr "[DRUKARKA...]"

#: ../gnome-cups-manager/gnome-cups-manager.c:239
msgid "Printers View"
msgstr "Drukarki"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:1
msgid "    "
msgstr "    "

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:2
msgid "<b>Layout</b>"
msgstr "<b>Układ</b>"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:3
msgid "<b>Media</b>"
msgstr "<b>Media</b>"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:4
msgid "Advanced"
msgstr "Zaawansowane"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:5
msgid "Become _Administrator"
msgstr "Zaloguj jako _administrator"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:6
msgid "Cancel Jobs"
msgstr "Anuluj zadania"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:7
msgid "General"
msgstr "Ogólne"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:8
msgid "Make _Default"
msgstr "Uczyń _domyślną"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:9
msgid "Paper"
msgstr "Papier"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:10
msgid "Paper _type:"
msgstr "_Typ papieru:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:11
msgid "Paper si_ze:"
msgstr "_Rozmiar papieru:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:12
msgid "Pause Jobs"
msgstr "Wstrzymaj zadania"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:13
msgid "Pause Printer"
msgstr "Wstrzymaj drukarkę"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:14
msgid "Print _Test Page"
msgstr "Drukuj stronę _testową"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:15
#: ../libgnomecups/gnome-cups-ui-connection.c:979
msgid "Printer"
msgstr "Drukarka"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:16
msgid "Printer Properties"
msgstr "Właściwości drukarki"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:17
msgid "Printers"
msgstr "Drukarki"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:18
msgid "Ready or something"
msgstr "Gotowa"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:19
msgid "Resume Jobs"
msgstr "Wznów zadania"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:20
msgid "Set as Default"
msgstr "Ustaw jako domyślną"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:21
msgid "Status:"
msgstr "Status:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:22
msgid "_Add Printer"
msgstr "_Dodaj drukarkę"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:23
msgid "_Binding:"
msgstr "_Powiązanie:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:24
msgid "_Close"
msgstr "_Zamknij"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:25
msgid "_Description:"
msgstr "_Opis:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:26
msgid "_Double Sided:"
msgstr "D_wustronnie:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:27
msgid "_Edit"
msgstr "_Edycja"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:28
msgid "_Location:"
msgstr "_Położenie:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:29
msgid "_Name:"
msgstr "_Nazwa:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:30
msgid "_Orientation:"
msgstr "_Orientacja:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:31
msgid "_Pause"
msgstr "W_strzymaj"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:32
msgid "_Print a Test Page"
msgstr "_Drukuj stronę testową"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:33
msgid "_Printer"
msgstr "_Drukarka"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:34
msgid "_Properties"
msgstr "_Właściwości"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:35
msgid "_Remove Printer"
msgstr "_Usuń drukarkę"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:36
msgid "_Resolution:"
msgstr "_Rozdzielczość:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:37
msgid "_Resume"
msgstr "W_znów"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:38
msgid "_Source:"
msgstr "Ź_ródło:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:39
msgid "_Status"
msgstr "_Status"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:40
msgid "dummy"
msgstr "tymczasowy"

#: ../gnome-cups-manager/Gnome_CupsManager.server.in.h:1
msgid "Printer Icon"
msgstr "Ikona drukarki"

#: ../gnome-cups-manager/Gnome_CupsManager.server.in.h:2
msgid "Printer View"
msgstr "Widok drukarki"

#: ../gnome-cups-manager/gnome-printer-list.c:96
msgid "New Printer"
msgstr "Nowa drukarka"

#: ../gnome-cups-manager/gnome-printer-list.c:384
msgid "Jobs"
msgstr "Zadania"

#: ../gnome-cups-manager/gnome-printer-list.c:395
#: ../gnome-cups-manager/view-queue.c:523
msgid "Pause"
msgstr "Wstrzymaj"

#: ../gnome-cups-manager/gnome-printer-list.c:403
#: ../gnome-cups-manager/view-queue.c:530
msgid "Resume"
msgstr "Wznów"

#: ../gnome-cups-manager/gnome-printer-list.c:411
msgid "Make Default"
msgstr "Uczyń domyślną"

#: ../gnome-cups-manager/printer-properties.c:598
msgid "Driver"
msgstr "Sterownik"

#: ../gnome-cups-manager/printer-properties.c:646
msgid "Connection"
msgstr "Połączenie"

#: ../gnome-cups-manager/tray.c:47
msgid "Could not launch printer view"
msgstr "Nie można otworzyć widoku drukarek"

#: ../gnome-cups-manager/tray.c:67
msgid "Could not launch printer properties"
msgstr "Nie można otworzyć właściwości drukarek"

#: ../gnome-cups-manager/tray.c:91
#: ../gnome-cups-manager/tray.c:99
#: ../gnome-cups-manager/view-queue.c:276
#: ../gnome-cups-manager/view-queue.c:284
msgid "Could not resume printer"
msgstr "Nie można wznowić drukarki"

#: ../gnome-cups-manager/tray.c:130
#: ../gnome-cups-manager/view-queue.c:243
msgid "_Resume Printer"
msgstr "_Wznów drukarkę"

#: ../gnome-cups-manager/tray.c:132
#: ../gnome-cups-manager/view-queue.c:245
msgid "P_ause Printer"
msgstr "W_strzymaj drukarkę"

#: ../gnome-cups-manager/tray.c:199
#, c-format
msgid "%s - %s"
msgstr "%s - %s"

#: ../gnome-cups-manager/view-queue.c:57
#, c-format
msgid "%.1f K"
msgstr "%.1f K"

#: ../gnome-cups-manager/view-queue.c:61
#, c-format
msgid "%.1f MB"
msgstr "%.1f MB"

#: ../gnome-cups-manager/view-queue.c:65
#, c-format
msgid "%.1f GB"
msgstr "%.1f GB"

#: ../gnome-cups-manager/view-queue.c:69
#, c-format
msgid "1 byte"
msgid_plural "%lu bytes"
msgstr[0] "1 bajt"
msgstr[1] "%lu bajty"
msgstr[2] "%lu bajtów"

#: ../gnome-cups-manager/view-queue.c:80
#, c-format
msgid "%d of %d"
msgstr "%d z %d"

#: ../gnome-cups-manager/view-queue.c:303
#, c-format
msgid "Could not set %s as the default"
msgstr "Nie można ustawić %s jako domyślną"

#: ../gnome-cups-manager/view-queue.c:400
msgid "Could not pause job"
msgstr "Nie można wstrzymać zadania"

#: ../gnome-cups-manager/view-queue.c:433
msgid "Could not resume job"
msgstr "Nie można wznowić zadania"

#: ../gnome-cups-manager/view-queue.c:465
msgid "Could not cancel job"
msgstr "Nie można anulować zadania"

#: ../gnome-cups-manager/view-queue.c:541
msgid "Cancel"
msgstr "Anuluj"

#: ../gnome-cups-manager/view-queue.c:615
msgid "Name"
msgstr "Nazwa"

#: ../gnome-cups-manager/view-queue.c:624
msgid "Job Number"
msgstr "Numer zadania"

#: ../gnome-cups-manager/view-queue.c:632
msgid "Owner"
msgstr "Właściciel"

#: ../gnome-cups-manager/view-queue.c:641
msgid "Size"
msgstr "Rozmiar"

#: ../gnome-cups-manager/view-queue.c:649
msgid "State"
msgstr "Stan"

#: ../icons/emblem-default.icon.in.h:1
msgid "Default"
msgstr "Domyślna"

#: ../icons/emblem-paused.icon.in.h:1
msgid "Paused"
msgstr "Wstrzymana"

#: ../libgnomecups/gnome-cups-permission.c:102
#, c-format
msgid ""
"<b>Problems launching %s as root via %s</b>\n"
"\t<small>%s</small>"
msgstr ""
"<b>Problem podczas uruchamiania %s jako root poprzez %s</b>\n"
"\t<small>%s</small>"

#: ../libgnomecups/gnome-cups-ui-connection.c:775
#, c-format
msgid "Identity and Password for %s in workgroup %s"
msgstr "Tożsamość i hasło dla %s w grupie roboczej %s"

#: ../libgnomecups/gnome-cups-ui-connection.c:778
#, c-format
msgid "Identity and Password for %s"
msgstr "Tożsamość i hasło dla %s"

#: ../libgnomecups/gnome-cups-ui-connection.c:781
#, c-format
msgid "Identity and Password for workgroup %s"
msgstr "Tożsamość i hasło dla grupy roboczej %s"

#: ../libgnomecups/gnome-cups-ui-connection.c:785
msgid "Identity and Password"
msgstr "Tożsamość i hasło"

#: ../libgnomecups/gnome-cups-ui-connection.c:789
msgid "Authentication Required"
msgstr "Wymagane uwierzytelnienie"

#: ../libgnomecups/gnome-cups-ui-connection.c:1016
msgid "<i>No printers detected</i>"
msgstr "<i>Nie wykryto żadnych drukarek</i>"

#: ../libgnomecups/gnome-cups-ui-connection.c:1091
msgid ""
"For example :\n"
"\thttp://hostname:631/ipp/\n"
"\thttp://hostname:631/ipp/port1\n"
"\tipp://hostname/ipp/\n"
"\tipp://hostname/ipp/port1"
msgstr ""
"Na przykład:\n"
"\thttp://nazwahosta:631/ipp/\n"
"\thttp://nazwahosta:631/ipp/port1\n"
"\tipp://nazwahosta/ipp/\n"
"\tipp://nazwahosta/ipp/port1"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:1
msgid "9100"
msgstr "9100"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:2
msgid "<b>P_assword:</b>"
msgstr "<b>H_asło:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:3
msgid "<b>Printer Type:</b>"
msgstr "<b>Typ drukarki:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:4
msgid "<b>_Host:</b>"
msgstr "<b>_Host:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:5
msgid "<b>_Port:</b>"
msgstr "<b>_Port:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:6
msgid "<b>_Printer:</b>"
msgstr "<b>_Drukarka:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:7
msgid "<b>_Printers:</b>"
msgstr "<b>_Drukarki:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:8
msgid "<b>_Queue:</b>"
msgstr "<b>_Kolejka:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:9
msgid "<b>_URI:</b>"
msgstr "<b>_URI:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:10
msgid "<b>_Username:</b>"
msgstr "<b>_Nazwa użytkownika:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:11
msgid "<b>_Workgroup:</b>"
msgstr "<b>_Grupa robocza:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:12
msgid ""
"CUPS Printer (IPP)\n"
"Windows Printer (SMB)\n"
"UNIX Printer (LPD)\n"
"HP JetDirect"
msgstr ""
"Drukarka CUPS (IPP)\n"
"Drukarka Windows (SMB)\n"
"Drukarka UNIX (LPD)\n"
"HP JetDirect"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:16
msgid "Printer _Port:"
msgstr "_Port drukarki:"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:17
msgid "Use _another printer by specifying a port:"
msgstr "_Użyj innej drukarki na porcie:"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:18
msgid "_Local Printer"
msgstr "Drukarka l_okalna"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:19
msgid "_Network Printer"
msgstr "Drukarka _sieciowa"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:20
msgid "_Scan"
msgstr "_Wyszukaj"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:21
msgid "_Use a detected printer:"
msgstr "_Użyj wykrytej drukarki:"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:22
msgid "hp"
msgstr "hp"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:23
msgid "ipp"
msgstr "ipp"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:24
msgid "local"
msgstr "local"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:25
msgid "lpd"
msgstr "lpd"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:26
msgid "smb"
msgstr "smb"

#: ../libgnomecups/gnome-cups-ui-driver.c:292
msgid "Generic"
msgstr "Zwykła"

#: ../libgnomecups/gnome-cups-ui-driver.c:312
msgid "High Quality Image (GIMP-Print Inkjet)"
msgstr "Obraz wysokiej jakości (GIMP-Print Inkjet)"

#: ../libgnomecups/gnome-cups-ui-driver.c:319
#: ../libgnomecups/gnome-cups-ui-driver.c:347
msgid "High Quality Image (GIMP-Print)"
msgstr "Obraz wysokiej jakości (GIMP-Print)"

#: ../libgnomecups/gnome-cups-ui-driver.c:332
#: ../libgnomecups/gnome-cups-ui-driver.c:362
msgid "Standard (CUPS)"
msgstr "Standardowy (CUPS)"

#: ../libgnomecups/gnome-cups-ui-driver.c:344
#, c-format
msgid "High Quality Image (GIMP-Print) (%s)"
msgstr "Obraz wysokiej jakości (GIMP-Print) (%s)"

#: ../libgnomecups/gnome-cups-ui-driver.c:358
msgid "Standard"
msgstr "Standardowy"

#: ../libgnomecups/gnome-cups-ui-driver.c:476
#: ../libgnomecups/gnome-cups-ui-driver.c:695
msgid "(Suggested)"
msgstr "(zalecany)"

#: ../libgnomecups/gnome-cups-ui-driver.c:806
msgid "Select a PPD File"
msgstr "Wybór pliku PPD"

#: ../libgnomecups/gnome-cups-ui-driver.c:817
msgid "All Files"
msgstr "Wszystkie pliki"

#: ../libgnomecups/gnome-cups-ui-driver.c:822
msgid "PPD Files"
msgstr "Pliki PPD"

#: ../libgnomecups/gnome-cups-ui-driver.c:847
msgid "Only files ending with .ppd or .ppd.gz will be installed"
msgstr "Zainstalowane zostaną tylko pliki z rozszerzeniem .ppd lub .ppd.gz."

#: ../libgnomecups/gnome-cups-ui-driver.c:853
#, c-format
msgid "CUPS is installed differently than expected.  There is no directory '%s'"
msgstr "CUPS jest zainstalowany w inny sposób. Brak katalogu '%s'"

#: ../libgnomecups/gnome-cups-ui-driver.c:883
#, c-format
msgid ""
"The PPD\n"
"\t<b>%s</b>\n"
"is already installed"
msgstr ""
"PPD\n"
"\t<b>%s</b>\n"
"jest już zainstalowany"

#: ../libgnomecups/gnome-cups-ui-driver.c:905
#, c-format
msgid ""
"Unable to write to\n"
"\t<b>%s</b>\n"
"because %s"
msgstr ""
"Nie można zapisać do\n"
"\t<b>%s</b>\n"
"z powodu %s"

#: ../libgnomecups/gnome-cups-ui-driver.c:1018
msgid "Model"
msgstr "Model"

#: ../libgnomecups/gnome-cups-ui-driver.glade.h:1
msgid "<b>Description:</b>"
msgstr "<b>Opis:</b>"

#: ../libgnomecups/gnome-cups-ui-driver.glade.h:2
msgid "<b>M_anufacturer:</b>"
msgstr "<b>_Producent:</b>"

#: ../libgnomecups/gnome-cups-ui-driver.glade.h:3
msgid "<b>_Driver:</b>"
msgstr "<b>_Sterownik:</b>"

#: ../libgnomecups/gnome-cups-ui-driver.glade.h:4
msgid "<b>_Model:</b>"
msgstr "<b>_Model:</b>"

#: ../libgnomecups/gnome-cups-ui-driver.glade.h:5
msgid "_Install Driver..."
msgstr "_Instaluj sterownik..."

#: ../libgnomecups/gnome-cups-ui-driver.glade.h:6
msgid "make and model"
msgstr "producent i model"

#: ../libgnomecups/gnome-cups-ui-init.c:47
msgid "Password"
msgstr "Hasło"

#: ../libgnomecups/gnome-cups-ui-init.c:55
msgid "Enter a username and password to modify this printer:"
msgstr "Proszę podać użytkownika i hasło do modyfikacji tej drukarki:"

#: ../libgnomecups/gnome-cups-ui-init.c:63
msgid "Username: "
msgstr "Użytkownik:"

#: ../libgnomecups/gnome-cups-ui-init.c:75
msgid "Password: "
msgstr "Hasło:"

#: ../libgnomecups/gnome-cups-ui-print.c:32
#, c-format
msgid ""
"Printing to '%s' failed with error code: %d\n"
"is the printer paused ?"
msgstr ""
"Drukowanie do '%s' nie powiodło się. Kod: %d\n"
"Czy drukarka jest wstrzymana?"

#: ../libgnomecups/gnome-cups-ui-util.c:27
msgid "Unknown Error"
msgstr "Nieznany błąd"

#: ../libgnomecups/gnome-cups-ui-util.c:88
msgid "Test Page"
msgstr "Strona testowa"

#: ../libgnomecups/gnome-cups-ui-util.c:97
#, c-format
msgid "%s test page has been sent to %s."
msgstr "Strona testowa %s została wysłana do %s."

