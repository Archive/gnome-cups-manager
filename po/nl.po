# Dutch translation of gnome-cups-manager.
# Copyright (C) 2003 THE gnome-cups-manager'S COPYRIGHT HOLDER
# This file is distributed under the same license as the 
# gnome-cups-manager package.
#
# Vincent van Adrighem <adrighem@gnome.org>, 2003.
# Tino Meinen <a.t.meinen@chello.nl>, 2004, 2005, 2006
# Wouter Bolsterlee <wbolster@gnome.org>, 2006.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-cups-manager cvs\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-11-19 21:42+0100\n"
"PO-Revision-Date: 2006-11-19 21:41+0100 \n"
"Last-Translator: Wouter Bolsterlee <wbolster@gnome.org>\n"
"Language-Team: Dutch <vertaling@vrijschrift.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../gnome-cups-add/add-printer.c:154
msgid "Couldn't add printer"
msgstr "Kon printer niet toevoegen"

# afdrukken zou hier verwarrend zijn (omdat het een afdrukbeheerprogramma is)
#: ../gnome-cups-add/add-printer.c:343
msgid "Print version and exit"
msgstr "Versie tonen en afsluiten"

#: ../gnome-cups-add/add-printer.c:345
msgid "CUPS Printer URI"
msgstr "CUPS printer URI"

#: ../gnome-cups-add/add-printer.c:362
#: ../gnome-cups-add/gnome-cups-add.glade.h:1
msgid "Add a Printer"
msgstr "Printer toevoegen"

#: ../gnome-cups-add/gnome-cups-add.glade.h:2
msgid "Step 1 of 2: Printer Connection"
msgstr "Stap 1 van 2: Printerverbinding"

#: ../gnome-cups-add/gnome-cups-add.glade.h:3
msgid "Step 2 of 2: Printer Driver"
msgstr "Stap 2 van 2: Printer stuurprogramma"

#: ../gnome-cups-add/gnome-cups-add.glade.h:4
msgid "This assistant helps you set up a printer."
msgstr "Deze assistent helpt u bij het instellen van een printer."

#: ../gnome-cups-manager/eggtrayicon.c:118
msgid "Orientation"
msgstr "Oriëntatie"

#: ../gnome-cups-manager/eggtrayicon.c:119
msgid "The orientation of the tray."
msgstr "De oriëntatie van de papierbak"

#: ../gnome-cups-manager.desktop.in.h:1
msgid "Configure your printers"
msgstr "Uw printers instellen"

#: ../gnome-cups-manager.desktop.in.h:2
msgid "Printing"
msgstr "Printen"

#: ../gnome-cups-manager/gnome-cups-icon.c:72
msgid ""
"Could not start the printer tray icon, because the CUPS server could not be "
"contacted."
msgstr ""
"Kon statuspictogram niet starten omdat er geen contact kan worden gemaakt "
"met de CUPS-server."

#: ../gnome-cups-manager/gnome-cups-icon.c:129
msgid "Print Monitor"
msgstr "Afdrukmonitor"

#: ../gnome-cups-manager/gnome-cups-manager.c:61
msgid "The CUPS server could not be contacted."
msgstr "Kan geen contact krijgen met de CUPS-server."

#: ../gnome-cups-manager/gnome-cups-manager.c:97
#, c-format
msgid "Printer not found: %s"
msgstr "Printer niet gevonden: %s"

#: ../gnome-cups-manager/gnome-cups-manager.c:211
msgid "Show printer properties for printers listed on the command line"
msgstr ""
"Printereigenschappen weergeven voor de op de opdrachtregel gegeven printers"

#: ../gnome-cups-manager/gnome-cups-manager.c:214
msgid "View the queues of printers listed on the command line"
msgstr "Wachtrij weergeven voor de op de opdrachtregel gegeven printers"

#: ../gnome-cups-manager/gnome-cups-manager.c:225
msgid "Printers View"
msgstr "Printerweegave"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:1
msgid "    "
msgstr "    "

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:2
msgid "<b>Layout</b>"
msgstr "<b>Indeling</b>"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:3
msgid "<b>Media</b>"
msgstr "<b>Media</b>"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:4
msgid "Advanced"
msgstr "Geavanceerd"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:5
msgid "Become _Administrator"
msgstr "_Beheerder worden"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:6
msgid "Cancel Jobs"
msgstr "Taken afbreken"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:7
msgid "General"
msgstr "Algemeen"

# als standaard instellen/standaard maken
#: ../gnome-cups-manager/gnome-cups-manager.glade.h:8
msgid "Make _Default"
msgstr "_Standaard maken"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:9
msgid "Paper"
msgstr "Papier"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:10
msgid "Paper _type:"
msgstr "Papier_type:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:11
msgid "Paper si_ze:"
msgstr "Papier_afmetingen:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:12
msgid "Pause Jobs"
msgstr "Taken pauzeren"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:13
msgid "Pause Printer"
msgstr "Printer pauzeren"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:14
msgid "Print _Test Page"
msgstr "_Testpagina afdrukken"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:15
#: ../libgnomecups/gnome-cups-ui-connection.c:979
msgid "Printer"
msgstr "Printer"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:16
msgid "Printer Properties"
msgstr "Printereigenschappen"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:17
msgid "Printers"
msgstr "Printers"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:18
msgid "Ready or something"
msgstr "Klaar ofzo"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:19
msgid "Resume Jobs"
msgstr "Taken hervatten"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:20
msgid "Set as Default"
msgstr "Als standaard instellen"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:21
msgid "Status:"
msgstr "Status:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:22
msgid "_Add Printer"
msgstr "Printer _toevoegen"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:23
msgid "_Binding:"
msgstr "_Binding:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:24
msgid "_Close"
msgstr "Sl_uiten"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:25
msgid "_Description:"
msgstr "_Omschrijving:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:26
msgid "_Double Sided:"
msgstr "_Dubbelzijdig:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:27
msgid "_Edit"
msgstr "Be_werken"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:28
msgid "_Location:"
msgstr "_Locatie:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:29
msgid "_Name:"
msgstr "_Naam:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:30
msgid "_Orientation:"
msgstr "_Oriëntatie:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:31
msgid "_Pause"
msgstr "_Pauzeren"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:32
msgid "_Print a Test Page"
msgstr "Een testpagina a_fdrukken"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:33
msgid "_Printer"
msgstr "_Printer"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:34
msgid "_Properties"
msgstr "_Eigenschappen"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:35
msgid "_Remove Printer"
msgstr "Printer _verwijderen"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:36
msgid "_Resolution:"
msgstr "_Resolutie:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:37
msgid "_Resume"
msgstr "_Doorgaan"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:38
msgid "_Source:"
msgstr "_Bron:"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:39
msgid "_Status"
msgstr "_Status"

#: ../gnome-cups-manager/gnome-cups-manager.glade.h:40
msgid "dummy"
msgstr "dummy"

#: ../gnome-cups-manager/Gnome_CupsManager.server.in.h:1
msgid "Printer Icon"
msgstr "Printer pictogram"

#: ../gnome-cups-manager/Gnome_CupsManager.server.in.h:2
msgid "Printer View"
msgstr "Printerweegave"

#: ../gnome-cups-manager/gnome-printer-list.c:96
msgid "New Printer"
msgstr "Nieuwe printer"

# taken/opdrachten/printopdrachten
#: ../gnome-cups-manager/gnome-printer-list.c:384
msgid "Jobs"
msgstr "Taken"

#: ../gnome-cups-manager/gnome-printer-list.c:395
#: ../gnome-cups-manager/view-queue.c:523
msgid "Pause"
msgstr "Pauzeren"

#: ../gnome-cups-manager/gnome-printer-list.c:403
#: ../gnome-cups-manager/view-queue.c:530
msgid "Resume"
msgstr "Hervatten"

# standaard maken/als standaard instellen
#: ../gnome-cups-manager/gnome-printer-list.c:411
msgid "Make Default"
msgstr "Als standaard instellen"

#: ../gnome-cups-manager/printer-properties.c:566
msgid "Driver"
msgstr "Stuurprogramma"

#: ../gnome-cups-manager/printer-properties.c:614
msgid "Connection"
msgstr "Verbinding"

#: ../gnome-cups-manager/tray.c:46
msgid "Could not launch printer view"
msgstr "Kan printerweergave niet starten"

#: ../gnome-cups-manager/tray.c:66
msgid "Could not launch printer properties"
msgstr "Kan printereigenschappen niet starten"

#: ../gnome-cups-manager/tray.c:90 ../gnome-cups-manager/tray.c:98
#: ../gnome-cups-manager/view-queue.c:276
#: ../gnome-cups-manager/view-queue.c:284
msgid "Could not resume printer"
msgstr "Kan printer niet hervatten"

#: ../gnome-cups-manager/tray.c:129 ../gnome-cups-manager/view-queue.c:243
msgid "_Resume Printer"
msgstr "Printer _hervatten"

#: ../gnome-cups-manager/tray.c:131 ../gnome-cups-manager/view-queue.c:245
msgid "P_ause Printer"
msgstr "Printer _pauzeren"

#: ../gnome-cups-manager/tray.c:198
#, c-format
msgid "%s - %s"
msgstr "%s - %s"

#: ../gnome-cups-manager/view-queue.c:57
#, c-format
msgid "%.1f K"
msgstr "%.1f K"

#: ../gnome-cups-manager/view-queue.c:61
#, c-format
msgid "%.1f MB"
msgstr "%.1f MB"

#: ../gnome-cups-manager/view-queue.c:65
#, c-format
msgid "%.1f GB"
msgstr "%.1f GB"

#: ../gnome-cups-manager/view-queue.c:69
#, c-format
msgid "1 byte"
msgid_plural "%lu bytes"
msgstr[0] "1 byte"
msgstr[1] "%lu bytes"

#: ../gnome-cups-manager/view-queue.c:80
#, c-format
msgid "%d of %d"
msgstr "%d van %d"

#: ../gnome-cups-manager/view-queue.c:303
#, c-format
msgid "Could not set %s as the default"
msgstr "Kan %s niet als standaard instellen"

#: ../gnome-cups-manager/view-queue.c:400
msgid "Could not pause job"
msgstr "Kan taak niet pauzeren"

#: ../gnome-cups-manager/view-queue.c:433
msgid "Could not resume job"
msgstr "Kan taak niet hervatten"

#: ../gnome-cups-manager/view-queue.c:465
msgid "Could not cancel job"
msgstr "Kan taak niet afbreken"

#: ../gnome-cups-manager/view-queue.c:541
msgid "Cancel"
msgstr "Annuleren"

#: ../gnome-cups-manager/view-queue.c:615
msgid "Name"
msgstr "Naam"

#: ../gnome-cups-manager/view-queue.c:624
msgid "Job Number"
msgstr "Taaknummer"

#: ../gnome-cups-manager/view-queue.c:632
msgid "Owner"
msgstr "Eigenaar"

#: ../gnome-cups-manager/view-queue.c:641
msgid "Size"
msgstr "Grootte"

#: ../gnome-cups-manager/view-queue.c:649
msgid "State"
msgstr "Status"

# Als standaard instellen<
#: ../icons/emblem-default.icon.in.h:1
msgid "Default"
msgstr "Standaard"

#: ../icons/emblem-paused.icon.in.h:1
msgid "Paused"
msgstr "Gepauzeerd"

#: ../libgnomecups/gnome-cups-permission.c:102
#, c-format
msgid ""
"<b>Problems launching %s as root via %s</b>\n"
"\t<small>%s</small>"
msgstr ""
"<b>Problemen met opstarten %s als root via %s</b>\n"
"\t<small>%s</small>"

#: ../libgnomecups/gnome-cups-ui-connection.c:775
#, c-format
msgid "Identity and Password for %s in workgroup %s"
msgstr "Identiteit en wachtwoord voor %s in werkgroep %s"

#: ../libgnomecups/gnome-cups-ui-connection.c:778
#, c-format
msgid "Identity and Password for %s"
msgstr "Identiteit en wachtwoord voor %s"

#: ../libgnomecups/gnome-cups-ui-connection.c:781
#, c-format
msgid "Identity and Password for workgroup %s"
msgstr "Identiteit en wachtwoord voor werkgroep %s"

#: ../libgnomecups/gnome-cups-ui-connection.c:785
msgid "Identity and Password"
msgstr "Identiteit en wachtwoord"

#: ../libgnomecups/gnome-cups-ui-connection.c:789
msgid "Authentication Required"
msgstr "Aanmeldingscontrole vereist"

#: ../libgnomecups/gnome-cups-ui-connection.c:1016
msgid "<i>No printers detected</i>"
msgstr "<i>Geen printers gevonden</i>"

#: ../libgnomecups/gnome-cups-ui-connection.c:1091
msgid ""
"For example :\n"
"\thttp://hostname:631/ipp/\n"
"\thttp://hostname:631/ipp/port1\n"
"\tipp://hostname/ipp/\n"
"\tipp://hostname/ipp/port1"
msgstr ""
"Bijvoorbeeld :\n"
"\thttp://hostnaam:631/ipp/\n"
"\thttp://hostnaam:631/ipp/poort1\n"
"\tipp://hostnaam/ipp/\n"
"\tipp://hostnaam/ipp/poort1"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:1
msgid "9100"
msgstr "9100"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:2
msgid "<b>P_assword:</b>"
msgstr "<b>_Wachtwoord:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:3
msgid "<b>Printer Type:</b>"
msgstr "<b>Printertype:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:4
msgid "<b>_Host:</b>"
msgstr "<b>_Computer:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:5
msgid "<b>_Port:</b>"
msgstr "<b>_Poort:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:6
msgid "<b>_Printer:</b>"
msgstr "<b>_Printer:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:7
msgid "<b>_Printers:</b>"
msgstr "<b>_Printers:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:8
msgid "<b>_Queue:</b>"
msgstr "<b>_Wachtrij:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:9
msgid "<b>_URI:</b>"
msgstr "<b>_URI:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:10
msgid "<b>_Username:</b>"
msgstr "<b>_Gebruikersnaam:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:11
msgid "<b>_Workgroup:</b>"
msgstr "<b>_Werkgroep:</b>"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:12
msgid ""
"CUPS Printer (IPP)\n"
"Windows Printer (SMB)\n"
"UNIX Printer (LPD)\n"
"HP JetDirect"
msgstr ""
"CUPS Printer (IPP)\n"
"Windows Printer (SMB)\n"
"UNIX Printer (LPD)\n"
"HP JetDirect"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:16
msgid "Printer _Port:"
msgstr "Printer_poort:"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:17
msgid "Use _another printer by specifying a port:"
msgstr "Gebruik een _andere printer door een poort te geven:"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:18
msgid "_Local Printer"
msgstr "_Locale printer"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:19
msgid "_Network Printer"
msgstr "_Netwerk printer"

# zoeken/doorzoeken/onderzoeken/scannen
#: ../libgnomecups/gnome-cups-ui-connection.glade.h:20
msgid "_Scan"
msgstr "_Scannen"

# gedetecteerde printer?
#: ../libgnomecups/gnome-cups-ui-connection.glade.h:21
msgid "_Use a detected printer:"
msgstr "_Gebruik een gevonden printer:"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:22
msgid "hp"
msgstr "hp"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:23
msgid "ipp"
msgstr "ipp"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:24
msgid "local"
msgstr "locaal"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:25
msgid "lpd"
msgstr "lpd"

#: ../libgnomecups/gnome-cups-ui-connection.glade.h:26
msgid "smb"
msgstr "smb"

#: ../libgnomecups/gnome-cups-ui-driver.c:292
msgid "Generic"
msgstr "Algemeen"

#: ../libgnomecups/gnome-cups-ui-driver.c:312
msgid "High Quality Image (GIMP-Print Inkjet)"
msgstr "Afbeelding met hoge kwaliteit (GIMP-Print inkjet)"

#: ../libgnomecups/gnome-cups-ui-driver.c:319
#: ../libgnomecups/gnome-cups-ui-driver.c:347
msgid "High Quality Image (GIMP-Print)"
msgstr "Afbeelding met hoge kwaliteit (GIMP-Print)"

#: ../libgnomecups/gnome-cups-ui-driver.c:332
#: ../libgnomecups/gnome-cups-ui-driver.c:362
msgid "Standard (CUPS)"
msgstr "Standaard (CUPS)"

#: ../libgnomecups/gnome-cups-ui-driver.c:344
#, c-format
msgid "High Quality Image (GIMP-Print) (%s)"
msgstr "Afbeelding met hoge kwaliteit (GIMP-Print) (%s)"

#: ../libgnomecups/gnome-cups-ui-driver.c:358
msgid "Standard"
msgstr "Standaard<"

# aangeraden
#: ../libgnomecups/gnome-cups-ui-driver.c:476
#: ../libgnomecups/gnome-cups-ui-driver.c:695
msgid "(Suggested)"
msgstr "(Aanbevolen)"

#: ../libgnomecups/gnome-cups-ui-driver.c:806
msgid "Select a PPD File"
msgstr "Een PPD-bestand kiezen"

#: ../libgnomecups/gnome-cups-ui-driver.c:817
msgid "All Files"
msgstr "Alle bestanden"

#: ../libgnomecups/gnome-cups-ui-driver.c:822
msgid "PPD Files"
msgstr "PPD-bestanden"

#: ../libgnomecups/gnome-cups-ui-driver.c:847
msgid "Only files ending with .ppd or .ppd.gz will be installed"
msgstr ""
"Alleen bestanden eindigend op .ppd of ppd.gz zullen geïnstalleerd worden"

#: ../libgnomecups/gnome-cups-ui-driver.c:853
#, c-format
msgid ""
"CUPS is installed differently than expected.  There is no directory '%s'"
msgstr ""
"CUPS is kennelijk op een aparte wijze geïnstalleerd.  De map '%s' ontbreekt"

#: ../libgnomecups/gnome-cups-ui-driver.c:883
#, c-format
msgid ""
"The PPD\n"
"\t<b>%s</b>\n"
"is already installed"
msgstr ""
"De PPD\n"
"\t<b>%s</b>\n"
"is al geïnstalleerd"

# vanwege
#: ../libgnomecups/gnome-cups-ui-driver.c:905
#, c-format
msgid ""
"Unable to write to\n"
"\t<b>%s</b>\n"
"because %s"
msgstr ""
"Kon niet schrijven naar\n"
"\t<b>%s</b>\n"
"omdat %s"

#: ../libgnomecups/gnome-cups-ui-driver.c:1018
msgid "Model"
msgstr "Model"

#: ../libgnomecups/gnome-cups-ui-driver.glade.h:1
msgid "<b>Description:</b>"
msgstr "<b>Beschrijving:</b>"

# leverancier
#: ../libgnomecups/gnome-cups-ui-driver.glade.h:2
msgid "<b>M_anufacturer:</b>"
msgstr "<b>_Producent:</b>"

#: ../libgnomecups/gnome-cups-ui-driver.glade.h:3
msgid "<b>_Driver:</b>"
msgstr "<b>_Stuurprogramma:</b>"

#: ../libgnomecups/gnome-cups-ui-driver.glade.h:4
msgid "<b>_Model:</b>"
msgstr "<b>_Model:</b>"

#: ../libgnomecups/gnome-cups-ui-driver.glade.h:5
msgid "_Install Driver..."
msgstr "S_tuurprogramma installeren..."

#: ../libgnomecups/gnome-cups-ui-driver.glade.h:6
msgid "make and model"
msgstr "merk en modeltype"

#: ../libgnomecups/gnome-cups-ui-init.c:47
msgid "Password"
msgstr "Wachtwoord"

#: ../libgnomecups/gnome-cups-ui-init.c:55
msgid "Enter a username and password to modify this printer:"
msgstr "Geef een gebruikersnaam en wachtwoord om deze printer te bewerken"

#: ../libgnomecups/gnome-cups-ui-init.c:63
msgid "Username: "
msgstr "Info _Huidig Frame"

#: ../libgnomecups/gnome-cups-ui-init.c:75
msgid "Password: "
msgstr "Wachtwoord:"

#: ../libgnomecups/gnome-cups-ui-print.c:32
#, c-format
msgid ""
"Printing to '%s' failed with error code: %d\n"
"is the printer paused ?"
msgstr ""
"Printen naar '%s' mislukt met foutcode: %d\n"
"Is de printer gepauzeerd?"

#: ../libgnomecups/gnome-cups-ui-util.c:27
msgid "Unknown Error"
msgstr "Onbekende fout"

#: ../libgnomecups/gnome-cups-ui-util.c:88
msgid "Test Page"
msgstr "Testpagina"

#: ../libgnomecups/gnome-cups-ui-util.c:97
#, c-format
msgid "%s test page has been sent to %s."
msgstr "%s testpagina verstuurd naar %s."

#~ msgid "*"
#~ msgstr "*"

#~ msgid "<b>_User:</b>"
#~ msgstr "<b>_Gebruiker:</b>"

#~ msgid "Delete"
#~ msgstr "Verwijderen"

#~ msgid "Properties"
#~ msgstr "Eigenschappen"

#~ msgid ""
#~ "Please provide the following data for your printer.  If you do not know "
#~ "where to find this information, consult your system administrator."
#~ msgstr ""
#~ "Geef alstublieft de benodigde informatie over uw printer.  Neem, als u "
#~ "deze informatie niet kunt vinden, contact op met uw systeembeheerder."

#~ msgid "UNIX printer %s on %s"
#~ msgstr "UNIX printer %s op %s"

#~ msgid "UNIX printer on %s"
#~ msgstr "UNIX printer op %s"

#~ msgid "Windows Printer %s on %s"
#~ msgstr "Windows printer %s op %s"

#~ msgid "IPP Printer at %s"
#~ msgstr "IPP printer op %s"

#~ msgid "<b>The printer I wish to add is:</b>"
#~ msgstr "<b>De printer die ik wil toevoegen is:</b>"

#~ msgid "A _local printer attached to this computer"
#~ msgstr "Een printer die aan _deze computer aangesloten zit"

#~ msgid "A _remote printer attached to the network"
#~ msgstr "Een printer die via het _netwerk benaderd wordt"

#~ msgid ""
#~ "The following printers have been detected on your computer.  Please "
#~ "select the one you would like to use, or specify another."
#~ msgstr ""
#~ "De volgende printers zijn gevonden op uw computer.  Kies degene die u "
#~ "wenst te gebruiken, of specificeer een ander."

#~ msgid ""
#~ "This assistant will help you to quickly add a new printer to your "
#~ "system.  To begin, please specify the location of the printer."
#~ msgstr ""
#~ "Deze assistent zal u helpen bij het snel toevoegen van een printer. Geef, "
#~ "om te beginnen, de locatie van uw printer."

#~ msgid "Welcome!"
#~ msgstr "Welkom!"

#~ msgid "%u bytes"
#~ msgstr "%u bytes"

#~ msgid "Local printer on %s"
#~ msgstr "Lokale printer op %s"

#~ msgid ""
#~ "The printer name must begin with a letter and may not contain spaces."
#~ msgstr ""
#~ "De printernaam moet beginnen met een letter en mag geen spaties bevatten."

#~ msgid "<b>Location:</b>"
#~ msgstr "<b>Locatie:</b>"

#~ msgid "<b>Name:</b>"
#~ msgstr "<b>Naam:</b>"

#~ msgid "<b>Type:</b>"
#~ msgstr "<b>Type:</b>"

#~ msgid "<b>_Description:</b>"
#~ msgstr "<b>Be_schrijving:</b>"

#~ msgid "<b>_Name:</b>"
#~ msgstr "<b>_Naam:</b>"

#~ msgid ""
#~ "<span size=\"smaller\">This description is optional, it will be used for "
#~ "display purposes only. </span>"
#~ msgstr ""
#~ "<span size=\"smaller\">Deze beschijving is optioneel en is alleen voor "
#~ "weergavedoeleinden. </span>"

#~ msgid ""
#~ "Enter a name for this printer.  The name must begin with a letter and may "
#~ "not contain any spaces."
#~ msgstr ""
#~ "Geef een naam voor deze printer.  De naam moet beginnen met een letter en "
#~ "mag geen spaties bevatten."

#~ msgid ""
#~ "If this is correct, click \"Apply\".  If not, click \"Cancel\" or go back "
#~ "and make changes."
#~ msgstr ""
#~ "Klik op \"Toepassen\" als dit correct is. Is dit niet het geval, ga dan "
#~ "terug om te corrigeren of klik op annuleren om af te breken."

#~ msgid "Step 3 of 4: Printer Display"
#~ msgstr "Stap 3 van 4: Printerweergave"

#~ msgid "Step 4 of 4: Finish"
#~ msgstr "Stap 4 van 4: Afronden"

#~ msgid "You have selected the following options for this printer:"
#~ msgstr "U heeft de volgende opties gekozen voor deze printer:"

#~ msgid ""
#~ "The following data about your printer has been detected.  Please review "
#~ "this information and make sure it is correct."
#~ msgstr ""
#~ "De volgende informatie over uw printer is gevonden.  Controleer of dit "
#~ "correct is."

#~ msgid "%s (%s)"
#~ msgstr "%s (%s)"

#~ msgid "CUPS"
#~ msgstr "CUPS"

#~ msgid "HP JetDirect Printer"
#~ msgstr "HP JetDirect Printer"

#~ msgid "UNIX Printer (LPD)"
#~ msgstr "UNIX printer (LPD)"

#~ msgid "Windows Printer (SMB)"
#~ msgstr "Windows printer (SMB)"

#~ msgid "_Delete"
#~ msgstr "_Verwijderen"

#~ msgid "_Open"
#~ msgstr "_Openen"

#~ msgid "Selection mode"
#~ msgstr "Selectiemodus"

#~ msgid "The selection mode"
#~ msgstr "De selectiemodus"

#~ msgid "Sorted"
#~ msgstr "Gesorteerd"

#~ msgid "Icon list is sorted"
#~ msgstr "Lijst is gesorteerd"

#~ msgid "Sort order"
#~ msgstr "Sorteerrichting"

#~ msgid "Sort direction the icon list should use"
#~ msgstr "Te gebruiken sorteerrichting"

#~ msgid "Icon padding"
#~ msgstr "Afstand tussen pictogrammen"

#~ msgid "Number of pixels between icons"
#~ msgstr "Aantal beeldpunten tussen pictogrammen"

#~ msgid "Top margin"
#~ msgstr "Bovenmarge"

#~ msgid "Number of pixels in top margin"
#~ msgstr "Aantal beeldputen aan de bovenkant"

#~ msgid "Bottom margin"
#~ msgstr "Ondermarge"

#~ msgid "Number of pixels in bottom margin"
#~ msgstr "Aantal beeldpunten aan de onderkant"

#~ msgid "Left margin"
#~ msgstr "Linkermarge"

#~ msgid "Number of pixels in left margin"
#~ msgstr "Aantal beeldpunten aan de linker kant"

#~ msgid "Right margin"
#~ msgstr "Rechtermarge"

#~ msgid "Number of pixels in right margin"
#~ msgstr "Aantal beeldpunten aan de rechter kant"

#~ msgid "Selection Box Color"
#~ msgstr "Kleur selectieveld"

#~ msgid "Color of the selection box"
#~ msgstr "Naar van het selectieveld"

#~ msgid "Selection Box Alpha"
#~ msgstr "Alpha-waarde selectieveld"

#~ msgid "Opacity of the selection box"
#~ msgstr "Doorschijnendheid van het selectieveld"
