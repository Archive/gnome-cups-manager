/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 * gnome-cups-permission.c: A central location to manage admin priviledges
 *
 * Copyright (C) 2004 Jody Goldberg (jody@gnome.org)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include "gnome-cups-permission.h"

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include <unistd.h>
#include <sys/types.h>
#include <string.h>

static char *gnome_cups_app_path = NULL;

gboolean
gnome_cups_can_admin (void)
{
	return (geteuid () == 0);
}

void
gnome_cups_checkpath (char const *argv_0)
{
	/* store the path for argv_0.  when re-running ourseleves we will want
	 * to use the same path. */
	if (argv_0 != NULL) {
		gnome_cups_app_path = g_path_get_dirname (argv_0);
		if (NULL != gnome_cups_app_path &&
		    0 == strcmp (".", gnome_cups_app_path)) {
			g_free (gnome_cups_app_path);
			gnome_cups_app_path = NULL;
		}
	}
}

static char *
gnome_cups_execname (char const *name)
{
	/* check for explicit path first */
	if (gnome_cups_app_path != NULL) {
		char *tmp = g_build_filename (gnome_cups_app_path, name, NULL);
		if (g_file_test (tmp, G_FILE_TEST_IS_EXECUTABLE | G_FILE_TEST_EXISTS))
			return tmp;
		g_free (tmp);
	} 

	/* then see if it is in the path */
	if (!g_find_program_in_path (name)) {
		/* Ok now we are screwed, check a hard coded path */
		g_warning ("unable to find '%s'", name);
	}
	return g_strdup (name);
}

gboolean
gnome_cups_spawn (char const *app, int argc, char const **argv,
		  gboolean force_admin, GtkWidget *toplevel)
{
	static char const *su_app[] = {
		"gnomesu",
		"--"
	};
	char const **args = g_new0 (char const *, argc + G_N_ELEMENTS (su_app) +
			     1 /* app */ + 1 /* null */);
	unsigned offset, i = 0;
	GError *err = NULL;

	if (force_admin && !gnome_cups_can_admin ())
		for (i = 0 ; i < G_N_ELEMENTS (su_app) ; i++)
			args [i] = (char *)su_app [i];
	offset = i;
	app = args [offset++] = gnome_cups_execname (app);
	for (i = 0 ; i < argc ; i++)
		args [i + offset] = argv [i];
	
	g_spawn_async (NULL, (char **)args, NULL, G_SPAWN_SEARCH_PATH,
		       NULL, NULL, NULL, &err);
	if (err != NULL) {
		GtkWidget *dialog = gtk_message_dialog_new_with_markup (NULL, 
			GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL, 
			GTK_MESSAGE_ERROR,
			GTK_BUTTONS_OK,
			_("<b>Problems launching %s as root via %s</b>\n\t<small>%s</small>"),
			app, su_app[0], err->message);
		if (toplevel != NULL)
			gtk_window_set_transient_for (GTK_WINDOW (dialog),
						      GTK_WINDOW (toplevel));
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog); 
		g_error_free (err);
	}
	g_free (args);

	return err == NULL;
}
