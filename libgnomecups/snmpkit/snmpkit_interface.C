/*
 * snmpkit_interface.C: implementaiton of the C interface of 
 * snmpkit.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later 
 * version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 *
 * See the AUTHORS file for a list of people who have hacked on 
 * this code. 
 * See the ChangeLog file for a list of changes.
 *
 */

#include "snmpkit.h"
#include "snmpkit"

#include "errno.h"

SNMPERRNO sk_errno;
char *sk_erroid;

struct snmpkit_error{
  const char *msg;
  const char *ref;
};

static const snmpkit_error _sk_errors[]={
  {"No error",NULL}, // 0
  {"Memory allocation failed",NULL}, // 1
  {"UDP protocol is not supported","getprotobyname(3)"}, // 2
  {"Can't create socket","socket(2)"}, // 3
  {"Can't create a thread to receive packets","pthread_create(3)"}, // 4
  {"Host not found","gethostbyname(3)"}, // 5
  {"Can't create a thread to reap spent threads","pthread_create(3)"}, //6
  {"Can't create worker thread","pthread_create(3)"}, // 7
  {"Community in hostspec doesn't have ending parenthesis",NULL}, // 8
  {"One of the octets in hostspec is greater than 255",NULL}, // 9
  {"Subnet mask wasn't in a understood form",NULL}, // 10
  {"Number of bits in subnet mask is too large",NULL}, // 11
  {"Subnet mask had discontinious bits",NULL}, // 12
  {"The structure filler is already empty",NULL}, //13
  {"The structure filler did not contain that object",NULL}, //14
  {"The structure filler is corrupt, the table had the element but the OidSeq was empty",NULL}, //15
  {"The structure filler is corrupt, the table had the element but it wasn't found in the OidSeq",NULL}, //16
  {"The structure filler is corrupt, the sequence container didn't follow the strucutre of an OidSeq",NULL}, //17
  {"No response",NULL}, // 18
  {"Bad SNMP response packet",NULL}, // 19
  {"Error reported with a requested object.",NULL}, // 20
  {"Problem sending packet","write(2)"}, // 21
  {"Internal error decifering packet",NULL}, // 22
  {"Could not decipher a suboid in oidstr",NULL}, // 23
  {"No objects in oidstring",NULL}, // 24
  {NULL,NULL}
};

inline SNMPSESSION *to_c(SNMP_session *s){ 
  return reinterpret_cast<SNMPSESSION *>(s); 
}
inline SNMPSTRUCTFILLER *to_c(SNMP_structFiller *s){
  return reinterpret_cast<SNMPSTRUCTFILLER *>(s);
}
inline SNMPTABLE *to_c(SNMP_table *s){
  return reinterpret_cast<SNMPTABLE *>(s);
}

inline SNMP_session *from_c(SNMPSESSION *s){ 
  return reinterpret_cast<SNMP_session *>(s); 
}
inline SNMP_structFiller *from_c(SNMPSTRUCTFILLER *s){ 
  return reinterpret_cast<SNMP_structFiller *>(s); 
}
inline SNMP_table *from_c(SNMPTABLE *s){ 
  return reinterpret_cast<SNMP_table *>(s); 
}

typedef void *(*CPP_FP)(SNMP_session*);
typedef void *(*C_FP)(SNMPSESSION*);
inline CPP_FP from_c(C_FP in){
  return reinterpret_cast<CPP_FP>(in);
}
inline C_FP to_c(CPP_FP in){
  return reinterpret_cast<C_FP>(in);
}

// -----SNMPSESSION-----
SNMPSESSION *sk_new_session(char *host, 
			    void *(start_routine)(SNMPSESSION*),
			    const char *community){
  try{
    return to_c((community==NULL)?
		new SNMP_session(host,from_c(start_routine)):
		new SNMP_session(host,from_c(start_routine),community));
  }catch(std::bad_alloc){
    sk_errno=SK_MEM_FAIL;
  }catch(SocketNoUDPException){
    sk_errno=SK_BAD_PROTO;
  }catch(SocketCreateFailException e){
    errno=e.error();
    sk_errno=SK_SOCKET_FAIL;
  }catch(ReceiverCreateException e){
    errno=e.error();
    sk_errno=SK_RECEIVER_FAIL;
  }catch(SessionHostNotFoundException e){
    h_errno=e.error();
    sk_errno=SK_HOSTNOTFOUND;
  }catch(JoinerCreateException e){
    errno=e.error();
    sk_errno=SK_REAPER_FAIL;
  }catch(SessionWorkerCreateException e){
    errno=e.error();
    sk_errno=SK_WORKER_FAIL;
  }
  return NULL;
}

SNMPSESSION **sk_new_sessions(char *hostspec,const char *community,
			      void *(start_routine)(SNMPSESSION*)){
  std::list<SNMP_session*> dest;
  std::string hs(hostspec);
  SNMPSESSION **retval=NULL;
  try{
    if(community==NULL)
      SNMP_sessions(dest,hs,from_c(start_routine));
    else
      SNMP_sessions(dest,hs,from_c(start_routine),community);
    retval=new SNMPSESSION*[dest.size()+1];
    int j=0;
    retval[dest.size()]=NULL;
    for(std::list<SNMP_session*>::iterator i=dest.begin();i!=dest.end();i++,j++)
      retval[j]=to_c(*i);
  }catch(std::bad_alloc){
    sk_errno=SK_MEM_FAIL;    
  }catch(SocketNoUDPException){
    sk_errno=SK_BAD_PROTO;
  }catch(SocketCreateFailException e){
    errno=e.error();
    sk_errno=SK_SOCKET_FAIL;
  }catch(ReceiverCreateException e){
    errno=e.error();
    sk_errno=SK_RECEIVER_FAIL;
  }catch(SessionHostNotFoundException e){
    h_errno=e.error();
    sk_errno=SK_HOSTNOTFOUND;
  }catch(JoinerCreateException e){
    errno=e.error();
    sk_errno=SK_REAPER_FAIL;
  }catch(SessionWorkerCreateException e){
    errno=e.error();
    sk_errno=SK_WORKER_FAIL;
  }catch(SessionCommunityException){
    sk_errno=SK_UNTERM_HOSTSPEC;
  }catch(SessionOctetOverflowException){
    sk_errno=SK_OCTET_OVERFLOW;
  }catch(SessionBadSubnetException){
    sk_errno=SK_BAD_NETMASK;
  }catch(SessionNetbitsOverflowException){
    sk_errno=SK_NETMASK_OVERFLOW;
  }catch(SessionBadNetmaskException){
    sk_errno=SK_DIS_NETMASK;
  }
  return retval;
}

SNMPSESSION **sk_new_sessions_multi(char **hostspecs,
				    void *(start_routine)(SNMPSESSION*),
				    const char *community){
  std::list<SNMP_session*> dest;
  std::string hs;
  SNMPSESSION **retval;
  try{
    if(community==NULL)
      for(;*hostspecs!=NULL;hostspecs++){
	hs=*hostspecs;
	SNMP_sessions(dest,hs,from_c(start_routine));
      }
    else
      for(;*hostspecs!=NULL;hostspecs++){
	hs=*hostspecs;
	SNMP_sessions(dest,hs,from_c(start_routine),community);
      }
    retval=new SNMPSESSION*[dest.size()+1];
    retval[dest.size()]=NULL;
    int j=0;
    for(std::list<SNMP_session*>::iterator i=dest.begin();i!=dest.end();i++,j++)
      retval[j]=to_c(*i);
  }catch(std::bad_alloc){
    sk_errno=SK_MEM_FAIL; 
  }catch(SocketNoUDPException){
    sk_errno=SK_BAD_PROTO;
  }catch(SocketCreateFailException e){
    errno=e.error();
    sk_errno=SK_SOCKET_FAIL;
  }catch(ReceiverCreateException e){
    errno=e.error();
    sk_errno=SK_RECEIVER_FAIL;
  }catch(SessionHostNotFoundException e){
    h_errno=e.error();
    sk_errno=SK_HOSTNOTFOUND;
  }catch(JoinerCreateException e){
    errno=e.error();
    sk_errno=SK_REAPER_FAIL;
  }catch(SessionWorkerCreateException e){
    errno=e.error();
    sk_errno=SK_WORKER_FAIL;
  }catch(SessionCommunityException){
    sk_errno=SK_UNTERM_HOSTSPEC;
  }catch(SessionOctetOverflowException){
    sk_errno=SK_OCTET_OVERFLOW;
  }catch(SessionBadSubnetException){
    sk_errno=SK_BAD_NETMASK;
  }catch(SessionNetbitsOverflowException){
    sk_errno=SK_NETMASK_OVERFLOW;
  }catch(SessionBadNetmaskException){
    sk_errno=SK_DIS_NETMASK;
  }
  return retval;
}

void sk_del_session(SNMPSESSION *doomed){
  delete from_c(doomed);
}

void sk_del_sessions(SNMPSESSION **doomed){
  for(;*doomed!=NULL;doomed++)
    sk_del_session(*doomed);
}

inline void sk_sessions_done(){
  SNMP_sessions_done();
}

// -----SNMPSTRUCTFILLER-----
SNMPSTRUCTFILLER *sk_new_sfiller(SNMPSESSION *session){
  
  return to_c(new SNMP_structFiller(*from_c(session)));
}

void sk_del_sfiller(SNMPSTRUCTFILLER *doomed){
  delete from_c(doomed);
}

int sk_sfiller_remove(SNMPSTRUCTFILLER *sf,const char *oidstr){
  int retval=0;
  try{
    from_c(sf)->remove(oidstr); /* ITS4: ignore */
  }catch(FillerRemoveEmptyException e){
    retval=1;
    sk_errno=SK_FILLER_EMPTY;
  }catch(FillerRemoveNotFoundException e){
    retval=2;
    sk_errno=SK_NOT_FOUND;
  }catch(FillerCorruptException e){
    retval=3;
    sk_errno=SK_FILLER_OID_EMPTY;
  }catch(OidSeqRemoveNotFoundException e){
    retval=4;
    sk_errno=SK_FILLER_OID;
  }catch(OidSeqBadLayoutException e){
    retval=5;
    sk_errno=SK_FILLER_BADFORM;
  }
  return retval;
}

void *sk_sfiller_get(SNMPSTRUCTFILLER *sf,void *tobefilled){
  void *retval=NULL;
  try{
    retval=from_c(sf)->get(tobefilled);
  }catch(SNMPNoResponseException){
    sk_errno=SK_NO_RESP;
  }catch(SNMPPacketNotSequenceException){
    sk_errno=SK_BAD_RESP;
  }catch(SNMPRespNotSequenceException){
    sk_errno=SK_BAD_RESP;
  }catch(SNMPNotResponseTagException){
    sk_errno=SK_BAD_RESP;
  }catch(SNMPSeqnoNotIntException){
    sk_errno=SK_BAD_RESP;
  }catch(SNMPStateNotIntException){
    sk_errno=SK_BAD_RESP;
  }catch(SNMPFaultOidNotIntException){
    sk_errno=SK_BAD_RESP;
  }catch(OidSeqBadLayoutException){
    sk_errno=SK_BAD_RESP;
  }catch(SNMPBadOidException e){
    sk_errno=SK_REQ_FAIL;
    sk_erroid=strdup(e.str().c_str());
  }catch(std::bad_alloc){
    sk_errno=SK_MEM_FAIL;
  }catch(SocketSendShortExecption){
    sk_errno=SK_DECODE;
  }catch(BerSequenceTagException){
    sk_errno=SK_DECODE;
  }catch(BerLengthException){
    sk_errno=SK_DECODE;
  }catch(BerIntTagException){
    sk_errno=SK_DECODE;
  }catch(BerCounterTagException){
    sk_errno=SK_DECODE;
  }catch(BerStringTagException){
    sk_errno=SK_DECODE;
  }catch(BerNullTagException){
    sk_errno=SK_DECODE;
  }catch(BerOidTagException){
    sk_errno=SK_DECODE;
  }catch(BerTimeTickTagException){
    sk_errno=SK_DECODE;
  }
  return retval;
}

void *sk_sfiller_get_next(SNMPSTRUCTFILLER *sf,void *tobefilled){
  void *retval=NULL;
  try{
    retval=from_c(sf)->get_next(tobefilled);
  }catch(SNMPNoResponseException){
    sk_errno=SK_NO_RESP;
  }catch(SNMPPacketNotSequenceException){
    sk_errno=SK_BAD_RESP;
  }catch(SNMPRespNotSequenceException){
    sk_errno=SK_BAD_RESP;
  }catch(SNMPNotResponseTagException){
    sk_errno=SK_BAD_RESP;
  }catch(SNMPSeqnoNotIntException){
    sk_errno=SK_BAD_RESP;
  }catch(SNMPStateNotIntException){
    sk_errno=SK_BAD_RESP;
  }catch(SNMPFaultOidNotIntException){
    sk_errno=SK_BAD_RESP;
  }catch(OidSeqBadLayoutException){
    sk_errno=SK_BAD_RESP;
  }catch(SNMPBadOidException e){
    sk_errno=SK_REQ_FAIL;
    sk_erroid=strdup(e.str().c_str());
  }catch(std::bad_alloc){
    sk_errno=SK_MEM_FAIL;
  }catch(SocketSendShortExecption){
    sk_errno=SK_DECODE;
  }catch(BerSequenceTagException){
    sk_errno=SK_DECODE;
  }catch(BerLengthException){
    sk_errno=SK_DECODE;
  }catch(BerIntTagException){
    sk_errno=SK_DECODE;
  }catch(BerCounterTagException){
    sk_errno=SK_DECODE;
  }catch(BerStringTagException){
    sk_errno=SK_DECODE;
  }catch(BerNullTagException){
    sk_errno=SK_DECODE;
  }catch(BerOidTagException){
    sk_errno=SK_DECODE;
  }catch(BerTimeTickTagException){
    sk_errno=SK_DECODE;
  }
  return retval;
}

int sk_sfiller_append_int(SNMPSTRUCTFILLER *sf,
			  const char *oidstr,
			  void (*fp)(void*,long)){
  int retval=0;
  try{
    from_c(sf)->append_int(std::string(oidstr),fp);
  }catch(std::bad_alloc){
    sk_errno=SK_MEM_FAIL;
  }catch(BerOidBadSubOidException){
    sk_errno=SK_BAD_SUBOID;
  }catch(BerNoOidsException){
    sk_errno=SK_NO_OBJ;
  }
  return retval;
}

int sk_sfiller_append_counter(SNMPSTRUCTFILLER *sf,
			      const char *oidstr,
			      void (*fp)(void*,long)){
  int retval=0;
  try{
    from_c(sf)->append_counter(std::string(oidstr),fp);
  }catch(std::bad_alloc){
    sk_errno=SK_MEM_FAIL;
  }catch(BerOidBadSubOidException){
    sk_errno=SK_BAD_SUBOID;
  }catch(BerNoOidsException){
    sk_errno=SK_NO_OBJ;
  }
  return retval;
}

int sk_sfiller_append_timetick(SNMPSTRUCTFILLER *sf,
			       const char *oidstr,
			       void (*fp)(void*,unsigned long)){
  int retval=0;
  try{
    from_c(sf)->append_timetick(std::string(oidstr),fp);
  }catch(std::bad_alloc){
    sk_errno=SK_MEM_FAIL;
  }catch(BerOidBadSubOidException){
    sk_errno=SK_BAD_SUBOID;
  }catch(BerNoOidsException){
    sk_errno=SK_NO_OBJ;
  }
  return retval;
}

int sk_sfiller_append_ipaddr(SNMPSTRUCTFILLER *sf,
			     const char *oidstr,
			     void (*fp)(void*,const unsigned char *)){
  int retval=0;
  try{
    from_c(sf)->append_ipaddr(std::string(oidstr),fp);
  }catch(std::bad_alloc){
    sk_errno=SK_MEM_FAIL;
  }catch(BerOidBadSubOidException){
    sk_errno=SK_BAD_SUBOID;
  }catch(BerNoOidsException){
    sk_errno=SK_NO_OBJ;
  }
  return retval;
}

int sk_sfiller_append_string(SNMPSTRUCTFILLER *sf,
			      const char *oidstr,
			      void (*fp)(void*,const char *)){
  int retval=0;
  try{
    from_c(sf)->append_string(std::string(oidstr),fp);
  }catch(std::bad_alloc){
    sk_errno=SK_MEM_FAIL;
  }catch(BerOidBadSubOidException){
    sk_errno=SK_BAD_SUBOID;
  }catch(BerNoOidsException){
    sk_errno=SK_NO_OBJ;
  }
  return retval;
}

// -----SNMPTABLE-----
SNMPTABLE *sk_new_table(SNMPSESSION *session,void *(*new_funct)()){
  return to_c(new SNMP_table(*from_c(session),new_funct));
}

void sk_del_table(SNMPTABLE *doomed){
  delete from_c(doomed);
}

/*** Exceptions all taken care of up to here ***/
int sk_sfiller_remove(SNMPTABLE *st,const char *oidstr){
  int retval=0;
  try{
    from_c(st)->remove(oidstr); /* ITS4: ignore */
  }catch(FillerRemoveEmptyException e){
    retval=1;
    sk_errno=SK_FILLER_EMPTY;
  }catch(FillerRemoveNotFoundException e){
    retval=2;
    sk_errno=SK_NOT_FOUND;
  }catch(FillerCorruptException e){
    retval=3;
    sk_errno=SK_FILLER_OID_EMPTY;
  }catch(OidSeqRemoveNotFoundException e){
    retval=4;
    sk_errno=SK_FILLER_OID;
  }catch(OidSeqBadLayoutException e){
    retval=5;
    sk_errno=SK_FILLER_BADFORM;
  }
  return retval;
}

void **sk_table_get(SNMPTABLE *st){
  std::list<void*> vals;
  from_c(st)->get(vals);
  void **retval=new void *[vals.size()+1];
  retval[vals.size()]=NULL;
  int i=0;
  for(std::list<void*>::iterator cur=vals.begin();cur!=vals.end();cur++,i++)
    retval[i]=*cur;
  return retval;
}

int
sk_table_append_int (SNMPTABLE *st,const char *oidstr,
		     void (*fp)(void*,long))
{
  int retval=0;
  try{
    from_c(st)->append_int(std::string(oidstr),fp);  
  }catch(std::bad_alloc){
    sk_errno=SK_MEM_FAIL;
  }catch(BerOidBadSubOidException){
    sk_errno=SK_BAD_SUBOID;
  }catch(BerNoOidsException){
    sk_errno=SK_NO_OBJ;
  }
  return retval;
}

int sk_table_append_counter(SNMPTABLE *st,const char *oidstr,
			    void (*fp)(void*,long)){
  int retval=0;
  try{
    from_c(st)->append_counter(std::string(oidstr),fp);
  }catch(std::bad_alloc){
    sk_errno=SK_MEM_FAIL;
  }catch(BerOidBadSubOidException){
    sk_errno=SK_BAD_SUBOID;
  }catch(BerNoOidsException){
    sk_errno=SK_NO_OBJ;
  }
  return retval;
}

int sk_table_append_timetick(SNMPTABLE *st,const char *oidstr,
			     void (*fp)(void*,unsigned long)){
  int retval=0;
  try{
    from_c(st)->append_timetick(std::string(oidstr),fp);
  }catch(std::bad_alloc){
    sk_errno=SK_MEM_FAIL;
  }catch(BerOidBadSubOidException){
    sk_errno=SK_BAD_SUBOID;
  }catch(BerNoOidsException){
    sk_errno=SK_NO_OBJ;
  }
  return retval;
}

int sk_table_append_ipaddr(SNMPTABLE *st,const char *oidstr,
			   void (*fp)(void*,const unsigned char *)){
  int retval=0;
  try{
    from_c(st)->append_ipaddr(std::string(oidstr),fp);
  }catch(std::bad_alloc){
    sk_errno=SK_MEM_FAIL;
  }catch(BerOidBadSubOidException){
    sk_errno=SK_BAD_SUBOID;
  }catch(BerNoOidsException){
    sk_errno=SK_NO_OBJ;
  }
  return retval;
}

int sk_table_append_string(SNMPTABLE *st,const char *oidstr,
			   void (*fp)(void*,const char *)){
  int retval=0;
  try{
    from_c(st)->append_string(std::string(oidstr),fp);
  }catch(std::bad_alloc){
    sk_errno=SK_MEM_FAIL;
  }catch(BerOidBadSubOidException){
    sk_errno=SK_BAD_SUBOID;
  }catch(BerNoOidsException){
    sk_errno=SK_NO_OBJ;
  }
  return retval;
}
