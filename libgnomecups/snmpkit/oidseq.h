/*
 * oidseq.h: declaration of object idenitifier sequence
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later 
 * version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 *
 * See the AUTHORS file for a list of people who have hacked on 
 * this code. 
 * See the ChangeLog file for a list of changes.
 *
 */

#ifndef __OIDSEQ_H__
#define __OIDSEQ_H__

#include <string>
#include <list>
#include <map>
#include <stdexcept>

#include "ber.h"

class TableEntry;

class OidSeq {
  /* seq MUST be a pointer. the problem is that the 
     OidSeq(BerSequence*) constructor is used by doreq in session 
     and there and it passes in a pointer to a berseq and there is
     no way short of copying the whole underlining sequence to be 
     able to get rid of the sequence passed in. It must be 
     adopted. */
  BerSequence *seq; 
public:
  inline OidSeq()throw(std::bad_alloc){seq=new BerSequence(SEQUENCE_TAG);}
  OidSeq(BerSequence *valseq) throw(OidSeqBadLayoutException);//adopts valseq
  //  OidSeq(const list<TableEntry> &table) throw(bad_alloc); 
  inline ~OidSeq(){delete seq;}
  //for gets
  void append(const std::string &oidstr)
    throw(std::bad_alloc,BerOidBadSubOidException,BerNoOidsException);

  //for sets of ints and counters.
  void append(const std::string &oidstr,long data)
    throw(std::bad_alloc,BerOidBadSubOidException,BerNoOidsException);
  void append(const std::string &oidstr,unsigned long data)
    throw(std::bad_alloc,BerOidBadSubOidException,BerNoOidsException);

  //for sets of strings oid's and ipaddrs
  void append(const std::string &oidstr,Tags type,void *data,
	      unsigned int len)
    throw(std::bad_alloc,BerOidBadSubOidException,BerNoOidsException,
	  BerIPAddrLengthExecption);

  /* ITS4: ignore */
  void remove(const std::string &oidstr) 
    throw(OidSeqRemoveNotFoundException,OidSeqBadLayoutException);

  // returns the pointer to the value (keeps ownership)
  BerBase *value(const std::string &oid) throw(OidSeqBadLayoutException); 
  /* returns the pointer to the value of the child whose oid this
     is */
  BerBase *child(const std::string &oid) throw(OidSeqBadLayoutException);

  inline void ascii_print(std::string &str){seq->ascii_print(str);}
  inline BerSequence *Seq(){ return seq;}
};

#endif
