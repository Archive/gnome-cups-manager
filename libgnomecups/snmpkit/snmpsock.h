/*
 * snmpsock.h: declaration of the snmp socket class
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later 
 * version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 *
 * See the AUTHORS file for a list of people who have hacked on 
 * this code. 
 * See the ChangeLog file for a list of changes.
 *
 */

#ifndef __SNMPSOCK_H_
#define __SNMPSOCK_H_

#include <pthread.h>

#include "snmpkit_except"

class SNMP_socket {
  int sock;
  int timeout;
  int retries;
  int port;
  pthread_t listening_thr;
public:
  SNMP_socket(int timeout, int retries, int port=0)
    throw(SocketNoUDPException,SocketCreateFailException,
	  ReceiverCreateException);
  ~SNMP_socket();
  unsigned char *call(char *addr,int addrlen,int addrtype,
		      const unsigned char *data,int &buflen)
    throw(SocketSendShortExecption);
};

#endif
