/*
 * ber.h: Declaration of basic encoding rules class
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later 
 * version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 *
 * See the AUTHORS file for a list of people who have hacked on 
 * this code. 
 * See the ChangeLog file for a list of changes.
 *
 */

#ifndef __BER_H__
#define __BER_H__

#include <stdlib.h>

#include <string>
#include <deque>

#include "snmpkit_tags"
#include "snmpkit_except"

typedef std::basic_string<unsigned char> ustring;

class BerBase {
public: 
  virtual ustring &encode(ustring &dest)=0; //stick it in variable
  virtual void ascii_print(std::string &buf)=0;
  virtual Tags type()=0; /* Tried to get around having the type 
			    virtual function but found that I needed 
			    it to check the return packet structure */
};

class BerNull: public BerBase{
public:
  BerNull(unsigned char *str)
    throw(BerNullTagException,BerNullLengthExecption);
  BerNull(){}
  virtual ustring &encode(ustring &dest);
  virtual void ascii_print(std::string &buf){buf+="NULL ";}
  virtual Tags type(){return NULL_TAG;}
};

class BerInt: public BerBase {
  long val;
public:
  BerInt(unsigned char *str)
    throw(BerIntTagException,BerIntLengthExecption);
  inline BerInt(long valu): val(valu){}
  virtual ustring &encode(ustring &dest);
  virtual void ascii_print(std::string &buf);
  virtual Tags type(){return INT_TAG;}
  inline long value(){ return val;}
};

class BerCounter: public BerBase{
  unsigned long val;
 public:
  BerCounter(unsigned char *str)
    throw(BerCounterTagException,BerCounterLengthExecption);
  inline BerCounter(unsigned long valu): val(valu){}
  virtual ustring &encode(ustring &dest);
  virtual void ascii_print(std::string &buf);
  virtual Tags type(){return COUNTER_TAG;}
  inline unsigned long value(){ return val;}
};

class BerTimeTick: public BerBase {
  unsigned long val;
public:
  BerTimeTick(unsigned char *str)
    throw(BerTimeTickTagException,BerTimeTickLengthExecption);
  virtual void ascii_print(std::string &buf);
  virtual ustring &encode(ustring &dest);
  virtual Tags type(){return TIME_TICK_TAG;}
  inline unsigned long value(){ return val;}
};

class BerString: public BerBase {
  std::string str;
public:
  /* this one is for decoding the wire format of the string only. 
     It is not for a typical initialization of the class. */
  BerString(unsigned char *str)
    throw(BerStringTagException,BerLengthException); 
  virtual ~BerString(){};

  // this one is for normal use
  inline BerString(std::string &strng):str(strng){}
  inline BerString(char *strng,unsigned int len):str(strng,len){}
  virtual ustring &encode(ustring &buf);
  virtual void ascii_print(std::string &buf);
  virtual Tags type(){return STRING_TAG;}
  inline const std::string &value(){return str;}
};

class BerIPAddr: public BerBase {
  ustring str;
public:
  BerIPAddr(unsigned char *strng)
    throw(BerIPAddrTagException,BerIPAddrLengthExecption);
  BerIPAddr(const ustring &strng) throw(BerIPAddrLengthExecption);
  BerIPAddr(unsigned char *dat,unsigned int len)
    throw(BerIPAddrLengthExecption);
  virtual ~BerIPAddr(){};
  virtual ustring &encode(ustring &buf);
  virtual Tags type(){return IPADDR_TAG;}
  virtual void ascii_print(std::string &buf)throw(BerIPAddrLengthExecption);
  inline const ustring &ipaddr(){return str;}
};

class BerOid: public BerBase {
  ustring encoded_oid; // not including the header
public:
  BerOid(unsigned char *str) // decoding wire protocol only
    throw(BerOidTagException,BerLengthException); 
  BerOid(const std::string &oidstr) 
    throw(BerOidBadSubOidException,BerNoOidsException);
  virtual ~BerOid(){}

  inline int operator==(const BerOid &other)const{
    return encoded_oid==other.encoded_oid;}
  virtual Tags type(){return OID_TAG;}
  virtual ustring &encode(ustring &dest);
  virtual void ascii_print(std::string &buf);
};

class BerSequence: public BerBase{
public:
  typedef std::deque<BerBase*> ElementContainer;
private:
  ElementContainer elements;
  Tags tag;
public:
  BerSequence(unsigned char *str)// decoding wire protocol only
    throw(BerSequenceTagException,
	  BerLengthException,
	  std::bad_alloc,
	  BerIntTagException,BerIntLengthExecption,
	  BerCounterTagException,BerCounterLengthExecption,
	  BerStringTagException,BerNullTagException,BerNullLengthExecption,
	  BerOidTagException,BerTimeTickTagException,
	  BerTimeTickLengthExecption,BerIPAddrLengthExecption); 
  inline BerSequence(Tags utag):tag(utag){}
  virtual ~BerSequence();
  virtual ustring &encode(ustring &dest);

  inline void append(BerBase *newone){ elements.push_back(newone);}
  BerBase *extract(std::deque<BerBase*>::iterator ele);
  virtual Tags type(){return tag;}
  virtual void ascii_print(std::string &buf);

  inline ElementContainer::iterator begin(){return elements.begin();}
  inline ElementContainer::iterator end(){return elements.end();}
  inline size_t size(){return elements.size();}
};

void start_data(Tags type,unsigned int len, ustring &dest);
unsigned long unpack_len(unsigned char *start,unsigned char &headlen) 
  throw(BerLengthException);

#endif //__BER_H__

