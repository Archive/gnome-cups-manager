/*
 * snmpkit.h: header file for C interface to snmpkit
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later 
 * version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 *
 * See the AUTHORS file for a list of people who have hacked on 
 * this code. 
 * See the ChangeLog file for a list of changes.
 *
 */

#ifndef __SNMPKIT_H__
#define __SNMPKIT_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define SK_NO_ERROR 0
#define SK_MEM_FAIL 1
#define SK_BAD_PROTO 2
#define SK_SOCKET_FAIL 3
#define SK_RECEIVER_FAIL 4
#define SK_HOSTNOTFOUND 5
#define SK_REAPER_FAIL 6
#define SK_WORKER_FAIL 7
#define SK_UNTERM_HOSTSPEC 8
#define SK_OCTET_OVERFLOW 9
#define SK_BAD_NETMASK 10 
#define SK_NETMASK_OVERFLOW 11
#define SK_DIS_NETMASK 12
#define SK_FILLER_EMPTY 13
#define SK_NOT_FOUND 14
#define SK_FILLER_OID_EMPTY 15
#define SK_FILLER_OID 16
#define SK_FILLER_BADFORM 17
#define SK_NO_RESP 18
#define SK_BAD_RESP 19
#define SK_REQ_FAIL 20
#define SK_SEND_FAIL 21
#define SK_DECODE 22
#define SK_BAD_SUBOID 23
#define SK_NO_OBJ 24

  typedef struct{char dummy;} SNMPSESSION;
  typedef struct{char dummy;} SNMPSTRUCTFILLER;
  typedef struct{char dummy;} SNMPTABLE;

  typedef unsigned int SNMPERRNO;
  extern SNMPERRNO sk_errno;
  extern char *sk_erroid;
  const char *sk_strerror(SNMPERRNO num);
  const char *sk_strerror_ref(SNMPERRNO num);

  /* ------------ Sessions ------------
     create one session from a hostname */
  SNMPSESSION *sk_new_session(char *host,
			       void *(start_routine)(SNMPSESSION*),
			       const char *community);
  /* create multiple sessions from a hostspec */
  SNMPSESSION **sk_new_sessions(char *hostspec,const char *community,
				 void *(start_routine)(SNMPSESSION*));
  /* create multiple sessions from multiple hostspecs */
  SNMPSESSION **sk_new_sessions_multi(char **hostspecs,
				      const char *community,
				      void *(start_routine)(SNMPSESSION*));
  /* delete sessions */
  void sk_del_snmpsession(SNMPSESSION *doomed);
  void sk_del_snmpsessions(SNMPSESSION **doomed);
  /* done creating sessions */
  void sk_snmpsessions_done();

  /* ---------- Structure fillers ---------- */
  SNMPSTRUCTFILLER *sk_new_sfiller(SNMPSESSION *session);
  void sk_del_sfiller(SNMPSTRUCTFILLER *doomed);

  int sk_sfiller_append_int(SNMPSTRUCTFILLER *sf,
			     const char *oidstr,
			     void (*fp)(void*,long));
  int sk_sfiller_append_counter(SNMPSTRUCTFILLER *sf,
				 const char *oidstr,
				 void (*fp)(void*,long));
  int sk_sfiller_append_timetick(SNMPSTRUCTFILLER *sf,
				  const char *oidstr,
				  void (*fp)(void*,unsigned long));
  int sk_sfiller_append_ipaddr(SNMPSTRUCTFILLER *sf,
				const char *oidstr,
				void (*fp)(void*,const unsigned char *));
  int sk_sfiller_append_string(SNMPSTRUCTFILLER *sf,
				const char *oidstr,
				void (*fp)(void*,const char *));
  int sk_sfiller_remove(SNMPSTRUCTFILLER *sf,const char *oidstr);
  void *sk_sfiller_get(SNMPSTRUCTFILLER *sf,void *tobefilled);
  void *sk_sfiller_get_next(SNMPSTRUCTFILLER *sf,
			    void *tobefilled);

  /* ---------- Tables of filled structures --------- */
  SNMPTABLE *sk_new_snmptable(SNMPSESSION *session,unsigned int structlen);
  void sk_del_table(SNMPTABLE *doomed);
  int sk_table_append_int(SNMPTABLE *st,const char *oidstr,
			   void (*fp)(void*,long));
  int sk_table_append_counter(SNMPTABLE *st,const char *oidstr,
			       void (*fp)(void*,long));
  int sk_table_append_timetick(SNMPTABLE *st,const char *oidstr,
				void (*fp)(void*,unsigned long));
  int sk_table_append_ipaddr(SNMPTABLE *st,const char *oidstr,
			      void (*fp)(void*,unsigned char *));
  int sk_table_append_string(SNMPTABLE *st,const char *oidstr,
			      void (*fp)(void*,const char *));
  int sk_table_remove(SNMPTABLE *st,const char *oidstr);
  void **sk_table_get(SNMPTABLE *st);
#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* __SNMPKIT_H__ */
