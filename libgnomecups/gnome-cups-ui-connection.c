/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 * gnome-cups-ui-connection.c: A printer connection selector
 *
 * Copyright (C) 2004 Jody Goldberg (jody@gnome.org)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include "gnome-cups-ui-connection.h"
#include "gnome-cups-ui-util.h"
#include <libgnomecups/gnome-cups-request.h>
#include <libgnomecups/gnome-cups-util.h>

#include <glib.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <glib/gi18n.h>
#include <ctype.h>

#include "snmpinter.h"
#include <stdarg.h>
#include <gnome-keyring.h>
#ifdef HAVE_LIBSMBCLIENT
#include <libgnomeui/gnome-password-dialog.h>
#include <libsmbclient.h>
#include <errno.h>
#endif

struct _GCupsConnectionSelector {
	GtkVBox base;

	GladeXML	*xml;
	gboolean	 updating;
};

typedef struct {
	GtkVBoxClass	base;
	void (*changed)     (GCupsConnectionSelector *cs);
	void (*model_guess) (GCupsConnectionSelector *cs, char const *model);
} GCupsConnectionSelectorClass;

/* Signals */
enum {
	CHANGED,
	MODEL_GUESS,
	LAST_SIGNAL
};

/* Keep in sync with the connection_types menu */
typedef enum {
	GCUPS_CONNECTION_IPP,
	GCUPS_CONNECTION_SMB,
	GCUPS_CONNECTION_LPD,
	GCUPS_CONNECTION_HP,
	GCUPS_CONNECTION_LOCAL,

	GCUPS_CONNECTION_UNKNOWN
} GCupsConnectionType;

enum {
	LOCAL_DETECTED_LABEL,
	LOCAL_DETECTED_PRINTER,
	LAST_LOCAL_DETECTED
};

typedef struct {
	char const *cannonical_name;
	char const *corporate_icon;
	GHashTable *aliases;
} Vendor;
static guint signals [LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE (GCupsConnectionSelector, gcups_connection_selector, GTK_TYPE_VBOX)

static char *
entry_get_text_stripped (GladeXML *xml, char const *name)
{
	GtkWidget *w =  glade_xml_get_widget (xml, name);
	char const *content;

	if (GTK_IS_COMBO_BOX_ENTRY (w))
		/* cheesy hack, we should be able to do this more gracefully */
		w = gtk_bin_get_child (GTK_BIN (w));

	content = gtk_entry_get_text (GTK_ENTRY (w));
	if (content != NULL)
		return g_strstrip (g_strdup (content));
	return NULL;
}

static gboolean 
toggle_button_is_active (GladeXML *xml, const char *name)
{
	GtkWidget *widget = glade_xml_get_widget (xml, name);
	return gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget));
}

static gboolean
tree_model_select_by_val (GtkTreeModel *model, int col, gpointer target,
			  GtkTreeIter *iter)
{
	gpointer      tmp;

	if (!gtk_tree_model_get_iter_first (model, iter))
		return FALSE;
	do {
		gtk_tree_model_get (model, iter, col, &tmp, -1);
		if (tmp != NULL && target != NULL && 0 == strcmp (tmp, target))
			return TRUE;
	} while (gtk_tree_model_iter_next (model, iter));
	return FALSE;
}

static GtkEntry *
combobox_entry_get_entry (GtkWidget *w)
{
	/* cheesy hack, we should be able to do this more gracefully */
	return (GtkEntry *) gtk_bin_get_child (GTK_BIN (w));
}

/*****************************************************************************/

typedef struct {
	char *label;
	char *uri;
	char *vendor_and_model;
} LocalPrinter;

static void
local_printer_free (LocalPrinter *printer)
{
	g_free (printer->label);
	g_free (printer->uri);
	g_free (printer->vendor_and_model);
	g_free (printer);
}

static void
local_printer_list_free (GSList *list)
{
	g_slist_foreach (list, (GFunc)local_printer_free, NULL);
	g_slist_free (list);
}
/*************************************************************************/
/* Local Printers */

static void
local_port_init (GladeXML *xml, GSList *devices)
{
	char *label;
	GSList		*ptr;
	LocalPrinter	*desc;
	GtkTreeIter	 iter;
        GtkComboBox	*combo;
        GtkCellRenderer *renderer;
	GtkListStore	*model = gtk_list_store_new (2,
		G_TYPE_STRING, G_TYPE_POINTER);

	for (ptr = devices ; ptr != NULL; ptr = ptr->next) {
		desc = ptr->data;
		label = (desc->vendor_and_model)
			? g_strdup_printf ("%s (%s)",
					   desc->label,
					   desc->vendor_and_model)
			: g_strdup (desc->label);
		gtk_list_store_append (model, &iter);
		gtk_list_store_set (model, &iter, 0, label, 1, desc, -1);
		g_free (label);
	}
	combo = GTK_COMBO_BOX (glade_xml_get_widget (xml, "local_ports")),
	gtk_combo_box_set_model (combo, GTK_TREE_MODEL (model));
	gtk_combo_box_set_active (combo, -1);
	if (devices != NULL)
		gtk_combo_box_set_active (combo, 0);

        renderer = gtk_cell_renderer_text_new ();
        gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, TRUE);
        gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo), renderer,
		"text", 0, NULL);
}

static void
update_local_location_sensitivities (GladeXML *xml)
{
	GtkWidget *view = glade_xml_get_widget (xml, "local_detected_view");
	GtkWidget *local_ports = glade_xml_get_widget (xml, "local_ports");

	if (toggle_button_is_active (xml, "local_use_detected_radio")) {
		gtk_widget_set_sensitive (view, TRUE);
		gtk_widget_set_sensitive (local_ports, FALSE);
	} else {
		gtk_widget_set_sensitive (view, FALSE);
		gtk_widget_set_sensitive (local_ports, TRUE);
	}
}

static LocalPrinter *
parse_cups_backend_device_list (char **in)
{
	LocalPrinter *desc = NULL;
	char *str, *inptr;
	int qstring;
	
	inptr = *in;
	if (*inptr == '\0')
		return NULL;
	
	str = inptr;
	while (*inptr != '\n' && *inptr != '\0' && *inptr != ' ')
		inptr++;
	
	/* first token for all local printers is 'direct' */
	if (strncmp (str, "direct", inptr - str) != 0 || *inptr != ' ')
		return NULL;
	
	inptr++;
	
	/* extract URI */
	if (*inptr == '"') {
		qstring = TRUE;
		inptr++;
	} else
		qstring = FALSE;
	
	str = inptr;
	while (*inptr != '\n' && *inptr != '\0' && (qstring ? *inptr != '"' : *inptr != ' '))
		inptr++;
	
	if (qstring && (*inptr != '"' || *(inptr + 1) != ' '))
		goto exception;
	else if (!qstring && *inptr != ' ')
		goto exception;
	
	desc = g_new0 (LocalPrinter, 1);
	desc->uri = g_strndup (str, inptr - str);
	
	if (qstring)
		inptr++;
	inptr++;
	
	/* extract vendor */
	if (*inptr == '"') {
		qstring = TRUE;
		inptr++;
	} else
		qstring = FALSE;
	
	str = inptr;
	while (*inptr != '\n' && *inptr != '\0' && (qstring ? *inptr != '"' : *inptr != ' '))
		inptr++;
	
	if (qstring && (*inptr != '"' || *(inptr + 1) != ' '))
		goto exception;
	else if (!qstring && *inptr != ' ')
		goto exception;
	
	desc->vendor_and_model = g_strndup (str, inptr - str);
	
	if (qstring)
		inptr++;
	inptr++;
	
	/* extract model */
	if (*inptr == '"') {
		qstring = TRUE;
		inptr++;
	} else
		qstring = FALSE;
	
	str = inptr;
	while (*inptr != '\n' && *inptr != '\0' && (qstring ? *inptr != '"' : *inptr != ' '))
		inptr++;
	
	if (qstring && *inptr != '"')
		goto exception;
	
	desc->label = g_strndup (str, inptr - str);
	
	while (*inptr != '\0' && *inptr != '\n')
		inptr++;
	
	if (*inptr == '\n')
		inptr++;
	
	*in = inptr;
	
	return desc;
	
 exception:
	
	local_printer_free (desc);
	
	return NULL;
}

static GSList *
get_cups_backend_devices (const char *backend)
{
	char *argv[2], *path, *stdout, *inptr;
	LocalPrinter *desc;
	GSList *ret = NULL;
	
	path = g_alloca (sizeof (CUPS_BACKENDS_DIR) + sizeof (G_DIR_SEPARATOR_S) + strlen (backend) + 1);
	sprintf (path, "%s%s%s", CUPS_BACKENDS_DIR, G_DIR_SEPARATOR_S, backend);
	
	argv[0] = path;
	argv[1] = NULL;
	
	if (!g_spawn_sync (NULL, argv, NULL, G_SPAWN_STDERR_TO_DEV_NULL,
			   NULL, NULL, &stdout, NULL, NULL, NULL))
		return NULL;
	
	inptr = stdout;
	while ((desc = parse_cups_backend_device_list (&inptr))) {
		if (!strcmp (backend, "hal") && !strcmp (desc->uri, "hal")) {
			/* this is just a dummy hal printer device, no more real detected printers after this */
			local_printer_free (desc);
			break;
		} else if (!strcmp (backend, "usb") && !strncmp (desc->uri, "usb:/dev", 8)) {
			/* this is just a dummy usb printer device, no more real detected printers after this */
			local_printer_free (desc);
			break;
		}
		
		ret = g_slist_prepend (ret, desc);
	}
	
	g_free (stdout);
	
	return ret;
}

static GSList *
get_local_devices (void)
{
	ipp_t *request, *response;
	GHashTable *device_hash;
	GSList *devices, *l;
	
	device_hash = g_hash_table_new (g_str_hash, g_str_equal);
	
	/* NOTE: direct querying of the CUPS backends is done because
	 * CUPS is broken and needs to be restarted after adding a usb
	 * printer otherwise the printer won't be detected. */
	
	/* prefer the hal backend over the usb backend */
	if (!(devices = get_cups_backend_devices ("hal")))
		devices = get_cups_backend_devices ("usb");
	
	l = devices;
	while (l != NULL) {
		g_hash_table_insert (device_hash, ((LocalPrinter *) l->data)->uri, l->data);
		l = l->next;
	}
	
	request = gnome_cups_request_new (CUPS_GET_DEVICES);
	response = gnome_cups_request_execute (request, NULL, "/", NULL);
	
	if (response) {
		ipp_attribute_t *attr;
		char *device_class = NULL;
		LocalPrinter *desc;
		
		desc = g_new0 (LocalPrinter, 1);
		for (attr = response->attrs; attr != NULL; attr = attr->next) {
			if (attr->name == NULL) {
				if (device_class && strcmp (device_class, "network") && desc->label
				    && desc->uri && !g_hash_table_lookup (device_hash, desc->uri)) {
					devices = g_slist_prepend (devices, desc);
				} else {
					local_printer_free (desc);
				}
				
				g_free (device_class);
				device_class = NULL;
				
				desc = g_new0 (LocalPrinter, 1);
			} else if (!strcmp (attr->name, "device-class")) {
				g_free (device_class);
				device_class = g_strdup (attr->values[0].string.text);
			} else if (!strcmp (attr->name, "device-info")) {
				g_free (desc->label);
				desc->label = g_strdup (attr->values[0].string.text);
			} else if (!strcmp (attr->name, "device-uri")) {
				g_free (desc->uri);
				desc->uri = g_strdup (attr->values[0].string.text);
			} else if (!strcmp (attr->name, "device-make-and-model") && strcmp (attr->values[0].string.text, "Unknown")) {
				g_free (desc->vendor_and_model);
				desc->vendor_and_model = g_strdup (attr->values[0].string.text);
			}
		}
		
		if (device_class && strcmp (device_class, "network") && desc->label
		    && desc->uri && !g_hash_table_lookup (device_hash, desc->uri)) {
			devices = g_slist_prepend (devices, desc);
		} else {
			local_printer_free (desc);
		}
		
		g_free (device_class);
		
		ippDelete (response);
	}
	
	g_hash_table_destroy (device_hash);
	
	return g_slist_reverse (devices);
}

/****************************************************************************/

static void
cb_connection_changed (GCupsConnectionSelector *cs)
{
	if (!cs->updating)
		g_signal_emit (G_OBJECT (cs), signals [CHANGED], 0);
}

static void
watch_for_change (GCupsConnectionSelector *cs, char const *name)
{
	GtkWidget *w = glade_xml_get_widget (cs->xml, name);
	if (GTK_IS_TOGGLE_BUTTON (w))
		g_signal_connect_swapped (w,
			"toggled",
			G_CALLBACK (cb_connection_changed), cs);
	else if (GTK_IS_EDITABLE (w) || GTK_IS_COMBO_BOX (w)) {
		g_signal_connect_swapped (w,
			"changed",
			G_CALLBACK (cb_connection_changed), cs);
		if (GTK_IS_COMBO_BOX_ENTRY (w))
		/* cheesy hack, we should be able to do this more gracefully */
			g_signal_connect_swapped (gtk_bin_get_child (GTK_BIN (w)),
				"changed",
				G_CALLBACK (cb_connection_changed), cs);
	} else if (GTK_IS_TREE_VIEW (w))
		g_signal_connect_swapped (
			gtk_tree_view_get_selection (GTK_TREE_VIEW (w)),
			"changed",
			G_CALLBACK (cb_connection_changed), cs);
	else
		g_warning ("unknown widget %s", name);
}

static GCupsConnectionType
get_connection_type (GladeXML *xml)
{
	GtkWidget *w = glade_xml_get_widget (xml, "local_connect_radio");
	int tmp;

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (w)))
		return GCUPS_CONNECTION_LOCAL;

	w = glade_xml_get_widget (xml, "connection_types");
	tmp = gtk_combo_box_get_active (GTK_COMBO_BOX (w));
	if (tmp >= 0)
		return (GCupsConnectionType)tmp;
	return GCUPS_CONNECTION_IPP;
}

static char *
get_uri_ipp (GladeXML *xml)
{
	char *ret, *escaped_entry;
	char *stripped_entry = entry_get_text_stripped (xml, "ipp_uri_entry");

	if (stripped_entry == NULL || stripped_entry[0] == '\0')
		return NULL;

	escaped_entry = gnome_cups_util_escape_uri_string (stripped_entry, GNOME_CUPS_UNSAFE_HOST);
	g_free (stripped_entry);
	if (escaped_entry == NULL)
		return NULL;

	if (0 != g_ascii_strncasecmp (escaped_entry, "ipp:", 4) &&
	    0 != g_ascii_strncasecmp (escaped_entry, "http:", 5))
		ret = g_strdup_printf ("ipp://%s", escaped_entry);
	else
		ret = g_strdup (escaped_entry);
	g_free (escaped_entry);

	return ret;
}

static char *
get_uri_smb (GladeXML *xml)
{
	char *ret = NULL;
	char *host = entry_get_text_stripped (xml, "smb_host_entry");
	char *printer = entry_get_text_stripped (xml, "smb_printer_entry");
	char *username = entry_get_text_stripped (xml, "smb_username_entry");
	char *password = entry_get_text_stripped (xml, "smb_password_entry");

	if (host != NULL && host[0] && printer != NULL && printer[0]) {
		if (username != NULL && username[0])
			ret = g_strdup_printf ("smb://%s:%s@%s/%s",
				username, password, host, printer);
		else
			ret = g_strdup_printf ("smb://%s/%s", host, printer);
	}

	g_free (host);
	g_free (printer);
	g_free (username);
	g_free (password);

	return ret;
}

static char *
get_uri_lpd (GladeXML *xml)
{
	char *ret = NULL;
	char *host = entry_get_text_stripped (xml, "lpd_host_entry");
	char *queue = entry_get_text_stripped (xml, "lpd_queue_entry");

	if (host[0])
		ret = g_strdup_printf ("lpd://%s/%s", host, queue);
	g_free (host);
	g_free (queue);
	return ret;
}

static char *
get_uri_hp (GladeXML *xml)
{
	char *ret = NULL;
	char *host = entry_get_text_stripped (xml, "hp_host_entry");
	char *port = entry_get_text_stripped (xml, "hp_port_entry");

	if (port == NULL || !port[0]) {
		g_free (port);
		port = g_strdup ("9100");
	}

	if (host != NULL && host[0])
		ret = g_strdup_printf ("socket://%s:%s", host, port);

	g_free (host);
	g_free (port);

	return ret;
}

static LocalPrinter const *
get_current_local (GladeXML *xml)
{
	GtkTreeIter iter;
	GtkTreeView *tree_view	    = GTK_TREE_VIEW (glade_xml_get_widget (xml, "local_detected_view"));
	GtkTreeSelection *selection = gtk_tree_view_get_selection (tree_view);
	LocalPrinter const *lp = NULL;

	if (toggle_button_is_active (xml, "local_specify_port_radio"))
		combo_selected_get (xml, "local_ports", 1, &lp, -1);
	else if (gtk_tree_selection_get_selected (selection, NULL, &iter))
		gtk_tree_model_get (gtk_tree_view_get_model (tree_view), &iter,
				    1, &lp,
				    -1);
	return lp;
}

static char *
get_uri_local (GladeXML *xml)
{
	LocalPrinter const *lp = get_current_local (xml);
	return (lp != NULL) ? g_strdup (lp->uri) : NULL;
}


static GHashTable *smb_servers;
static GSList *new_servers = NULL;
static GSList *new_printers = NULL;
static GStaticMutex server_mutex = G_STATIC_MUTEX_INIT;
#ifdef HAVE_LIBSMBCLIENT
static GStaticMutex smb_request_mutex = G_STATIC_MUTEX_INIT;
static GStaticMutex printer_request_mutex = G_STATIC_MUTEX_INIT;

typedef struct {
	GCond *cond;
	/* In */  char *server, *share;
		gboolean keyring;
	/* Out */ char *workgroup, *id, *passwd;
} SmbReqAuth;
typedef struct {
	char *name, *path;
} SmbNewServer;
typedef struct {
	char *server, *printer;
} SmbNewPrinter;
typedef struct {
	char *server;
} SmbGetPrinters;

static SmbReqAuth *auth_req = NULL;

/***************************************************************************/
/* On the SMB side */
static void 
smb_auth_fn (char const *server, char const *share,
	     char *workgroup, int workgroup_max,
	     char *id,	      int id_len,
	     char *passwd,    int passwd_len)
{
	/* blah I need some extra user hooks here to store the keyring override
	 * flag safely */
	static gboolean cheesy_hack = FALSE;
	static gboolean used_keyring = FALSE;
	static char *prev_share = NULL;

	SmbReqAuth *req;
	gboolean new_target = prev_share == NULL ||
		0 != strcmp (prev_share, share);

	g_free (prev_share);
	prev_share = g_strdup (share);

	/* First try blank */
	if (new_target) {
		cheesy_hack = TRUE;
		return;
	}
	req = g_new0 (SmbReqAuth, 1);
	req->cond = g_cond_new ();
	req->server = g_strdup (server);
	req->share = g_strdup (share);

	/* if blank failed try keyring, and finally a dailog */
	req->keyring = cheesy_hack;
	cheesy_hack = FALSE;

	g_static_mutex_lock (&smb_request_mutex);
	if (auth_req != NULL) { g_warning ("dropping an auth req"); }
	auth_req = req;
	g_cond_wait (req->cond, g_static_mutex_get_mutex (&smb_request_mutex));

	auth_req = NULL;
	g_static_mutex_unlock (&smb_request_mutex);

	strncpy (id,	 req->id     ? req->id     : "", id_len);
	strncpy (passwd, req->passwd ? req->passwd : "", passwd_len);

	used_keyring = req->keyring;
	g_free (prev_share);
	prev_share = g_strdup (server);

	g_cond_free (req->cond);
	g_free (req->server);
	g_free (req->share);
	g_free (req->workgroup);
	g_free (req->id);
	g_free (req->passwd);
	g_free (req);
}

static gpointer
cb_smb_thread (gpointer data)
{
	struct smbc_dirent const *dirent;
	int wg_handle, serve_handle;

	if ((wg_handle = smbc_opendir ("smb://")) >= 0) {
		while ((dirent = smbc_readdir (wg_handle)))
			if (dirent->smbc_type == SMBC_WORKGROUP) {
				char *path = g_strconcat ("smb://", dirent->name, NULL);
				if ((serve_handle = smbc_opendir (path)) >= 0) {
					while ((dirent = smbc_readdir (serve_handle)))
						if (dirent->smbc_type == SMBC_SERVER) {
							char *path = g_strconcat ("smb://", dirent->name, "/", NULL);
							g_static_mutex_lock (&server_mutex);
							new_servers = g_slist_append (new_servers, g_strdup (dirent->name));
							new_servers = g_slist_append (new_servers, path);
							g_static_mutex_unlock (&server_mutex);
						}
					smbc_closedir (serve_handle);
				} else {
					g_warning ("Could not list %s : %s\n", path, strerror (errno));
				}
				g_free (path);
			}

		smbc_closedir (wg_handle);
	} else {
		g_warning ("Could not list %s : %s\n", "smb://", strerror (errno));
	}

	return NULL;
}

static gpointer
cb_smb_find_printers (char const *dir_url)
{
	struct smbc_dirent const *dirent;
	int dir_handle;

reauth :
	if ((dir_handle = smbc_opendir (dir_url)) >= 0) {
		while ((dirent = smbc_readdir (dir_handle)))
			if (dirent->smbc_type == SMBC_PRINTER_SHARE) {
				g_static_mutex_lock (&printer_request_mutex);
				new_printers = g_slist_append (new_printers, g_strdup (dir_url));
				new_printers = g_slist_append (new_printers, g_strdup (dirent->name));
				g_static_mutex_unlock (&printer_request_mutex);
			}
		smbc_closedir (dir_handle);
	} else if (dir_handle == EACCES)
		goto reauth;
	else {
		g_warning ("Could not list %s : %s\n", dir_url, strerror (errno));
	}

	return NULL;
}

/***************************************************************************/
/* On the UI side */
static void
ui_auth_req_handler (GladeXML *xml)
{
	static char *default_id = NULL;
	GList *list = NULL;
	GtkWidget *w;
	GnomePasswordDialog *dialog;
	GnomeKeyringResult result = GNOME_KEYRING_RESULT_OK;
	gboolean found = FALSE;

	g_warning ("authenticating with %s for %s", auth_req->server, auth_req->share);

	if (default_id == NULL)
		default_id = g_strdup (g_getenv ("USER"));
	if (default_id == NULL)
		default_id = g_strdup (g_getenv ("LOGNAME"));

	if (default_id != NULL && auth_req->workgroup != NULL && auth_req->keyring == TRUE)
		result = gnome_keyring_find_network_password_sync (default_id,
			auth_req->workgroup, auth_req->server, auth_req->share,
			"smb", NULL, 0, &list);
	
	if (list != NULL) {
		if (result == GNOME_KEYRING_RESULT_OK) {
			GnomeKeyringNetworkPasswordData *pwd_data = list->data;
			auth_req->id = g_strdup (pwd_data->user);
			auth_req->passwd = g_strdup (pwd_data->password);
			found = TRUE;
		}
		gnome_keyring_network_password_list_free (list);
	}

	if (!found) {
		char *msg = NULL;
		if (auth_req->server != NULL) {
			if (auth_req->workgroup != NULL)
				msg = g_strdup_printf (_("Identity and Password for %s in workgroup %s"),
					auth_req->server, auth_req->workgroup);
			else
				msg = g_strdup_printf (_("Identity and Password for %s"),
					auth_req->server);
		} else if (auth_req->workgroup != NULL)
			msg = g_strdup_printf (_("Identity and Password for workgroup %s"),
				auth_req->workgroup);
		else {
			g_warning ("huh ?? what are we authenticating for ?");
			msg = g_strdup_printf (_("Identity and Password"));
		}

		dialog = GNOME_PASSWORD_DIALOG (gnome_password_dialog_new (
			_("Authentication Required"), msg, "", "", FALSE));
		if (default_id != NULL)
			gnome_password_dialog_set_username	(dialog, default_id);
		gnome_password_dialog_set_show_username		(dialog,  TRUE);
		gnome_password_dialog_set_show_domain		(dialog, FALSE);
		gnome_password_dialog_set_show_password		(dialog,  TRUE);
		gnome_password_dialog_set_show_remember		(dialog, FALSE);
#ifdef HAVE_GNOME_PASSWORD_DIALOG_SET_SHOW_USERPASS_BUTTONS
		gnome_password_dialog_set_show_userpass_buttons (dialog, FALSE);
#endif

		auth_req->keyring = FALSE;
		if (gnome_password_dialog_run_and_block (dialog)) {
			auth_req->id     = gnome_password_dialog_get_username (dialog);
			auth_req->passwd = gnome_password_dialog_get_password (dialog);
		}
		gtk_widget_destroy (GTK_WIDGET (dialog));
		g_free (msg);
	}
	if (auth_req->id != NULL) {
		w = glade_xml_get_widget (xml, "smb_username_entry");
		gtk_entry_set_text (GTK_ENTRY (w), auth_req->id);
	}
	if (auth_req->passwd != NULL) {
		w = glade_xml_get_widget (xml, "smb_password_entry");
		gtk_entry_set_text (GTK_ENTRY (w), auth_req->passwd);
	}
}

static void
ui_add_server_handler (GladeXML *xml)
{
	GtkComboBox  *combo = GTK_COMBO_BOX (glade_xml_get_widget (xml, "smb_host_entry"));
	GtkListStore *store = (GtkListStore *) gtk_combo_box_get_model (combo);
	GtkTreeIter   iter;
	char *name, *path;

	g_assert (new_servers != NULL);
	name = new_servers->data;
	new_servers = g_slist_remove (new_servers, name);

	g_assert (new_servers != NULL); /* minor leak of name */
	path = new_servers->data;
	new_servers = g_slist_remove (new_servers, path);

	gtk_list_store_append (store, &iter);
	gtk_list_store_set (store, &iter,
		0, name,
		1, path,
		-1);
	g_free (name);
	g_free (path);
}

static void
ui_add_printer_handler (GladeXML *xml)
{
	GtkListStore *store;
	GtkTreeIter   iter;
	char *server, *printer;

	g_assert (new_printers != NULL);
	server = new_printers->data;
	new_printers = g_slist_remove (new_printers, server);

	g_assert (new_printers != NULL); /* minor leak of server */
	printer = new_printers->data;
	new_printers = g_slist_remove (new_printers, printer);

	store = g_hash_table_lookup (smb_servers, server);
	if (store != NULL) {
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter,
			0, printer,
			-1);
	} else {
		g_warning ("missing smb server model ??");
	}
	g_free (printer);
	g_free (server);
}
static gint
cb_smb_req_handler (GCupsConnectionSelector *cs)
{
	g_static_mutex_lock (&smb_request_mutex);
	if (auth_req != NULL) {
		ui_auth_req_handler (cs->xml);
		g_cond_signal (auth_req->cond);
	}
	g_static_mutex_unlock (&smb_request_mutex);

	g_static_mutex_lock (&server_mutex);
	while (new_servers != NULL)
		ui_add_server_handler (cs->xml);
	g_static_mutex_unlock (&server_mutex);

	g_static_mutex_lock (&printer_request_mutex);
	while (new_printers != NULL)
		ui_add_printer_handler (cs->xml);
	g_static_mutex_unlock (&printer_request_mutex);
	return TRUE;
}
#endif

static void
cb_smb_host_changed (GtkComboBox *combo, GladeXML *xml)
{
	GtkTreeIter   iter;

	if (gtk_combo_box_get_active_iter (combo, &iter)) {
		gboolean make_req = FALSE;
		char *path;
		GtkTreeModel *model = gtk_combo_box_get_model (combo);
		gtk_tree_model_get (model, &iter, 1, &path, -1);
		if (smb_servers == NULL)
			smb_servers = g_hash_table_new_full (g_str_hash, g_str_equal,
					       g_free, NULL);
		if (NULL == (model = g_hash_table_lookup (smb_servers, path))) {
			model = (GtkTreeModel *)gtk_list_store_new (1, G_TYPE_STRING);
			g_hash_table_insert (smb_servers, g_strdup (path), model);
			make_req = TRUE;
		}

		gtk_combo_box_set_model (
			(GtkComboBox *)glade_xml_get_widget (xml, "smb_printer_entry"),
			GTK_TREE_MODEL (model));
#ifdef HAVE_LIBSMBCLIENT
		if (make_req)
			g_thread_create ((GThreadFunc)cb_smb_find_printers,
					 path, TRUE, NULL);
#endif
	}
}

/*****************************************************************************/

static void
cb_connection_type_changed (GCupsConnectionSelector *cs)
{
	static gboolean start_smb_scan = TRUE;
	GtkWidget *w = glade_xml_get_widget (cs->xml, "local_connect_radio");
	GtkWidget *notebook = glade_xml_get_widget (cs->xml, "connection_notebook");
	GtkWidget *combo = glade_xml_get_widget (cs->xml, "connection_types");
	int t = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (w))
		? GCUPS_CONNECTION_LOCAL
		: gtk_combo_box_get_active (GTK_COMBO_BOX (combo));
	gtk_notebook_set_current_page (GTK_NOTEBOOK (notebook), t);
	gtk_widget_set_sensitive (combo, t != GCUPS_CONNECTION_LOCAL);

	if (cs->updating)
		return;

	if (start_smb_scan &&
	    get_connection_type (cs->xml) == GCUPS_CONNECTION_SMB) {
		start_smb_scan = FALSE;
#ifdef HAVE_LIBSMBCLIENT
		if (smbc_init (smb_auth_fn, 0) >= 0) {
			if (!g_thread_supported ())
				g_thread_init (NULL);
			g_thread_create (cb_smb_thread, NULL, TRUE, NULL);
			g_timeout_add (200, (GtkFunction)cb_smb_req_handler, cs);
		} else {
			g_warning ("smbc_init returned %s (%i)\nDo you have a ~/.smb/smb.conf file?\n",
				   strerror (errno), errno);
		}
#else
		g_warning ("no smb browsing support");
#endif
	}
}

static void
setup_local_connections (GCupsConnectionSelector *cs)
{
	GtkTreeView	  *tree_view;
	GtkListStore	  *list_store;
	GtkTreeSelection  *selection;
	GtkCellRenderer	  *renderer;
	GtkTreeViewColumn *column;
	int num_detected;
	GSList *ptr, *devices;

	devices = get_local_devices ();
	g_object_set_data_full (G_OBJECT (cs->xml), "local-devices", devices,
				(GDestroyNotify)local_printer_list_free);
	local_port_init (cs->xml, devices);

	tree_view = GTK_TREE_VIEW (glade_xml_get_widget (cs->xml, "local_detected_view"));

	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Printer"),
							   renderer,
							   "markup",
							   LOCAL_DETECTED_LABEL,
							   NULL);
	gtk_tree_view_append_column (tree_view, column);

	list_store = gtk_list_store_new (LAST_LOCAL_DETECTED,
					 G_TYPE_STRING, G_TYPE_POINTER);
	gtk_tree_view_set_model (tree_view, GTK_TREE_MODEL (list_store));

	selection = gtk_tree_view_get_selection (tree_view);
	num_detected = 0;
	for (ptr = devices; ptr != NULL; ptr = ptr->next) {
		LocalPrinter *desc = ptr->data;
		if (desc->vendor_and_model
		    /* weed out two that are always returned
		     * regardless of what's there */
		    && strcmp (desc->vendor_and_model, "EPSON")
		    && strcmp (desc->vendor_and_model, "CANON")) {
			GtkTreeIter iter;

			gtk_list_store_append (list_store, &iter);
			gtk_list_store_set (list_store, &iter,
					    0, desc->vendor_and_model,
					    1, desc,
					    -1);
			if (num_detected == 0)
				gtk_tree_selection_select_iter (selection, &iter);
			num_detected++;
		}
	}

	if (num_detected == 0) {
		GtkTreeIter iter;
		gtk_list_store_append (list_store, &iter);
		gtk_list_store_set (list_store, &iter,
				    0, _("<i>No printers detected</i>"),
				    1, NULL,
				    -1);

		gtk_tree_selection_set_mode (selection, GTK_SELECTION_NONE);
		gtk_tree_selection_set_mode (selection, GTK_SELECTION_NONE);
		gtk_toggle_button_set_active ((GtkToggleButton *)
			glade_xml_get_widget (cs->xml, "local_specify_port_radio"), TRUE);
	}

	update_local_location_sensitivities (cs->xml);
	g_signal_connect_swapped (glade_xml_get_widget (cs->xml, "local_use_detected_radio"),
		"toggled",
		G_CALLBACK (update_local_location_sensitivities), cs->xml);
	g_signal_connect_swapped (glade_xml_get_widget (cs->xml, "local_specify_port_radio"),
		"toggled",
		G_CALLBACK (update_local_location_sensitivities), cs->xml);

	watch_for_change (cs, "local_use_detected_radio");
	watch_for_change (cs, "local_specify_port_radio");
}

/**************************************************************************/

static void
init_smb_combos (GladeXML *xml)
{
	GtkComboBox  *combo = GTK_COMBO_BOX (glade_xml_get_widget (xml, "smb_host_entry"));
	GtkListStore *store = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_STRING);
	gtk_combo_box_set_model (combo, GTK_TREE_MODEL (store));
	gtk_combo_box_entry_set_text_column (GTK_COMBO_BOX_ENTRY (combo), 0);
	gtk_combo_box_set_active (combo, -1);
	g_signal_connect (GTK_WIDGET (combo),
		"changed",
		G_CALLBACK (cb_smb_host_changed), xml);

	combo = GTK_COMBO_BOX (glade_xml_get_widget (xml, "smb_printer_entry"));
	store = gtk_list_store_new (1, G_TYPE_STRING);
	gtk_combo_box_set_model (combo, GTK_TREE_MODEL (store));
	gtk_combo_box_entry_set_text_column (GTK_COMBO_BOX_ENTRY (combo), 0);
	gtk_combo_box_set_active (combo, -1);
}

/**************************************************************************/

static void
setup_network_connections (GCupsConnectionSelector *cs)
{
	GtkWidget *w = glade_xml_get_widget (cs->xml, "connection_types");
	gtk_combo_box_set_active (GTK_COMBO_BOX (w), GCUPS_CONNECTION_IPP);

	init_smb_combos (cs->xml);

	watch_for_change (cs, "local_connect_radio");
	watch_for_change (cs, "connection_types");
	watch_for_change (cs, "lpd_host_entry");
	watch_for_change (cs, "smb_username_entry");
	watch_for_change (cs, "smb_password_entry");
	watch_for_change (cs, "smb_printer_entry");
	watch_for_change (cs, "smb_host_entry");
	watch_for_change (cs, "ipp_uri_entry");
	watch_for_change (cs, "hp_host_entry");
	watch_for_change (cs, "hp_port_entry");
	cs->updating = TRUE;
	cb_connection_type_changed (cs);
	cs->updating = FALSE;
	g_signal_connect_swapped (glade_xml_get_widget (cs->xml, "connection_types"),
		"changed",
		G_CALLBACK (cb_connection_type_changed), cs);
	g_signal_connect_swapped (glade_xml_get_widget (cs->xml, "local_connect_radio"),
		"toggled",
		G_CALLBACK (cb_connection_type_changed), cs);

	w = glade_xml_get_widget (cs->xml, "ipp_uri_entry");
	gtk_tooltips_set_tip (gtk_tooltips_new (), w,
			      _("For example :\n"
				"\thttp://hostname:631/ipp/\n"
				"\thttp://hostname:631/ipp/port1\n"
				"\tipp://hostname/ipp/\n"
				"\tipp://hostname/ipp/port1"), NULL);
}

static void
gcups_connection_selector_class_init (GCupsConnectionSelectorClass *obj_klass)
{
	signals [CHANGED] = g_signal_new ("changed",
		G_OBJECT_CLASS_TYPE (obj_klass), G_SIGNAL_RUN_LAST,
		G_STRUCT_OFFSET (GCupsConnectionSelectorClass, changed),
		NULL, NULL, g_cclosure_marshal_VOID__VOID,
		G_TYPE_NONE, 0, G_TYPE_NONE);
	signals [MODEL_GUESS] = g_signal_new ("model-guess",
		G_OBJECT_CLASS_TYPE (obj_klass), G_SIGNAL_RUN_LAST,
		G_STRUCT_OFFSET (GCupsConnectionSelectorClass, model_guess),
		NULL, NULL, g_cclosure_marshal_VOID__STRING,
		G_TYPE_NONE, 1, G_TYPE_STRING);
}

static void
cb_ipp_model (guint id, const char *path, ipp_t *response, GError **error,
	      gpointer cs)
{
	ipp_attribute_t *attr;

	if (!error && response)
		for (attr = response->attrs; attr != NULL; attr = attr->next)
			if (attr->name != NULL &&
			    !g_ascii_strcasecmp (attr->name, "printer-make-and-model")) {
				g_warning ("Found a %s", attr->values[0].string.text);
				g_signal_emit (G_OBJECT (cs), signals [MODEL_GUESS], 0, 
					attr->values[0].string.text);
			}

	ippDelete (response);
	g_clear_error (error);
}

static char *
parse_network_detect (char const *line)
{
	char *raw_line = g_strdup (line);
	char *vendor_and_model;
	char *vendor = NULL;
	char *model = NULL;
	char **fields;
	char **p;
	char *sep;

	g_warning ("snmp result == '%s'", line);

	sep = strchr (line, '\n');
	if (sep) {
		*sep = '\0';
	}

	line = g_strstrip (raw_line);
	fields = g_strsplit (line, ";", -1);

	for (p = fields; *p != NULL; p++) {
		char **pieces = g_strsplit (*p, "=", -1);
		char *name = pieces[0];
		char *value = pieces[1];
		if (!name || !value) {
			g_strfreev (pieces);
			continue;
		}

		if (!strcmp (name, "vendor")) {
			vendor = g_strdup (value);
		} else if (!strcmp (name, "model")) {
			model = g_strdup (value);
		}

		g_strfreev (pieces);
	}
	g_strfreev (fields);

	if (!vendor || !model) {
		g_free (raw_line);
		g_free (vendor);
		g_free (model);
		return NULL;
	}

	vendor_and_model = (strstr (model, vendor) == model)
		? g_strdup (model) : g_strdup_printf ("%s %s", vendor, model);

	g_free (vendor);
	g_free (model);
	g_free (raw_line);

	return vendor_and_model;
}

static void
gcups_connection_selector_init (GCupsConnectionSelector *cs)
{
	GtkWidget *w;

	cs->updating = FALSE;
	cs->xml = glade_xml_new (GNOME_CUPS_MANAGER_DATADIR "/gnome-cups-ui-connection.glade",
				 "gcups_connection_selector", GETTEXT_PACKAGE);

	setup_network_connections (cs);
	setup_local_connections (cs);

	w = glade_xml_get_widget (cs->xml, "gcups_connection_selector");
	gtk_container_add (GTK_CONTAINER (cs), w);
	gtk_widget_show (GTK_WIDGET (cs));
}

GtkWidget *
gcups_connection_selector_new (void)
{
	return g_object_new (gcups_connection_selector_get_type (), NULL);
}

char *
gcups_connection_selector_get_uri (GCupsConnectionSelector *cs)
{
	switch (get_connection_type (cs->xml)) {
	case GCUPS_CONNECTION_IPP :	return get_uri_ipp (cs->xml);
	case GCUPS_CONNECTION_SMB :	return get_uri_smb (cs->xml);
	case GCUPS_CONNECTION_LPD :	return get_uri_lpd (cs->xml);
	case GCUPS_CONNECTION_HP :	return get_uri_hp (cs->xml);
	case GCUPS_CONNECTION_LOCAL :	return get_uri_local (cs->xml);

	default :
		g_warning ("unsupported type\n");
	}

	return NULL;
}

void
gcups_connection_selector_queue_guess (GCupsConnectionSelector *cs)
{
	char *nickname = NULL;
	char *uri = gcups_connection_selector_get_uri (cs);

	if (uri == NULL)
		return;

	switch (get_connection_type (cs->xml)) {
	case GCUPS_CONNECTION_IPP : {
		static char const *attrs[] = {
			"printer-make-and-model"
		};
		ipp_t *request = gnome_cups_request_new (IPP_GET_PRINTER_ATTRIBUTES);
		gnome_cups_request_add_requested_attributes (request, 
			IPP_TAG_OPERATION, G_N_ELEMENTS (attrs), (char**)attrs);
		gnome_cups_request_execute_async (request, uri, "/",
			cb_ipp_model, g_object_ref (cs->xml), g_object_unref);
#if 0
		snprintf(uri, sizeof(uri), "ipp://%s/printers/%s", printer);
		ippAddString(request, IPP_TAG_OPERATION, IPP_TAG_URI,
			     "printer-uri", NULL, uri);
		ippAddStrings(request, IPP_TAG_OPERATION, IPP_TAG_NAME,
			      "requested-attributes",
			      G_N_ELEMENTS (attrs), NULL, attrs);
#endif

		break;
	}

	case GCUPS_CONNECTION_LPD : {
		int snmp_err = 0;
		char *host = entry_get_text_stripped (cs->xml, "lpd_host_entry");
		if (host != NULL) {
			char const *detected = get_snmp_printers (host, &snmp_err);
			if (NULL != detected && 0 == snmp_err)
				nickname = parse_network_detect (detected);
			g_free (host);
		}
		break;
	}

	case GCUPS_CONNECTION_LOCAL : {
		LocalPrinter const *lp = get_current_local (cs->xml);
		if (lp != NULL)
			nickname = g_strdup (lp->vendor_and_model);
		break;
	}

	default :
		break;
	}

	/* if we can not autorecognize the printer we may be able to
	 * use an existing instance of it to look up a driver */
	if (nickname == NULL) {
#if 0
		ipp_t *request = gnome_cups_request_new (IPP_GET_PRINTER_ATTRIBUTES);
		gnome_cups_request_add_requested_attributes (request, 
			IPP_TAG_OPERATION, G_N_ELEMENTS (attrs), (char**)attrs);
		gnome_cups_request_execute_async (request, uri, "/",
			cb_ipp_model, g_object_ref (cs->xml), g_object_unref);
#endif
	}
	g_free (uri);

	g_signal_emit (G_OBJECT (cs), signals [MODEL_GUESS], 0, nickname);
	g_free (nickname);
}


struct URI {
	char *protocol;
	char *domain;
	char *user;
	char *passwd;
	char *host;
	int port;
	char *path;
};

#define HEXVAL(c) (isdigit (c) ? (c) - '0' : tolower (c) - 'a' + 10)

static void
url_decode (unsigned char *in, const char *url)
{
	register unsigned char *inptr, *outptr;
	
	inptr = outptr = in;
	while (*inptr) {
		if (*inptr == '%') {
			if (isxdigit ((int) inptr[1]) && isxdigit ((int) inptr[2])) {
				*outptr++ = HEXVAL (inptr[1]) * 16 + HEXVAL (inptr[2]);
				inptr += 3;
			} else {
				g_warning ("Invalid encoding in url: %s at %s", url, inptr);
				*outptr++ = *inptr++;
			}
		} else
			*outptr++ = *inptr++;
	}
	
	*outptr = '\0';
}

static struct URI *
parse_printer_uri (const char *in)
{
	/* smb://[[domain;]user[:password]@]server/share/path/path/path */
	register const char *start, *inptr;
	char *user = NULL;
	struct URI *uri;
	
	if (!(inptr = strchr ((start = in), ':')))
		return NULL;
	
	uri = g_new0 (struct URI, 1);
	uri->protocol = g_ascii_strdown (start, inptr - start);
	
	inptr++;
	
	if (*inptr == '\0')
		return uri;
	
	if (!strncmp (inptr, "//", 2))
		inptr += 2;
	
	start = inptr;
	while (*inptr && *inptr != ';' && *inptr != ':' && *inptr != '@' && *inptr != '/')
		inptr++;
	
	switch (*inptr) {
	case ';': /* domain;user */
		if (inptr - start)
			uri->domain = g_strndup (start, inptr - start);
		
		inptr++;
		start = inptr;
		while (*inptr && *inptr != ':' && *inptr != '@')
			inptr++;
	case ':': /* user:passwd or host:port */
	case '@': /* user@host */
		if (inptr - start) {
			user = g_strndup (start, inptr - start);
			url_decode (user, in);
		}
		
		switch (*inptr) {
		case ':': /* user:passwd@ or host:port */
			inptr++;
			start = inptr;
			
			while (*inptr && *inptr != '@' && *inptr != '/')
				inptr++;
			
			if (*inptr == '@') {
				/* user:passwd@ */
				uri->user = user;
				user = NULL;
				
				if (inptr - start) {
					uri->passwd = g_strndup (start, inptr - start);
					url_decode (uri->passwd, in);
				}
				
				inptr++;
				start = inptr;
				
				goto decode_host;
			} else {
				/* host:port */
				uri->host = user;
				user = NULL;
				
				inptr = start;
				
				goto decode_port;
			}
			
			break;
		case '@': /* user@host */
			uri->user = user;
			user = NULL;
			
			inptr++;
			start = inptr;
		decode_host:
			while (*inptr && *inptr != ':' && *inptr != '/')
				inptr++;
			
			if (*inptr == '/' && inptr[-1] == '.')
				inptr--;
			
			if (inptr - start)
				uri->host = g_strndup (start, inptr - start);
			
			if (*inptr == ':') {
				inptr++;
			decode_port:
				uri->port = 0;
				
				while (*inptr >= '0' && *inptr <= '9' && uri->port < 6554)
					uri->port = (uri->port * 10) + ((*inptr++) - '0');
				
				if (uri->port > 65535) {
					/* chop off the last digit */
					uri->port /= 10;
				}
				
				if (*inptr && *inptr != '/') {
					while (*inptr && *inptr != '/')
						inptr++;
				}
			}
			
			while (*inptr && *inptr != ':' && *inptr != '/')
				inptr++;
		}
		break;
	case '/': /* host/path */
		if (inptr - start)
			uri->host = g_strndup (start, inptr - start);
		break;
	case '\0':
		/* host */
		if (*start != '\0')
			uri->host = g_strdup (start);
	default:
		break;
	}
	
	if (*inptr != '\0') {
		uri->path = g_strdup (inptr);
		url_decode (uri->path, in);
	}
	
	return uri;
}

void
gcups_connection_selector_set_uri (GCupsConnectionSelector *cs, char const *uri)
{
	GCupsConnectionType type = GCUPS_CONNECTION_UNKNOWN;
	LocalPrinter const *desc = NULL;
	struct URI *url = NULL;
	const char *resource;
	GtkWidget *w;
	GSList *ptr;
	
	g_return_if_fail (uri != NULL);
	g_return_if_fail (!cs->updating);

	cs->updating = TRUE;

	g_warning ("connect = '%s'", uri);

	/* Check for local devices first */
	ptr = g_object_get_data (G_OBJECT (cs->xml), "local-devices");
	for (; ptr != NULL ; ptr = ptr->next) {
		desc = ptr->data;
		if (desc->uri && !strcmp (uri, desc->uri))
			break;
	}

	if (ptr == NULL) {
		url = parse_printer_uri (uri);
#if 1
		g_warning ("\nprotocol: '%s'\n"
			   "domain:     '%s'\n"
			   "user:       '%s'\n"
			   "passwd:     '%s'\n"
			   "host:       '%s'\n"
			   "port:       %d\n"
			   "resource:   '%s'\n",
			   url->protocol,
			   url->domain ? url->domain : "",
			   url->user ? url->user : "",
			   url->passwd ? url->passwd : "",
			   url->host ? url->host : "",
			   url->port,
			   url->path ? url->path : "");
		
		resource = url->path ? url->path + ((url->path[0] == '/') ? 1 : 0) : NULL;
#endif
	}

	if (ptr != NULL) {
		GtkTreeIter  iter;

		type = GCUPS_CONNECTION_LOCAL;
		w = glade_xml_get_widget (cs->xml, "local_detected_view");
		if (tree_model_select_by_val (
				gtk_tree_view_get_model (GTK_TREE_VIEW (w)),
				1, (gpointer)desc, &iter)) {
			gtk_tree_selection_select_iter (
				gtk_tree_view_get_selection (GTK_TREE_VIEW (w)),
				&iter);
			w = glade_xml_get_widget (cs->xml, "local_use_detected_radio");
		} else {
			w = glade_xml_get_widget (cs->xml, "local_ports");
			if (tree_model_select_by_val (
					gtk_combo_box_get_model (GTK_COMBO_BOX (w)),
					1, (gpointer)desc, &iter))
				gtk_combo_box_set_active_iter (GTK_COMBO_BOX (w), &iter);
			w = glade_xml_get_widget (cs->xml, "local_specify_port_radio");
		}
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), TRUE);
	} else if (!strcmp (url->protocol, "smb")) {
		type = GCUPS_CONNECTION_SMB;
		w =  glade_xml_get_widget (cs->xml, "smb_host_entry");
		gtk_entry_set_text (combobox_entry_get_entry (w), url->host ? url->host : "");
		w =  glade_xml_get_widget (cs->xml, "smb_printer_entry");
		gtk_entry_set_text (combobox_entry_get_entry (w), resource ? resource : "");
		w =  glade_xml_get_widget (cs->xml, "smb_username_entry");
		gtk_entry_set_text (GTK_ENTRY (w), url->user ? url->user : "");
		w =  glade_xml_get_widget (cs->xml, "smb_password_entry");
		gtk_entry_set_text (GTK_ENTRY (w), url->passwd ? url->passwd : "");
	} else if (!strcmp (url->protocol, "lpd")) {
		type = GCUPS_CONNECTION_LPD;

		/* "lpd://%s/%s" */
		w =  glade_xml_get_widget (cs->xml, "lpd_host_entry");
		gtk_entry_set_text (GTK_ENTRY (w), url->host ? url->host : "");
		w =  glade_xml_get_widget (cs->xml, "lpd_queue_entry");
		gtk_entry_set_text (GTK_ENTRY (w), resource ? resource : "");
	} else if (!strcmp (url->protocol, "socket")) {
		type = GCUPS_CONNECTION_HP;

		/* "socket://%s:%s"  */
		w =  glade_xml_get_widget (cs->xml, "hp_host_entry");
		gtk_entry_set_text (GTK_ENTRY (w), url->host ? url->host : "");

		if (url->port > 0) {
			char *tmp = g_strdup_printf ("%d", url->port);
			w =  glade_xml_get_widget (cs->xml, "hp_port_entry");
			gtk_entry_set_text (GTK_ENTRY (w), tmp);
			g_free (tmp);
		}
	} else { /* pretend everything else is IPP */
		char *user_uri = gnome_cups_util_unescape_uri_string (uri);
		w =  glade_xml_get_widget (cs->xml, "ipp_uri_entry");
		gtk_entry_set_text (GTK_ENTRY (w), user_uri);
		g_free (user_uri);
		type = GCUPS_CONNECTION_IPP;
	}

	if (type == GCUPS_CONNECTION_LOCAL) {
		w  = glade_xml_get_widget (cs->xml, "local_connect_radio");
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), TRUE);
	} else {
		w  = glade_xml_get_widget (cs->xml, "network_connect_radio");
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), TRUE);
		w  = glade_xml_get_widget (cs->xml, "connection_types");
		gtk_combo_box_set_active (GTK_COMBO_BOX (w), type);
	}
	cs->updating = FALSE;
	
	if (url != NULL) {
		g_free (url->protocol);
		g_free (url->domain);
		g_free (url->user);
		g_free (url->passwd);
		g_free (url->host);
		g_free (url->path);
		g_free (url);
	}
}
