#include "config.h"
#include <libgnomecups/gnome-cups-init.h>
#include <libgnomecups/gnome-cups-printer.h>
#include "gnome-cups-ui-init.h"
#include "gnome-cups-permission.h"
#include "gnome-cups-i18n.h"

#include <gtk/gtk.h>

static gboolean initialized = FALSE;

static void
password_activate_cb (GtkWidget *entry, gpointer user_data)
{
	GtkDialog *dialog;
	
	dialog = GTK_DIALOG (user_data);
	
	gtk_dialog_response (dialog, GTK_RESPONSE_OK);
}

static gboolean
gcups_password_cb (const char           *prompt,
		   char                **username,
		   char                **password,
		   GnomeCupsAuthContext *ctxt)
{
	GtkWidget *dialog;
	GtkWidget *label;
	GtkWidget *user_entry;
	GtkWidget *password_entry;
	GtkWidget *box;
	GtkSizeGroup *size_group;
	gboolean ret;
	int response;

	/* FIXME: This should grab and stuff.  ugh */
	g_warning ("BROKEN : Something is attempting to authenticate with a prompt '%s'",
		   prompt);
	return FALSE;
	
	dialog = gtk_dialog_new_with_buttons (_("Password"),
					      NULL, 
					      GTK_DIALOG_MODAL,
					      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					      GTK_STOCK_OK, GTK_RESPONSE_OK,
					      NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);

	label = gtk_label_new (_("Enter a username and password to modify this printer:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), label,
			    FALSE, FALSE, 4);

	size_group = gtk_size_group_new (GTK_SIZE_GROUP_BOTH);
	label = gtk_label_new (_("Username: "));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_size_group_add_widget (size_group, label);
	user_entry = gtk_entry_new ();
	gtk_entry_set_text (GTK_ENTRY (user_entry), "root");

	box = gtk_hbox_new (FALSE, 4);
	gtk_box_pack_start (GTK_BOX (box), label, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (box), user_entry, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), box,
			    FALSE, FALSE, 4);

	label = gtk_label_new (_("Password: "));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_size_group_add_widget (size_group, label);
	password_entry = gtk_entry_new ();
	gtk_entry_set_visibility (GTK_ENTRY (password_entry), FALSE);
	g_signal_connect (password_entry, "activate", 
			  G_CALLBACK (password_activate_cb), dialog);

	box = gtk_hbox_new (FALSE, 4);
	gtk_box_pack_start (GTK_BOX (box), label, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (box), password_entry, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), box,
			    FALSE, FALSE, 4);

	gtk_widget_grab_focus (GTK_WIDGET (password_entry));

	gtk_widget_show_all (dialog);
	response = gtk_dialog_run (GTK_DIALOG (dialog));

	if (response == GTK_RESPONSE_OK) {
		*username = g_strdup (gtk_entry_get_text (GTK_ENTRY (user_entry)));
		*password = g_strdup (gtk_entry_get_text (GTK_ENTRY (password_entry)));
		ret = TRUE;
	} else {
		ret = FALSE;
	}

	gtk_widget_destroy (dialog);
	
	return ret;
}

void
gnome_cups_ui_init (char const *argv_0)
{
	if (initialized) {
		return;
	}

	initialized = TRUE;

	gnome_cups_init (gcups_password_cb);
	gnome_cups_checkpath (argv_0);
}

