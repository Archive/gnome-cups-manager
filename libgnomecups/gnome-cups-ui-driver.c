/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

/*
 * gnome-cups-ui-driver.c: A printer driver selector
 *
 * Copyright (C) 2004 Jody Goldberg (jody@gnome.org)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include <stdlib.h>

#include "gnome-cups-ui-driver.h"
#include "gnome-cups-ui-util.h"
#include <libgnomecups/gnome-cups-request.h>

#include <glib.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <glib/gi18n.h>
#include <stdio.h>
#include <errno.h>

struct _GCupsDriverSelector {
	GtkVBox	base;
	GladeXML	*xml;
	GHashTable	*ppds;
	GHashTable	*vendors;
};

typedef struct {
	GtkVBoxClass	base;
	void (*changed) (GCupsDriverSelector *ds);
} GCupsDriverSelectorClass;

/* Signals */
enum {
	CHANGED,
	LAST_SIGNAL
};

static guint signals [LAST_SIGNAL] = { 0 };

G_DEFINE_TYPE (GCupsDriverSelector, gcups_driver_selector, GTK_TYPE_VBOX)

static gboolean
combo_select (GladeXML *xml, char const *name, int col, gconstpointer target)
{
	gpointer	 tmp;
	GtkTreeIter	 iter;
	GtkComboBox	*combo = (GtkComboBox *)glade_xml_get_widget (xml, name);
	GtkTreeModel	*model = gtk_combo_box_get_model (combo);

	if (!gtk_tree_model_get_iter_first (model, &iter))
		return FALSE;

	do {
		gtk_tree_model_get (model, &iter, col, &tmp, -1);
		if (tmp != NULL && target != NULL && 0 == strcmp (tmp, target)) {
			gtk_combo_box_set_active_iter (combo, &iter);
			return TRUE;
		}
	} while (gtk_tree_model_iter_next (model, &iter));
	return FALSE;
}

static void
scroll_to_iter (GtkTreeView *view, GtkTreeModel *model, GtkTreeIter *iter)
{
	GtkTreePath *path;

	path = gtk_tree_model_get_path (model, iter);

	gtk_tree_view_scroll_to_cell (view,
				      path,
				      NULL,
				      FALSE,
				      0.0, 0.0);
	gtk_tree_path_free (path);
}

static void
collect_key_func (gpointer key, gpointer value, gpointer user_data)
{
	GSList **l = user_data;

	*l = g_slist_prepend (*l, key);
}

static GSList *
gnome_cups_hash_table_keys (GHashTable *hash)
{
	GSList *ret = NULL;

	g_hash_table_foreach (hash, collect_key_func, &ret);

	return ret;
}

/*****************************************************************************/
/*
 * PPDs are taken from cups and filtered into the following data structures:
 *
 * ppds (GHashTable *) : all of the GCupsPPD files hashed by nickname
 *
 * vendors (GHashTable *)
 *    "vendor1" -> ...
 *    "vendor2" -> ...
 *    "vendor3" -> models (GHashTable *)
 *                   "model 1" -> ...
 *                   "model 2" -> ...
 *                   "model 3" -> ppds (GSList *)
 *                                  GCupsPPD *
 *                                  GCupsPPD *
 *                                  GCupsPPD *
 *
 */

static ipp_t *
ppd_request (void)
{
	static char const *attrs[] = {
		"ppd-name",
		"ppd-make",
		"ppd-make-and-model",
	};
	ipp_t *request = gnome_cups_request_new (CUPS_GET_PPDS);
	gnome_cups_request_add_requested_attributes (request, IPP_TAG_PRINTER,
		G_N_ELEMENTS (attrs), (char **)attrs);
	return gnome_cups_request_execute (request, NULL, "/", NULL);
}

static GCupsPPD *
ppd_new ()
{
	return g_new0 (GCupsPPD, 1);
}

static void
ppd_free (GCupsPPD *ppd)
{
	g_free (ppd->filename);
	g_free (ppd->vendor);
	g_free (ppd->model);
	g_free (ppd->nickname);
	g_free (ppd->driver);
	g_free (ppd);
}
static void
ppd_list_free (GSList *list)
{
	g_slist_foreach (list, (GFunc)ppd_free, NULL);
	g_slist_free (list);
}

static int
str_case_compare (char const *v1, char const *v2)
{
	char *a = g_utf8_casefold (v1, -1);
	char *b = g_utf8_casefold (v2, -1);
	int ret = g_utf8_collate (a, b);
	g_free (a);
	g_free (b);
	return ret;
}

static gboolean
str_case_equal (gconstpointer v1,
		gconstpointer v2)
{
	return str_case_compare (v1, v2) == 0;
}

static guint
str_case_hash (gconstpointer key)
{
	char *str;
	guint ret;

	str = g_ascii_strup (key, -1);
	ret = g_str_hash (str);
	g_free (str);

	return ret;
}

static GHashTable *vendor_to_alias_map = NULL;
static GHashTable *alias_to_vendor_map = NULL;

/**
 * Model names frequently include the vendor name which is already displayed
 * elsewhere, strip it out if possible.
 **/
static char *
remove_vendor (char const *vendor, char const *model)
{
	char const *found;

	found = strstr (model, vendor);
	if (found == NULL) {
		unsigned i = 0;
		char const **aliases = g_hash_table_lookup (vendor_to_alias_map, vendor);
		while (aliases != NULL && aliases[i] != NULL &&
		       NULL == (found = strstr (model, (vendor = aliases[i]))))
			i++;
	}

	if (found == model) { /* <vendor> <model> */
		found += strlen (vendor);
		while (*found && g_ascii_isspace (*found))
			found++;
		return *found ? g_strdup (found) : g_strdup (model);
	}
	if (found != NULL) {
		g_warning ("TODO <blah> vendor <blarg>, or even (vendor) or -vendor '%s' '%s'", model, vendor);
	}
	return NULL;
}

static void
add_vendor_aliases (char const *name, char const **aliases)
{
	unsigned i;
	for (i = 0 ; aliases[i] != NULL; i++)
		g_hash_table_insert (alias_to_vendor_map,
			(gpointer)aliases[i], (gpointer)name);
	g_hash_table_insert (vendor_to_alias_map, (gpointer)name, aliases);
}

/**
 * A steaming pile of heuristics based on the various flavours of ppd naming
 **/
static void
ppd_extract_model_and_driver (GCupsPPD *ppd)
{
	char *model, *driver;
	char *tmp, *without_vendor;
        char *language, *language_position;

	if (alias_to_vendor_map == NULL) {
		static char const *apple[] = { "APPLE", NULL };
		static char const *epson[] = { "EPSON", NULL };
		static char const *okidata[] = { "OKI DATA CORP", "OKI", "OKIDATA", NULL };
		static char const *minolta[] = { "MINOLTA-QMS", "MINOLTA QMS", "MINOLTA", NULL };
		static char const *lexmark[] = { "Lexmark-International", "Lexmark International", NULL };
		static char const *kyocera[] = { "Kyocera-Mita", "Kyocera Mita", NULL };
		static char const *hp[] = { "Hewlett-Packard", "Hewlett Packard", "hp", NULL };
		static char const *dymo[] = { "Dymo-CoStar", "DYMO", NULL };
		static char const *canon[] = { "Canon Inc. (Kosugi Offic", "CANON", NULL };
		static char const *generic[] = { "Raw Queue", "Postscript", NULL };

		alias_to_vendor_map = g_hash_table_new (str_case_hash, str_case_equal);
		vendor_to_alias_map = g_hash_table_new (str_case_hash, str_case_equal);
		add_vendor_aliases ("Apple",	apple);
		add_vendor_aliases ("Epson",	epson);
		add_vendor_aliases ("Okidata",	okidata);
		add_vendor_aliases ("Minolta",	minolta);
		add_vendor_aliases ("Lexmark",	lexmark);
		add_vendor_aliases ("Kyocera",	kyocera);
		add_vendor_aliases ("HP",	hp);
		add_vendor_aliases ("Dymo",	dymo);
		add_vendor_aliases ("Canon",	canon);
		add_vendor_aliases (_("Generic"), generic);
	}

	/* clean up the vendor */
	tmp = g_hash_table_lookup (alias_to_vendor_map, ppd->vendor);
	if (tmp != NULL) {
		g_free (ppd->vendor);
		ppd->vendor = g_strdup (tmp);
	}

	without_vendor = remove_vendor (ppd->vendor, ppd->nickname);
	if (NULL == without_vendor)
		without_vendor = (char *)ppd->nickname;

	g_assert (without_vendor != NULL);

	/* gimp-print ijs models (suse, debian)
	 * 	<model> Foomatic/gimp-print-ijs */
	if (NULL != (tmp = strstr (without_vendor, " Foomatic/gimp-print-ijs"))) {
		model = g_strndup (without_vendor, tmp - without_vendor);
		driver = g_strdup (_("High Quality Image (GIMP-Print Inkjet)"));
	}

	/* gimp-print and ijs models (suse, debian)
	 * 	<model> Foomatic/gimp-print */
	else if (NULL != (tmp = strstr (without_vendor, " Foomatic/gimp-print"))) {
		model = g_strndup (without_vendor, tmp - without_vendor);
		driver = g_strdup (_("High Quality Image (GIMP-Print)"));
	}

        /* Stock foomatic entries (suse, debian)
	 * 	<model> Foomatic/<driver> */
	else if (NULL != (tmp = strstr (without_vendor, " Foomatic/"))) {
		model = g_strndup (without_vendor, tmp - without_vendor);
		driver = g_strdup (tmp + 10);

	/* Cups builtin
	 * 	<MODEL_IN_CAPS> CUPS v<m>.<n> */
	} else if (NULL != (tmp = strstr (without_vendor, " CUPS v"))) {
		model = g_strndup (without_vendor, tmp - without_vendor);
		driver = g_strdup (_("Standard (CUPS)"));

	/* gimp-print builtin
	 * 	<MODEL_IN_CAPS> - CUPS+Gimp-Print vm.n.o'
         * The filename may have a subdirectory--treat the subdir as a language, and set
         * the driver string accordingly. Use the regular driver string for the "C" language. */
	} else if (NULL != (tmp = strstr (without_vendor, " - CUPS+Gimp-Print v"))) {
		model = g_strndup (without_vendor, tmp - without_vendor);
                if((NULL != (language_position = strstr (ppd->filename, "/"))) &&
                   strncmp ("C/", ppd->filename, 2) &&
                   strncmp ("stp/", ppd->filename, 4)) {
                    language = g_strndup (ppd->filename, language_position - ppd->filename);
                    driver = g_strdup_printf (_("High Quality Image (GIMP-Print) (%s)"), language);
                    g_free(language);
                } else {
                    driver = g_strdup (_("High Quality Image (GIMP-Print)"));
                }

	/* RH style entries
	 * 	<model>, <driver> */
	} else if (NULL != (tmp = strstr (without_vendor, ", "))) {
		model = g_strndup (without_vendor, tmp - without_vendor);
		driver = g_strdup (tmp + 2);
        /* hmm we can not parse it in a known structure but at least we have an idea of the vendor */
	} else if (without_vendor != ppd->nickname) {
		model = g_strdup (without_vendor);
		driver = g_strdup (_("Standard"));
	} else {
		g_warning ("model named '%s' doesn't have a recognized structure", ppd->nickname);
		model = g_strdup (without_vendor);
		driver = g_strdup (_("Standard (CUPS)"));
	}

	/* Lets see if this is a foomatic 'recommended' driver */
	if (NULL != (tmp = strstr (without_vendor, " (recommended)"))) {
		strcpy (tmp, tmp+14);
		ppd->is_recommended = TRUE;
	} else
		ppd->is_recommended = FALSE;

	ppd->model = g_strstrip (model);
	ppd->driver = g_strstrip (driver);

	if (without_vendor != ppd->nickname)
		g_free (without_vendor);
}

static int
compare_models_by_driver (gconstpointer a, gconstpointer b)
{
	GCupsPPD const *a1 = a;
	GCupsPPD const *b1 = b;
	if (a1->is_recommended ^ b1->is_recommended)
		return a1->is_recommended ? -1 : 1;
	return str_case_compare (a1->driver, b1->driver);
}

static void
setup_ppd (GCupsDriverSelector *ds, GCupsPPD *ppd, gboolean select)
{
	GSList *drivers = NULL, *tmp;
	GHashTable *models;

	ppd_extract_model_and_driver (ppd);

	models = g_hash_table_lookup (ds->vendors, ppd->vendor);
	if (models == NULL) {
		models = g_hash_table_new_full (
			str_case_hash, str_case_equal, NULL,
			(GDestroyNotify)ppd_list_free);
		g_hash_table_insert (ds->vendors, ppd->vendor, models);
		if (select) {
			GtkTreeIter   iter;
			GtkTreeModel *store = gtk_combo_box_get_model (
				GTK_COMBO_BOX (glade_xml_get_widget (ds->xml, "vendors")));
			gtk_list_store_append (GTK_LIST_STORE (store), &iter);
			gtk_list_store_set (GTK_LIST_STORE (store), &iter,
				0, ppd->vendor,
				-1);
		}
	} else
		drivers = g_hash_table_lookup (models, ppd->model);

	if (select && drivers == NULL) {
		GtkTreeIter   iter;
		GtkTreeModel *store = gtk_tree_view_get_model (
			GTK_TREE_VIEW (glade_xml_get_widget (ds->xml, "model_treeview")));
		gtk_list_store_append (GTK_LIST_STORE (store), &iter);
		gtk_list_store_set (GTK_LIST_STORE (store), &iter,
			0, ppd->model,
			-1);
	}

	if (NULL != (tmp = g_slist_find_custom (drivers, ppd, compare_models_by_driver))
	    && tmp->data != ppd) {
		GCupsPPD *existing = tmp->data;

		/* SuSE likes to tweak some of the ppds and install both copies.
		 * Give the tweaked versions perference.  Drop stp variants in
		 * preference to others */
		if (NULL != strstr (ppd->filename, "suse.ppd") ||
		    NULL != strstr (existing->filename, "stp/")) {
			char *tmp = existing->filename;
			existing->filename = ppd->filename;
			ppd->filename = tmp;
			ppd_free (ppd); /* prefer existing, drop the new one */
			return;
		}
		if (NULL != strstr (existing->filename, "suse.ppd") ||
		    NULL != strstr (ppd->filename, "stp/")) {
			ppd_free (ppd); /* prefer existing, drop the new one */
			return;
		}
		g_warning ("Two ppds have driver == '%s'\n\t->%s (%s[%d]) and\n\t->%s (%s)[%d]",
			   ppd->driver,
			   ppd->filename,	ppd->nickname, ppd->is_recommended,
			   existing->filename,	existing->nickname, existing->is_recommended);
	}

	drivers = g_slist_insert_sorted (drivers, ppd, compare_models_by_driver);
	g_hash_table_steal (models, ppd->model);
	g_hash_table_insert (models, ppd->model, drivers);

	if (select) {
		GdkPixbuf    *pixbuf;
		GtkTreeIter   iter;
		GtkComboBox  *combo = GTK_COMBO_BOX (glade_xml_get_widget (ds->xml, "drivers"));
		GtkTreeModel *store = gtk_combo_box_get_model (combo);

		if (NULL == store) {
			store = (GtkTreeModel *)gtk_list_store_new (4,
				G_TYPE_STRING, G_TYPE_POINTER, G_TYPE_STRING, GDK_TYPE_PIXBUF);
			gtk_combo_box_set_model (combo, store);
		}

		gtk_list_store_append (GTK_LIST_STORE (store), &iter);
		if (ppd->is_recommended)
			pixbuf = gtk_widget_render_icon (GTK_WIDGET (combo),
				GTK_STOCK_YES, GTK_ICON_SIZE_MENU, NULL);
		else
			pixbuf = NULL;
		gtk_list_store_set (GTK_LIST_STORE (store), &iter,
			0, ppd->driver,
			1, ppd,
			2, ppd->is_recommended ? _("(Suggested)") : "",
			3, pixbuf,
			-1);
	}
	g_hash_table_insert (ds->ppds, ppd->nickname, ppd);
}

static GCupsPPD *
add_ppd (GCupsDriverSelector *ds, char *filename, char *vendor, char *nickname)
{
	GCupsPPD *ppd = ppd_new ();
	ppd->filename = filename;
	ppd->vendor = vendor;
	ppd->nickname = nickname;
	setup_ppd (ds, ppd, TRUE);
	return ppd;
}

static GSList *
vendor_list (GHashTable *vendors)
{
	return g_slist_sort (gnome_cups_hash_table_keys (vendors),
		(GCompareFunc)str_case_compare);
}
static GSList *
model_list_for_vendor (GHashTable *vendors, char const *vendor)
{
	GHashTable *models = g_hash_table_lookup (vendors, vendor);
	return g_slist_sort (gnome_cups_hash_table_keys (models),
		(GCompareFunc)str_case_compare);
}

/* Return number of characters that match, or -1 if they are identical */
static int
num_match (char const *a, char const *b)
{
	char *a1 = g_utf8_casefold (a, -1);
	char *b1 = g_utf8_casefold (b, -1);
	int i;

	for (i = 0; a1[i] != '\0' && b1[i] != '\0'; i++)
		if (a1[i] != b1[i])
			break;
	if (a1[i] == '\0' && b1[i] == '\0')
		i = -1;

	g_free (a1);
	g_free (b1);

	return i;
}

static GCupsPPD *
get_detected_ppd (GCupsDriverSelector *ds, char const *nickname)
{
	GCupsPPD *res;
	GHashTable *model_hash;
	GSList *vendors;
	GSList *models;
	GSList *drivers;
	GSList *ptr;
	char *vendor;
	char *model;
	char *ppd_model;
	int highest_match;

	if (nickname == NULL)
		return NULL;

	/* check for an exact match */
	res = g_hash_table_lookup (ds->ppds, nickname);
	if (res != NULL)
		return res;

	/* Split the vendor and the model out of the nickname string */
	vendor = NULL;
	model = NULL;
	vendors = vendor_list (ds->vendors);
	for (ptr = vendors; ptr != NULL ; ptr = ptr->next)
		if (NULL != (model = remove_vendor (ptr->data, nickname))) {
			vendor = g_strdup (ptr->data);
			break;
		}
	g_slist_free (vendors);
	if (NULL == ptr)
		return NULL;

	/* Now we have a valid make and model.  Find the closest ppd */
	ppd_model = NULL;
	highest_match = 0;
	models = model_list_for_vendor (ds->vendors, vendor);
	for (ptr = models; ptr != NULL; ptr = ptr->next) {
		int match = num_match (model, ptr->data);

		/* exact match */
		if (match == -1) {
			ppd_model = ptr->data;
			break;
		}

		if (match > highest_match) {
			ppd_model = ptr->data;
			highest_match = match;
		}
	}

	if (!ppd_model)
		return NULL;
	g_slist_free (models);

	model_hash = g_hash_table_lookup (ds->vendors, vendor);
	g_assert (model_hash != NULL);
	drivers = g_hash_table_lookup (model_hash, ppd_model);

	g_free (vendor);
	g_free (model);

	if (!drivers)
		return NULL;
	for (ptr = drivers; ptr != NULL ; ptr = ptr->next)
		if (((GCupsPPD *)(ptr->data))->is_recommended)
			return ptr-> data;
	return drivers->data;
}

static void
select_model (GladeXML *xml, char const *model_name)
{
	GtkTreeIter	  iter;
	GtkTreeView	 *view = GTK_TREE_VIEW (glade_xml_get_widget (xml, "model_treeview"));
	GtkTreeModel	 *model = gtk_tree_view_get_model (view);
	GtkTreeSelection *selection = gtk_tree_view_get_selection (view);
	char *item_name;

	if (!gtk_tree_model_get_iter_first (model, &iter))
		return;

	gtk_tree_selection_select_iter (selection, &iter);
	scroll_to_iter (view, model, &iter);
	do {
		gtk_tree_model_get (model, &iter, 0, &item_name, -1);
		if (str_case_compare (item_name, model_name) >= 0) {
			gtk_tree_selection_select_iter (selection, &iter);
			scroll_to_iter (view, model, &iter);
			break;
		}
	} while (gtk_tree_model_iter_next (model, &iter));
}

static void
set_make_and_model (GladeXML *xml, const char *make_and_model)
{
	GtkWidget *label;
	
	label = glade_xml_get_widget (xml, "make_and_model_label");
	gtk_label_set_text (GTK_LABEL (label), make_and_model);
}

static char *
get_selected_vendor (GladeXML *xml)
{
	char *res = NULL;
	if (combo_selected_get (xml, "vendors", 0, &res, -1))
		return res;
	return NULL;
}

static char *
get_selected_model (GladeXML *xml)
{
	GtkWidget *tree_view  = glade_xml_get_widget (xml, "model_treeview");
	GtkTreeSelection *sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (tree_view));
	GtkTreeIter iter;
	GtkTreeModel *model;

	if (gtk_tree_selection_get_selected (sel, &model, &iter)) {
		char *ret;
		gtk_tree_model_get (model, &iter, 0, &ret, -1);
		return ret;
	}
	return NULL;
}

static void
populate_driver_combo (GladeXML *xml)
{
	char	   *vendor  = get_selected_vendor (xml);
	char	   *model   = get_selected_model (xml);
	GtkComboBox  *combo = GTK_COMBO_BOX (glade_xml_get_widget (xml, "drivers"));
	GHashTable *models  = NULL;
	GdkPixbuf  *pixbuf;
	GtkListStore	*store;
	GtkTreeIter	 iter;
	int select = -1, i;

	if (vendor && model) {
		GHashTable *vendors = g_object_get_data (
			G_OBJECT (xml), "vendors");
		models = g_hash_table_lookup (vendors, vendor);
	}

	store = gtk_list_store_new (4,
		G_TYPE_STRING, G_TYPE_POINTER, G_TYPE_STRING, GDK_TYPE_PIXBUF);
	if (models != NULL) {
		GSList *drivers = g_hash_table_lookup (models, model);
		gboolean only_choice = drivers != NULL && drivers->next == NULL;

		for (i = 0; drivers != NULL; drivers = drivers->next, i++) {
			GCupsPPD *ppd = drivers->data;
			gtk_list_store_append (store, &iter);
			if (select < 0 && (ppd->is_recommended || only_choice)) {
				select = i;
				pixbuf = gtk_widget_render_icon (GTK_WIDGET (combo),
					GTK_STOCK_YES, GTK_ICON_SIZE_MENU, NULL);
			} else
				pixbuf = NULL;
			gtk_list_store_set (store, &iter,
				0, ppd->driver,
				1, ppd,
				2, (select == i) ? _("(Suggested)") : "",
				3, pixbuf,
				-1);
		}
	}
	gtk_widget_set_sensitive (GTK_WIDGET (combo), models != NULL);
	gtk_combo_box_set_model (combo, GTK_TREE_MODEL (store));
	gtk_combo_box_set_active (combo, -1);
	if (select >= 0)
		gtk_combo_box_set_active (combo, select);

	g_free (vendor);
	g_free (model);
}

static void
populate_model_list (GladeXML *xml)
{
	char *vendor;
	GtkTreeIter	 iter;
	GHashTable	*vendors;
	GtkWidget	*tree_view;
	GtkListStore	*store;
	GSList *models = NULL;
	GSList *l;

	tree_view = glade_xml_get_widget (xml, "model_treeview");
	store = GTK_LIST_STORE (gtk_tree_view_get_model (GTK_TREE_VIEW (tree_view)));
	gtk_list_store_clear (store);

	vendor = get_selected_vendor (xml);
	if (NULL == vendor)
		return;

	vendors = g_object_get_data (G_OBJECT (xml), "vendors");
	models = model_list_for_vendor (vendors, vendor);
	for (l = models; l != NULL; l = l->next) {
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter, 0, l->data, -1);
	}

	g_slist_free (models);
	g_free (vendor);
}

static void
populate_vendor_combo (GladeXML *xml)
{
	GHashTable *ppds = g_object_get_data (G_OBJECT (xml), "vendors");
	GtkComboBox *combo = GTK_COMBO_BOX (glade_xml_get_widget (xml, "vendors"));
	GtkListStore	*store;
	GtkTreeIter	 iter;
	GSList *vendors = vendor_list (ppds);
	GSList *l;

	store = gtk_list_store_new (1, G_TYPE_STRING);
	for (l = vendors; l != NULL; l = l->next) {
		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter, 0, l->data, -1);
	}
	gtk_combo_box_set_model (combo, GTK_TREE_MODEL (store));
	gtk_combo_box_set_active (combo, -1);
	if (vendors != NULL)
		gtk_combo_box_set_active (combo, 0);
	g_slist_free (vendors);
}

static void
gcups_driver_selector_class_init (GCupsDriverSelectorClass *obj_klass)
{
	signals[CHANGED] = g_signal_new ("changed",
		G_OBJECT_CLASS_TYPE (obj_klass), G_SIGNAL_RUN_LAST,
		G_STRUCT_OFFSET (GCupsDriverSelectorClass, changed),
		NULL, NULL, g_cclosure_marshal_VOID__VOID,
		G_TYPE_NONE, 0, G_TYPE_NONE);
}

static void
cb_driver_changed (GCupsDriverSelector *ds)
{
	/* some debug spew */
#if 1
	GCupsPPD const *ppd = gcups_driver_selector_get (ds);
	if (ppd != NULL)
		g_print ("Selected ppd file = %s\n", ppd->filename);
#endif

	g_signal_emit (G_OBJECT (ds), signals [CHANGED], 0);
}

#define CUPS_DIR  "/usr/share/cups/model"

static gboolean
cb_find_ppd_by_filename (gpointer key, gpointer value, gpointer filename)
{
	return (0 == strcmp (filename, ((GCupsPPD const *)value)->filename));
}

static void
cb_install_driver (GCupsDriverSelector *ds)
{
	GCupsPPD   *gppd = NULL;
	ppd_attr_t *manufacturer, *nickname;
	ppd_file_t *ppd = NULL;
	FILE *stdio = NULL;
	char *base, *ppd_file, *target_path = NULL, *content = NULL;
	GError *err = NULL;
	gsize length;
	GtkFileFilter  *filter;
	GtkFileChooser *fsel = g_object_new (GTK_TYPE_FILE_CHOOSER_DIALOG,
		"action",	GTK_FILE_CHOOSER_ACTION_OPEN,
		"title",	_("Select a PPD File"),
		"local-only",	TRUE,
		"use-preview-label", TRUE,
		NULL);
	gtk_dialog_add_buttons (GTK_DIALOG (fsel),
		GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
		GTK_STOCK_OPEN,   GTK_RESPONSE_OK,
		NULL);
	gtk_dialog_set_default_response (GTK_DIALOG (fsel), GTK_RESPONSE_OK);

	filter = gtk_file_filter_new ();
	gtk_file_filter_set_name (filter, _("All Files"));
	gtk_file_filter_add_pattern (filter, "*");
	gtk_file_chooser_add_filter (fsel, filter);

	filter = gtk_file_filter_new ();
	gtk_file_filter_set_name (filter, _("PPD Files"));
	gtk_file_filter_add_pattern (filter, "*.ppd");
	gtk_file_filter_add_pattern (filter, "*.PPD");
	gtk_file_filter_add_pattern (filter, "*.ppd.gz");
	gtk_file_filter_add_pattern (filter, "*.PPD.GZ");
	gtk_file_chooser_add_filter (fsel, filter);
	gtk_file_chooser_set_filter (fsel, filter);

	gtk_window_set_modal (GTK_WINDOW (fsel), TRUE);
	gtk_window_set_transient_for (GTK_WINDOW (fsel), 
		(GtkWindow *)gtk_widget_get_toplevel (GTK_WIDGET (ds)));
	gtk_widget_show_all (GTK_WIDGET (fsel));

	if (GTK_RESPONSE_OK != gtk_dialog_run (GTK_DIALOG (fsel)))
		goto done;

	ppd_file = gtk_file_chooser_get_filename (fsel);
	if (ppd_file == NULL)
		goto done;

	/* Check suffix == .ppd or .ppd.gz */
	length = strlen (ppd_file);
	if (length < 4 ||
	    (0 != g_ascii_strcasecmp (ppd_file+length-4, ".ppd") &&
	     (length < 7 || 0 != g_ascii_strcasecmp (ppd_file+length-7, ".ppd.gz")))) {
		err = g_error_new (1, 1, _("Only files ending with .ppd or .ppd.gz will be installed"));
		goto done;
	}
	
	if (!g_file_test (CUPS_DIR, G_FILE_TEST_IS_DIR)) {
		err = g_error_new (1, 1,
			_("CUPS is installed differently than expected.  There is no directory '%s'"), CUPS_DIR);
		goto done;
	}
	
	if (length > 7 && !g_ascii_strcasecmp (ppd_file+length-7, ".ppd.gz")) {
		char *command, *name;
		const char *gzip;
		
		if (!(gzip = g_find_program_in_path ("gzip")))
			goto done;
		
		name = g_path_get_basename (ppd_file);
		name[strlen (name) - 3] = '\0';
		
		command = g_strdup_printf ("%s -cd %s > %s/%s", gzip, ppd_file, g_get_tmp_dir (), name);
		system (command);
		g_free (command);
		
		ppd_file = g_strdup_printf ("%s/%s", g_get_tmp_dir (), name);
		g_free (name);
		
		if (!g_file_test (ppd_file, G_FILE_TEST_EXISTS))
			goto done;
	}
	
	base = g_path_get_basename  (ppd_file);
	target_path = g_build_filename (CUPS_DIR, base, NULL);

	if (g_file_test (target_path, G_FILE_TEST_EXISTS)) {
		err = g_error_new (1, 1,
			_("The PPD\n\t<b>%s</b>\nis already installed"), target_path);
		gppd = g_hash_table_find (ds->ppds, &cb_find_ppd_by_filename, base);
		goto done;
	}

	ppd = ppdOpenFile (ppd_file);
	if (ppd == NULL) {
		int line = 1;
		ppd_status_t status = ppdLastError(&line);
		char *msg = g_strdup_printf ("%s at %d:'%s'",
			ppdErrorString (status), line, ppd_file);
		err = g_error_new (1, 1, "%s", msg);
		g_free (msg);
		goto done;
	}

	manufacturer = ppdFindAttr (ppd, "Manufacturer", NULL);
	nickname = ppdFindAttr (ppd, "NickName", NULL);

	stdio = fopen (target_path, "w");
	if (stdio == NULL) {
		err = g_error_new (1, 1,
			_("Unable to write to\n\t<b>%s</b>\nbecause %s"), target_path, strerror (errno));
		goto done;
	}

	if (!g_file_get_contents (ppd_file, &content, &length,  &err))
		goto done;

	fwrite (content, 1, length, stdio);
	gppd = add_ppd (ds, base,
		 g_strdup (manufacturer ? manufacturer->value : "Raw Queue"),
		 g_strdup (nickname ? nickname->value : "User supplied"));
done :
	if (gppd != NULL) {
		combo_select (ds->xml, "vendors", 0, gppd->vendor);
		select_model (ds->xml, gppd->model);
		combo_select (ds->xml, "drivers", 0, gppd->driver);
	}

	if (ppd != NULL)
		ppdClose (ppd);
	if (stdio != NULL)
		fclose (stdio);
	g_free (content);
	gtk_widget_destroy (GTK_WIDGET (fsel));

	if (err != NULL) {
		GtkWidget *dialog = gtk_message_dialog_new_with_markup (
			(GtkWindow *)gtk_widget_get_toplevel (GTK_WIDGET (ds)),
			GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_ERROR,
			GTK_BUTTONS_OK,
			"%s",
			err->message);
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (GTK_WIDGET (dialog));
		g_error_free (err);
	}
}

static void
init_ppds (GCupsDriverSelector *ds)
{
	ipp_attribute_t *attr;
	ipp_t		*response = ppd_request ();
	GCupsPPD   *ppd;

	ds->ppds = g_hash_table_new (g_str_hash, g_str_equal);
	ds->vendors = g_hash_table_new_full (
		str_case_hash, str_case_equal, NULL,
		(GDestroyNotify)g_hash_table_destroy);

	g_object_set_data_full (G_OBJECT (ds->xml), "ppds", ds->ppds,
		(GDestroyNotify)g_hash_table_destroy);
	g_object_set_data_full (G_OBJECT (ds->xml), "vendors", ds->vendors,
		(GDestroyNotify)g_hash_table_destroy);

	if (NULL == response) {
		g_warning ("Unable to load the set of known printers.  Please check your installation of cups");
		return;
	}

	ppd = ppd_new ();
	for (attr = response->attrs; attr != NULL; attr = attr->next) {
		if (NULL == attr->name) {
			if (ppd->filename && ppd->vendor && ppd->nickname)
				setup_ppd (ds, ppd, FALSE);
			else
				ppd_free (ppd);
			ppd = ppd_new ();
		} else if (!strcmp (attr->name, "ppd-name")) {
			g_free (ppd->filename);
			ppd->filename = g_strdup (attr->values[0].string.text);
		} else if (!strcmp (attr->name, "ppd-make")) {
			g_free (ppd->vendor);
			ppd->vendor = g_strdup (attr->values[0].string.text);
		} else if (!strcmp (attr->name, "ppd-make-and-model")) {
			g_free (ppd->nickname);
			ppd->nickname = g_strdup (attr->values[0].string.text);
		}
	}
	if (ppd->filename && ppd->vendor && ppd->nickname)
		setup_ppd (ds, ppd, FALSE);
	else
		ppd_free (ppd);
	ippDelete (response);
}

static void
gcups_driver_selector_init (GCupsDriverSelector *ds)
{
	GtkWidget *combo, *view, *w;
	GtkTreeViewColumn *column;
	GtkCellRenderer *renderer;
	GtkListStore *store;
	GtkTreeSelection *selection;

	ds->xml = glade_xml_new (GNOME_CUPS_MANAGER_DATADIR "/gnome-cups-ui-driver.glade",
		"gcups_driver_selector", GETTEXT_PACKAGE);
	init_ppds (ds);

	combo = glade_xml_get_widget (ds->xml, "vendors");
        renderer = gtk_cell_renderer_text_new ();
        gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, TRUE);
        gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo), renderer,
		"text", 0, NULL);
        gtk_combo_box_set_wrap_width (GTK_COMBO_BOX (combo), 3);
	g_signal_connect_swapped (combo,
		"changed",
		G_CALLBACK (populate_model_list), ds->xml);

	view = glade_xml_get_widget (ds->xml, "model_treeview");
	store = gtk_list_store_new (1, G_TYPE_STRING);
	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (
		_("Model"), renderer, "text", 0, NULL);
	gtk_tree_view_append_column (GTK_TREE_VIEW (view), column);
	gtk_tree_view_set_model (GTK_TREE_VIEW (view),
				 GTK_TREE_MODEL (store));

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (view));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);
	g_signal_connect_swapped (selection,
		"changed",
		G_CALLBACK (populate_driver_combo), ds->xml);

	combo = glade_xml_get_widget (ds->xml, "drivers");
        renderer = gtk_cell_renderer_text_new ();
        gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, TRUE);
        gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo), renderer,
		"text", 0, NULL);
        renderer = gtk_cell_renderer_text_new ();
        gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, FALSE);
        gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo), renderer,
		"text", 2, NULL);
        renderer = gtk_cell_renderer_pixbuf_new ();
        gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, FALSE);
        gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (combo), renderer,
		"pixbuf", 3, NULL);

	g_signal_connect_swapped (combo,
		"changed",
		G_CALLBACK (cb_driver_changed), ds);
	gtk_widget_set_sensitive (GTK_WIDGET (combo), FALSE);

	g_signal_connect_swapped (glade_xml_get_widget (ds->xml, "install_driver"),
		"clicked",
		G_CALLBACK (cb_install_driver), ds);
	gtk_widget_set_sensitive (GTK_WIDGET (combo), FALSE);

	populate_vendor_combo (ds->xml);
	w = glade_xml_get_widget (ds->xml, "gcups_driver_selector");
	gtk_container_add (GTK_CONTAINER (ds), w);
	gtk_widget_show_all (GTK_WIDGET (ds));
}

GCupsPPD const *
gcups_driver_selector_get (GCupsDriverSelector *ds)
{
	gpointer res = NULL;
	if (combo_selected_get (ds->xml, "drivers", 1, &res, -1))
		return res;
	return NULL;
}

void
gcups_driver_selector_set_nickname (GCupsDriverSelector *ds, const char *nickname)
{
	GCupsPPD *ppd;

	g_return_if_fail (ds != NULL);
	
	set_make_and_model (ds->xml, nickname);
	
	ppd = get_detected_ppd (ds, nickname);
	if (ppd != NULL) {
		combo_select (ds->xml, "vendors", 0, ppd->vendor);
		select_model (ds->xml, ppd->model);
		combo_select (ds->xml, "drivers", 0, ppd->driver);
	}
	g_object_set_data (G_OBJECT (ds->xml), "detected_ppd", ppd);
}

char *
gcups_driver_selector_get_vendor (GCupsDriverSelector *ds)
{
	return get_selected_vendor (ds->xml);
}

char *
gcups_driver_selector_get_model	(GCupsDriverSelector *ds)
{
	return get_selected_model (ds->xml);
}

GtkWidget *
gcups_driver_selector_new ()
{
	return g_object_new (gcups_driver_selector_get_type (), NULL);
}
