#include <config.h>

#include "gnome-cups-ui-util.h"

#include <libgnomecups/gnome-cups-util.h>
#include <libgnomecups/gnome-cups-ui-init.h>
#include <libgnomecups/gnome-cups-printer.h>
#include <libgnomecups/gnome-cups-request.h>
#include <libgnomecups/gnome-cups-ui-print.h>

#include <glade/glade.h>
#include <gtk/gtk.h>

#include <glib/gi18n.h>
#include <cups/cups.h>
#include <stdlib.h>

void
gnome_cups_error_dialog (GtkWindow *window, 
			 const char *prefix,
			 GError *error)
{
	char *msg = error ? error->message : _("Unknown Error");
	GtkWidget *dialog = gtk_message_dialog_new (window, 
		GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL, 
		GTK_MESSAGE_ERROR,
		GTK_BUTTONS_OK,
		"%s: %s", prefix, msg);
	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog); 
}

static void
get_test_page (GnomeCupsPrinter *printer,
	       char **filename,
	       char **paper_name)
{
	char *paper_size;
	
	paper_size = gnome_cups_printer_get_option_value (printer, "PageSize");
	
	if (paper_size && !g_ascii_strcasecmp (paper_size, "a4")) {
		if (paper_name) {
			*paper_name = g_strdup_printf ("A4");
		}
		
		if (filename) {
			*filename = g_strdup (DATADIR "/gnome-cups-manager/xd2-testpage-a4.eps");
		}
	} else {
		if (paper_name) {
			*paper_name = g_strdup_printf ("Letter");
		}
		
		if (filename) {
			*filename = g_strdup (DATADIR "/gnome-cups-manager/xd2-testpage-letter.eps");
		}
	}
	
	g_free (paper_size);
}

static void
print_dialog_response_cb (GtkWidget *dialog, 
			  int response_code, 
			  gpointer user_data)
{
	gtk_widget_destroy (dialog);
}

void
gnome_cups_print_test_page (GnomeCupsPrinter *printer,
			    GtkWidget *parent_window)
{
	gboolean result;
	char *filename = NULL;
	char *paper_size = NULL;

	get_test_page (printer, &filename, &paper_size);

	result = gnome_cups_ui_print_file (printer, 
					   GTK_WINDOW (parent_window),
					   filename,
					   _("Test Page"), NULL);
	
	if (result) {
                GtkWidget *dialog;
 
                dialog = gtk_message_dialog_new
                        (GTK_WINDOW (parent_window),
                         GTK_DIALOG_DESTROY_WITH_PARENT,
                         GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
                         _("%s test page has been sent to %s."),
			 paper_size,
                         gnome_cups_printer_get_name (printer));
		gtk_widget_show (dialog);
		g_signal_connect (dialog, "response",
				  G_CALLBACK (print_dialog_response_cb),
				  NULL);
	}

	g_free (paper_size);
	g_free (filename);
}

static int active_windows = 0;

static void
window_weak_notify (gpointer user_data, GObject *old_object)
{
	active_windows--;

	if (active_windows == 0) {
		gtk_main_quit ();
	}
}

void
watch_window (GtkWidget *widget)
{
	active_windows++;
	g_object_weak_ref (G_OBJECT (widget), 
			   (GWeakNotify)window_weak_notify,
			    NULL);
}

void
set_window_icon (GtkWidget *window, const char *icon_name)
{
	GdkPixbuf *pixbuf = gtk_icon_theme_load_icon (gtk_icon_theme_get_default (),
		icon_name, 32, GTK_ICON_LOOKUP_USE_BUILTIN, NULL);
	if (pixbuf != NULL) {
		gtk_window_set_icon (GTK_WINDOW (window), pixbuf);
		g_object_unref (pixbuf);
	} else
		g_warning ("unable to load icon '%s'", icon_name);
}

/**
 * combo_selected_get :
 * Extracts requested values from the selected element of the named combo's
 * model.
 **/
gboolean
combo_selected_get (GladeXML *xml, char const *name, ...)
{
	va_list		 var_args;
	GtkTreeIter	 iter;
	GtkComboBox	*combo = (GtkComboBox *)glade_xml_get_widget (xml, name);

	g_return_val_if_fail (combo != NULL, FALSE);

	if (!gtk_combo_box_get_active_iter (combo, &iter))
		return FALSE;

	va_start (var_args, name);
	gtk_tree_model_get_valist (gtk_combo_box_get_model (combo), &iter, var_args);
	va_end (var_args);
	return TRUE;
}

