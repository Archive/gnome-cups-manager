#ifndef GNOME_CUPS_UI_PRINT
#define GNOME_CUPS_UI_PRINT

#include <glib.h>
#include <gtk/gtk.h>
#include <libgnomecups/gnome-cups-printer.h>

G_BEGIN_DECLS

gboolean gnome_cups_ui_print_file (GnomeCupsPrinter        *printer,
				   GtkWindow               *opt_parent,
				   const char              *filename,
				   const char              *job_name,
				   GList                   *options);

G_END_DECLS

#endif
