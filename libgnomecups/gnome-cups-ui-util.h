#ifndef GNOME_CUPS_UI_UTIL_H
#define GNOME_CUPS_UI_UTIL_H

#include <glib.h>
#include <glade/glade.h>

#include <gtk/gtk.h>
#include <libgnomecups/gnome-cups-printer.h>

void gnome_cups_error_dialog (GtkWindow *window, 
			      const char *prefix,
			      GError *error);

void gnome_cups_print_test_page (GnomeCupsPrinter *printer,
				 GtkWidget *parent_window);

void watch_window (GtkWidget *window);
void set_window_icon (GtkWidget *window, const char *icon_name);

gboolean combo_selected_get (GladeXML *xml, char const *name, ...);

#endif /* GNOME_CUPS_UI_UTIL_H */
