#ifndef GNOME_CUPS_UI_INIT
#define GNOME_CUPS_UI_INIT

#include <glib.h>

G_BEGIN_DECLS

void gnome_cups_ui_init (char const *argv_0);

G_END_DECLS

#endif
