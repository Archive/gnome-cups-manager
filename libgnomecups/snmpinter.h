/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef GNOME_CUPS_UI_SNMPINTER_H
#define GNOME_CUPS_UI_SNMPINTER_H

G_BEGIN_DECLS

char *get_snmp_printers(char *specstr, int *retval);

G_END_DECLS

#endif /* GNOME_CUPS_UI_SNMPINTER_H */
