/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef GNOME_CUPS_UI_DRIVER_H
#define GNOME_CUPS_UI_DRIVER_H

#include <glib.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GCUPS_DRIVER_SELECTOR_TYPE	(gcups_driver_selector_get_type ())
#define GCUPS_DRIVER_SELECTOR(o)	G_TYPE_CHECK_INSTANCE_CAST ((o), GCUPS_DRIVER_SELECTOR_TYPE, GCupsDriverSelector)
#define IS_GCUPS_DRIVER_SELECTOR(o)	G_TYPE_CHECK_INSTANCE_TYPE ((o), GCUPS_DRIVER_SELECTOR_TYPE)

typedef struct _GCupsDriverSelector	  GCupsDriverSelector;

/* Identifies a PPD retrieved from the cups server */
typedef struct {
	/* from cups */
	char *filename;	/* ppd-name : name of file in <prefix>/share/cups/model */
	char *vendor;	/* ppd-make : *Manufacturer in ppd */
	char *nickname;	/* ppd-make-and-model : *NickName in ppd */

	/* generated */
	char *model;
	char *driver;
	gboolean is_recommended;
} GCupsPPD;


GType		 gcups_driver_selector_get_type (void);
GtkWidget	*gcups_driver_selector_new	(void);
GCupsPPD const	*gcups_driver_selector_get	(GCupsDriverSelector *ds);
void	gcups_driver_selector_set_nickname	(GCupsDriverSelector *ds,
						 const char *nickname);
char	*gcups_driver_selector_get_vendor	(GCupsDriverSelector *ds);
char	*gcups_driver_selector_get_model	(GCupsDriverSelector *ds);

G_END_DECLS

#endif /* GNOME_CUPS_UI_DRIVER_H */
