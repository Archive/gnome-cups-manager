#include "config.h"

#include <glib.h>
#include <gtk/gtk.h>

#include <libgnomecups/gnome-cups-init.h>
#include <libgnomecups/gnome-cups-printer.h>

#include "gnome-cups-ui-print.h"
#include "gnome-cups-i18n.h"

gboolean
gnome_cups_ui_print_file (GnomeCupsPrinter        *printer,
			  GtkWindow               *opt_parent,
			  const char              *filename,
			  const char              *job_name,
			  GList                   *options)
{
	gboolean result;
	GError  *error = NULL;

	g_return_val_if_fail (GNOME_CUPS_IS_PRINTER (printer), FALSE);

	result = gnome_cups_printer_print_file
		(printer, filename, job_name, options, &error);

	if (!result) {
		GtkWidget *dialog;

		dialog = gtk_message_dialog_new
			(opt_parent,
			 GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
			 GTK_MESSAGE_ERROR, GTK_BUTTONS_OK,
			 _("Printing to '%s' failed with error code: %d\nis the printer paused ?"),
			 gnome_cups_printer_get_name (printer),
			 error ? error->code : -1);

		gtk_dialog_run (GTK_DIALOG (dialog));
		
		gtk_widget_destroy (dialog);
	}

	return result;
}
