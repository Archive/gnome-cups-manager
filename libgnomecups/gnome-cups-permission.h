/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef GNOME_CUPS_PERMISSION_H
#define GNOME_CUPS_PERMISSION_H

#include <glib.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

void	 gnome_cups_checkpath (char const *argv_0);
gboolean gnome_cups_can_admin (void);
gboolean gnome_cups_spawn     (char const *app, int argc, char const **argv,
			       gboolean force_admin, GtkWidget *toplevel);

G_END_DECLS

#endif /* GNOME_CUPS_PERMISSION_H */
