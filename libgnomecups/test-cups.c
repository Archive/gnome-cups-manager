#include <config.h>
#include <gtk/gtk.h>
#include "gnome-cups-printer.h"

static gboolean
timeout_fn (gpointer user_data)
{
	g_print ("here\n");
	gtk_main_quit ();
	return TRUE;
}

int
main (int argc, char *argv[])
{
	GnomeCupsPrinter *printer;
	
	gtk_init (&argc, &argv);
	printer = gnome_cups_printer_get ("Copyroom");
	
	g_print ("test me - wait for 20secs\n");

	g_timeout_add (20000, timeout_fn, NULL);

	gtk_main ();
	
	return 0;
}
