/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef GNOME_CUPS_UI_CONNECTION_H
#define GNOME_CUPS_UI_CONNECTION_H

#include <glib.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GCUPS_CONNECTION_SELECTOR_TYPE	(gcups_connection_selector_get_type ())
#define GCUPS_CONNECTION_SELECTOR(o)	G_TYPE_CHECK_INSTANCE_CAST ((o), GCUPS_CONNECTION_SELECTOR_TYPE, GCupsConnectionSelector)
#define IS_GCUPS_CONNECTION_SELECTOR(o)	G_TYPE_CHECK_INSTANCE_TYPE ((o), GCUPS_CONNECTION_SELECTOR_TYPE)

typedef struct _GCupsConnectionSelector	  GCupsConnectionSelector;

GType	   gcups_connection_selector_get_type	  (void);
GtkWidget *gcups_connection_selector_new	  (void);
void	   gcups_connection_selector_queue_guess  (GCupsConnectionSelector *cs);
char	  *gcups_connection_selector_get_uri	  (GCupsConnectionSelector *cs);
void	   gcups_connection_selector_set_uri	  (GCupsConnectionSelector *cs,
						   char const *uri);

G_END_DECLS

#endif /* GNOME_CUPS_UI_CONNECTION_H */
