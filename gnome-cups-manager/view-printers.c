/* gnome-printers-view
 * 
 *  Copyright (C) 2002 Ximian Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this library; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dave Camp <dave@ximian.com>
 *
 */

#include <config.h>

#include "gnome-printer-list.h"
#include "view-printers.h"
#include "view-queue.h"
#include "printer-properties.h"

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <libgnomecups/gnome-cups-util.h>
#include <libgnomecups/gnome-cups-ui-init.h>
#include <libgnomecups/gnome-cups-printer.h>
#include <libgnomecups/gnome-cups-ui-print.h>
#include <libgnomecups/gnome-cups-ui-util.h>
#include <libgnomecups/gnome-cups-permission.h>

#include <stdlib.h>
#include <cups/cups.h>

static void
printer_view_weak_notify (gpointer user_data, GObject *old_spot)
{
	GtkWidget **widget = user_data;
	
	*widget = NULL;
}

static void
close_cb (GtkWidget *item, 
	  gpointer user_data)
{
	GladeXML *xml;
	GtkWidget *widget;
	
	xml = GLADE_XML (user_data);

	widget = glade_xml_get_widget (xml, "printer_window");
	gtk_widget_destroy (widget);
}

static void
new_printer_cb (GtkWidget *item,
		gpointer user_data)
{
	GladeXML *xml;
	GnomePrinterList *printer_list;
	
	xml = GLADE_XML (user_data);
	
	printer_list = g_object_get_data (G_OBJECT (xml), "icon_list");

	gnome_printer_list_new_printer (printer_list);
}

static void
status_cb (GtkWidget *item,
	 gpointer user_data)
{
	GladeXML *xml;
	GnomePrinterList *printer_list;
	
	xml = GLADE_XML (user_data);
	
	printer_list = g_object_get_data (G_OBJECT (xml), "icon_list");

	gnome_printer_list_selection_activate (printer_list);
}

static void
properties_cb (GtkWidget *item,
	       gpointer user_data)
{
	GladeXML *xml;
	GnomePrinterList *printer_list;
	
	xml = GLADE_XML (user_data);
	
	printer_list = g_object_get_data (G_OBJECT (xml), "icon_list");

	gnome_printer_list_selection_properties (printer_list);
}

static void
pause_cb (GtkWidget *item,
	       gpointer user_data)
{
	GladeXML *xml;
	GnomePrinterList *printer_list;
	
	xml = GLADE_XML (user_data);
	
	printer_list = g_object_get_data (G_OBJECT (xml), "icon_list");

	gnome_printer_list_selection_pause (printer_list);
}

static void
resume_cb (GtkWidget *item,
	       gpointer user_data)
{
	GladeXML *xml;
	GnomePrinterList *printer_list;
	
	xml = GLADE_XML (user_data);
	
	printer_list = g_object_get_data (G_OBJECT (xml), "icon_list");

	gnome_printer_list_selection_resume (printer_list);
}

static void
become_admin_cb (GtkWidget *item, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);
	if (gnome_cups_spawn ("gnome-cups-manager", 0, NULL,
			      TRUE, glade_xml_get_widget (xml, "printer_window")))
		gtk_main_quit ();

}

static void
make_default_cb (GtkWidget *item,
	       gpointer user_data)
{
	GladeXML *xml;
	GnomePrinterList *printer_list;
	
	xml = GLADE_XML (user_data);
	
	printer_list = g_object_get_data (G_OBJECT (xml), "icon_list");

	gnome_printer_list_selection_make_default (printer_list);
}

static void
delete_cb (GtkWidget *item,
	       gpointer user_data)
{
	GladeXML *xml;
	GnomePrinterList *printer_list;
	
	xml = GLADE_XML (user_data);
	
	printer_list = g_object_get_data (G_OBJECT (xml), "icon_list");

	gnome_printer_list_selection_delete (printer_list);
}

static void
setup_menus (GladeXML *xml)
{
	GtkWidget *item;
	
	item = glade_xml_get_widget (xml, "close_item");
	g_signal_connect (item, "activate", G_CALLBACK (close_cb), xml);

	item = glade_xml_get_widget (xml, "new_printer_item");
	g_signal_connect (item, "activate", G_CALLBACK (new_printer_cb), xml);

	item = glade_xml_get_widget (xml, "status_item");
	g_signal_connect (item, "activate", G_CALLBACK (status_cb), xml);

	item = glade_xml_get_widget (xml, "properties_item");
	g_signal_connect (item, "activate", G_CALLBACK (properties_cb), xml);

	item = glade_xml_get_widget (xml, "pause_item");
	g_signal_connect (item, "activate", G_CALLBACK (pause_cb), xml);

	item = glade_xml_get_widget (xml, "resume_item");
	g_signal_connect (item, "activate", G_CALLBACK (resume_cb), xml);

	item = glade_xml_get_widget (xml, "become_admin_item");
	g_signal_connect (item, "activate", G_CALLBACK (become_admin_cb), xml);

	item = glade_xml_get_widget (xml, "make_default_item");
	g_signal_connect (item, "activate", G_CALLBACK (make_default_cb), xml);

	item = glade_xml_get_widget (xml, "delete_item");
	g_signal_connect (item, "activate", G_CALLBACK (delete_cb), xml);
}

static void
update_menus (GladeXML *xml)
{
	GtkWidget *item;
	GnomePrinterList *list;
	gboolean activate;
	gboolean pause;
	gboolean resume;
	gboolean make_default;
	gboolean delete;
	gboolean properties;
	gboolean status;

	list = GNOME_PRINTER_LIST (g_object_get_data (G_OBJECT (xml), "icon_list"));

	gnome_printer_list_get_command_sensitivities (list,
		&activate, &pause, &resume, &make_default,
		&delete, &properties, &status);

	item = glade_xml_get_widget (xml, "status_item");
	gtk_widget_set_sensitive (item, status);

	item = glade_xml_get_widget (xml, "pause_item");
	gtk_widget_set_sensitive (item, pause);

	item = glade_xml_get_widget (xml, "resume_item");
	gtk_widget_set_sensitive (item, resume);

	if (gnome_cups_can_admin ()) {
		item = glade_xml_get_widget (xml, "become_admin_item");
		gtk_widget_hide (item);
	}

	item = glade_xml_get_widget (xml, "make_default_item");
	gtk_widget_set_sensitive (item, make_default);

	item = glade_xml_get_widget (xml, "delete_item");
	gtk_widget_set_sensitive (item, delete);

	item = glade_xml_get_widget (xml, "properties_item");
	gtk_widget_set_sensitive (item, properties);
}

static void
selection_changed_cb (EggIconList *list,
		      gpointer user_data)
{
	update_menus (GLADE_XML (user_data));
}

void
gnome_cups_manager_view_printers (void) 
{
	GladeXML *xml;
	GtkWidget *scrolled;
	GtkWidget *printer_list;

	static GtkWidget *window = NULL;

	if (window) {
		gtk_window_present (GTK_WINDOW (window));
		return;
	}

	xml = glade_xml_new (GNOME_CUPS_MANAGER_DATADIR "/gnome-cups-manager.glade",
			     "printer_window",
			     GETTEXT_PACKAGE);

	printer_list = gnome_printer_list_new ();

	g_signal_connect (printer_list, "selection_changed",
			  G_CALLBACK (selection_changed_cb), xml);

	gtk_widget_show (printer_list);
	g_object_set_data (G_OBJECT (xml), "icon_list", printer_list);

	scrolled = glade_xml_get_widget (xml, "icon_list_scrolled");
	gtk_container_add (GTK_CONTAINER (scrolled), printer_list);

	window = glade_xml_get_widget (xml, "printer_window");
	g_object_weak_ref (G_OBJECT (window),
			   (GWeakNotify)printer_view_weak_notify, &window);

	set_window_icon (window, "gnome-dev-printer");

	setup_menus (xml);
	update_menus (xml);
	
	watch_window (window);
}
