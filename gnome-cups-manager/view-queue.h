#ifndef VIEW_QUEUE_H
#define VIEW_QUEUE_H

#include <libgnomecups/gnome-cups-printer.h>

void gnome_cups_manager_view_queue (GnomeCupsPrinter *printer);

#endif
