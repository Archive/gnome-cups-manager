#ifndef GNOME_PRINTER_LIST_H
#define GNOME_PRINTER_LIST_H

#include <glib.h>
#include <glib-object.h>

#include "eggiconlist.h"

G_BEGIN_DECLS

#define GNOME_TYPE_PRINTER_LIST            (gnome_printer_list_get_type())
#define GNOME_PRINTER_LIST(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOME_TYPE_PRINTER_LIST, GnomePrinterList))
#define GNOME_PRINTER_LIST_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GNOME_TYPE_PRINTER_LIST, GnomePrinterListClass))
#define GNOME_IS_PRINTER_LIST(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNOME_TYPE_PRINTER_LIST))
#define GNOME_IS_PRINTER_LIST_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), GNOME_TYPE_PRINTER_LIST))
#define GNOME_PRINTER_LIST_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), GNOME_TYPE_PRINTER_LIST, GnomePrinterListClass))

typedef struct _GnomePrinterList        GnomePrinterList;
typedef struct _GnomePrinterListClass   GnomePrinterListClass;
typedef struct _GnomePrinterListDetails GnomePrinterListDetails;

struct _GnomePrinterList {
	EggIconList parent;
	
	GnomePrinterListDetails *details;
};

struct _GnomePrinterListClass {
	EggIconListClass parent_class;
};

GType      gnome_printer_list_get_type                  (void);
GtkWidget *gnome_printer_list_new                       (void);
void       gnome_printer_list_get_command_sensitivities (GnomePrinterList *list,
							 gboolean         *activate,
							 gboolean         *pause,
							 gboolean         *resume,
							 gboolean         *make_default,
							 gboolean         *del,
							 gboolean         *properties,
							 gboolean	  *status);

void gnome_printer_list_new_printer (GnomePrinterList *list);

void gnome_printer_list_selection_activate (GnomePrinterList *list);
void gnome_printer_list_selection_pause (GnomePrinterList *list);
void gnome_printer_list_selection_resume (GnomePrinterList *list);
void gnome_printer_list_selection_make_default (GnomePrinterList *list);
void gnome_printer_list_selection_delete (GnomePrinterList *list);
void gnome_printer_list_selection_properties (GnomePrinterList *list);

G_END_DECLS
	
#endif
