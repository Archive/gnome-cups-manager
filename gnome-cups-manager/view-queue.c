#include <config.h>
#include "view-queue.h"

#include <cups/cups.h>
#include <cups/language.h>
#include <cups/http.h>
#include <cups/ipp.h>

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <glade/glade.h>

#include <libgnomecups/gnome-cups-request.h>
#include <libgnomecups/gnome-cups-printer.h>
#include <libgnomecups/gnome-cups-queue.h>
#include <libgnomecups/gnome-cups-ui-print.h>
#include <libgnomecups/gnome-cups-ui-util.h>
#include <libgnomecups/gnome-cups-permission.h>

#include "printer-properties.h"

enum {
	COLUMN_NAME,
	COLUMN_OWNER,
	COLUMN_ID,
	COLUMN_SIZE,
	COLUMN_STATE,
	COLUMN_STATE_INT,
	COLUMN_COMPLETE,
	LAST_COLUMN
};

#define KILOBYTE_FACTOR (1024.0)
#define MEGABYTE_FACTOR (KILOBYTE_FACTOR * 1024.0)
#define GIGABYTE_FACTOR (MEGABYTE_FACTOR * 1024.0)


/* from gnome-vfs */
static char*
format_size_for_display (unsigned long size)
{
        if (size >= (unsigned long)KILOBYTE_FACTOR) {
                gdouble displayed_size;                                                      
                if (size < (unsigned long) MEGABYTE_FACTOR) {
                        displayed_size = (gdouble) size / KILOBYTE_FACTOR;
                        return g_strdup_printf (_("%.1f K"),
                                                       displayed_size);
                } else if (size < (unsigned long) GIGABYTE_FACTOR) {
                        displayed_size = (gdouble) size / MEGABYTE_FACTOR;
                        return g_strdup_printf (_("%.1f MB"),
                                                       displayed_size);
                } else {
                        displayed_size = (gdouble) size / GIGABYTE_FACTOR;
                        return g_strdup_printf (_("%.1f GB"),
                                                       displayed_size);
                }
        } else
		return g_strdup_printf (ngettext ("1 byte", "%lu bytes", size ), size);
}

static void
list_store_set_job (GtkListStore *store, 
		    GtkTreeIter *iter,
		    GnomeCupsJob *job)
{
	char *complete_str;
	char *size;

	complete_str = g_strdup_printf (_("%d of %d"), 
					  job->pages_complete,
					  job->pages);
	
	size = format_size_for_display (job->size);
					  
	gtk_list_store_set (store, iter,
			    COLUMN_NAME, job->name,
			    COLUMN_ID, job->id,
			    COLUMN_OWNER, job->owner,
			    COLUMN_SIZE, size,
			    COLUMN_STATE, job->full_state,
			    COLUMN_STATE_INT, job->state,
			    COLUMN_COMPLETE, complete_str,
			    -1);

	g_free (complete_str);
	g_free (size);
}

static void
add_jobs (GladeXML *xml, const GList *jobs)
{
	GtkTreeView *view;
	GtkListStore *store;
	GtkTreeIter iter;
	const GList *l;
	
	view = GTK_TREE_VIEW (glade_xml_get_widget (xml, "queue_view"));
	store = GTK_LIST_STORE (gtk_tree_view_get_model (view));

	for (l = jobs; l != NULL; l = l->next) {
		GnomeCupsJob *job = l->data;
		gtk_list_store_append (store, &iter);
		list_store_set_job (store, &iter, job);
	}
}

static void
init_queue (GladeXML *xml, GnomeCupsQueue *queue)
{
	const GList *jobs;
	
	jobs = gnome_cups_queue_get_jobs (queue);
	add_jobs (xml, jobs);
}

static void
update_title (GladeXML *xml, GnomeCupsPrinter *printer)
{
	char *title;
	const char *name;
	const char *state;
	GtkWidget *window;

	name = gnome_cups_printer_get_name (printer);
	state = gnome_cups_printer_get_state_name (printer);
	if (gnome_cups_printer_get_is_default (printer)) {
		title = g_strdup_printf ("%s - %s (Default)", name, state);
	} else {
		title = g_strdup_printf ("%s - %s", name, state);
	}

	window = glade_xml_get_widget (xml, "queue_window");
	gtk_window_set_title (GTK_WINDOW (window), title);

	g_free (title);
}


static void
selection_has_paused_cb (GtkTreeModel *model,
			 GtkTreePath *path,
			 GtkTreeIter *iter, 
			 gpointer user_data)
{
	gboolean *paused = user_data;
	int state;
	
	gtk_tree_model_get (model, iter, COLUMN_STATE_INT, &state, -1);
	
	if (state == IPP_JOB_HELD || state == IPP_JOB_STOPPED) {
		*paused = TRUE;
	}
}

static void
selection_has_running_cb (GtkTreeModel *model,
			 GtkTreePath *path,
			 GtkTreeIter *iter, 
			 gpointer user_data)
{
	gboolean *running = user_data;
	int state;
	
	gtk_tree_model_get (model, iter, COLUMN_STATE_INT, &state, -1);
	
	if (state != IPP_JOB_HELD && state != IPP_JOB_STOPPED) {
		*running = TRUE;
	}
}

static void
selection_has_selection_cb (GtkTreeModel *model,
			    GtkTreePath *path,
			    GtkTreeIter *iter, 
			    gpointer user_data)
{
	gboolean *exists = user_data;
	*exists = TRUE;
}

static void
get_menu_sensitivities (GladeXML *xml,
			gboolean *pause,
			gboolean *resume, 
			gboolean *cancel)
{
	GtkTreeView *view;
	GtkTreeSelection *selection;
	
	view = GTK_TREE_VIEW (glade_xml_get_widget (xml, "queue_view"));

	selection = gtk_tree_view_get_selection (view);
	if (pause) {
		*pause = FALSE;
		if (gnome_cups_can_admin ())
			gtk_tree_selection_selected_foreach (selection,
				selection_has_running_cb, pause);
	}

	if (resume) {
		*resume = FALSE;
		if (gnome_cups_can_admin ())
			gtk_tree_selection_selected_foreach (selection,
				selection_has_paused_cb, resume);
	}

	if (cancel) {
		*cancel = FALSE;
		gtk_tree_selection_selected_foreach (selection,
						     selection_has_selection_cb,
						     cancel);
	}
}

static void
update_menus (GladeXML *xml)
{
	GnomeCupsPrinter *printer;
	GtkWidget *item;
	char *text;
	gboolean pause;
	gboolean resume;
	gboolean cancel;

	printer = g_object_get_data (G_OBJECT (xml), "printer");
	
	item = glade_xml_get_widget (xml, "set_as_default_item");
	gtk_widget_set_sensitive (item, !gnome_cups_printer_get_is_default (printer));

	item = glade_xml_get_widget (xml, "pause_or_resume_item");
	if (gnome_cups_printer_get_state (printer) == IPP_PRINTER_STOPPED) {
		text = _("_Resume Printer");
	} else {
		text = _("P_ause Printer");
	}
	gtk_label_set_text_with_mnemonic (GTK_LABEL (GTK_BIN (item)->child), text);

	get_menu_sensitivities (xml, &pause, &resume, &cancel);
	gtk_widget_set_sensitive (glade_xml_get_widget (xml, "pause_jobs_item"), pause);
	gtk_widget_set_sensitive (glade_xml_get_widget (xml, "resume_jobs_item"), resume);
	gtk_widget_set_sensitive (glade_xml_get_widget (xml, "cancel_jobs_item"), cancel);
}

static void
handle_error (GladeXML *xml, const char *prefix, GError *error)
{
	GtkWidget *window;
	
	window = glade_xml_get_widget (xml, "queue_window");
	gnome_cups_error_dialog (GTK_WINDOW (window), prefix, error);
}

static void
pause_or_resume_selected_cb (GtkWidget *item, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);
	GnomeCupsPrinter *printer;
	GError *error = NULL;
	
	printer = g_object_get_data (G_OBJECT (xml), "printer");
	if (gnome_cups_printer_get_state (printer) == IPP_PRINTER_STOPPED) {
		gnome_cups_printer_resume (printer, &error);
		if (error) {
			handle_error (xml, 
				      _("Could not resume printer"), 
				      error);
			g_error_free (error);
		}
	} else {
		gnome_cups_printer_pause (printer, &error);
		if (error) {
			handle_error (xml, 
				      _("Could not resume printer"), 
				      error);
			g_error_free (error);
		}
	}
}

static void
set_as_default_selected_cb (GtkWidget *item, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);
	GnomeCupsPrinter *printer;
	GError *error = NULL;
	
	printer = g_object_get_data (G_OBJECT (xml), "printer");
	
	gnome_cups_printer_set_default (printer, &error);
	if (error) {
		char *msg;
		msg = g_strdup_printf (_("Could not set %s as the default"),
				       gnome_cups_printer_get_name (printer));
		handle_error (xml, msg, error);
		g_free (msg);
		g_error_free (error);
	}
}

static void
test_page_selected_cb (GtkWidget *item, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);
	GtkWidget *window;
	GnomeCupsPrinter *printer;
	
	window = glade_xml_get_widget (xml, "queue_window");
	printer = g_object_get_data (G_OBJECT (xml), "printer");

	gnome_cups_print_test_page (printer, window);
}

static void
properties_selected_cb (GtkWidget *item, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);
	GnomeCupsPrinter *printer;
	
	printer = g_object_get_data (G_OBJECT (xml), "printer");
	
	gnome_cups_manager_printer_properties (printer);
}

static void
close_window (GladeXML *xml)
{
	GtkWidget *window;
	
	window = glade_xml_get_widget (xml, "queue_window");
	gtk_widget_destroy (window);
	
	g_object_unref (xml);
}

static void
close_selected_cb (GtkWidget *item, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);

	close_window (xml);
}

static void
queue_gone_cb (GtkWidget *item, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);

	close_window (xml);
}

static void
collect_jobs_cb (GtkTreeModel *model,
		 GtkTreePath *path,
		 GtkTreeIter *iter, 
		 gpointer user_data)
{
	GList **list = user_data;
	int id;
	
	gtk_tree_model_get (model, iter, COLUMN_ID, &id, -1);
	
	*list = g_list_prepend (*list, GINT_TO_POINTER (id));
}

static void
pause_jobs_cb (GtkWidget *widget, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);
	GnomeCupsQueue *queue;
	GtkTreeView *view;
	GtkTreeSelection *selection;
	GList *ids;
	GList *l;
	
	queue = g_object_get_data (G_OBJECT (xml), "queue");
	view = GTK_TREE_VIEW (glade_xml_get_widget (xml, "queue_view"));
	
	selection = gtk_tree_view_get_selection (view);
	
	ids = NULL;
	gtk_tree_selection_selected_foreach (selection, collect_jobs_cb, &ids);

	for (l = ids; l != NULL; l = l->next) {
		GError *error = NULL;
		int job = GPOINTER_TO_INT (l->data);
		gnome_cups_queue_pause_job (queue, job, &error);
		if (error) {
			handle_error (xml, 
				      _("Could not pause job"), 
				      error);
			g_error_free (error);
		}
	}
	
	g_list_free (ids);
}

static void
resume_jobs_cb (GtkWidget *widget, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);
	GnomeCupsQueue *queue;
	GtkTreeView *view;
	GtkTreeSelection *selection;
	GList *ids;
	GList *l;
	
	queue = g_object_get_data (G_OBJECT (xml), "queue");
	view = GTK_TREE_VIEW (glade_xml_get_widget (xml, "queue_view"));
	
	selection = gtk_tree_view_get_selection (view);
	
	ids = NULL;
	gtk_tree_selection_selected_foreach (selection, collect_jobs_cb, &ids);

	for (l = ids; l != NULL; l = l->next) {
		GError *error = NULL;
		int job = GPOINTER_TO_INT (l->data);
		gnome_cups_queue_resume_job (queue, job, &error);
		if (error) {
			handle_error (xml, 
				      _("Could not resume job"), error);
			g_error_free (error);
		}
	}
	
	g_list_free (ids);
}

static void
cancel_jobs_cb (GtkWidget *widget, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);
	GnomeCupsQueue *queue;
	GtkTreeView *view;
	GtkTreeSelection *selection;
	GList *ids;
	GList *l;
	
	queue = g_object_get_data (G_OBJECT (xml), "queue");
	view = GTK_TREE_VIEW (glade_xml_get_widget (xml, "queue_view"));
	
	selection = gtk_tree_view_get_selection (view);
	
	ids = NULL;
	gtk_tree_selection_selected_foreach (selection, collect_jobs_cb, &ids);

	for (l = ids; l != NULL; l = l->next) {
		GError *error = NULL;
		int job = GPOINTER_TO_INT (l->data);
		gnome_cups_queue_cancel_job (queue, job, &error);
		if (error) {
			handle_error (xml, 
				      _("Could not cancel job"), 
				      error);
			g_error_free (error);
		}
	}
	
	g_list_free (ids);
}

static void
setup_menus (GladeXML *xml)
{
	GtkWidget *item;
	
	item = glade_xml_get_widget (xml, "pause_or_resume_item");
	g_signal_connect (item, "activate", 
			  G_CALLBACK (pause_or_resume_selected_cb), xml);

	item = glade_xml_get_widget (xml, "set_as_default_item");
	g_signal_connect (item, "activate", 
			  G_CALLBACK (set_as_default_selected_cb), xml);

	item = glade_xml_get_widget (xml, "test_page_item");
	g_signal_connect (item, "activate", 
			  G_CALLBACK (test_page_selected_cb), xml);

	item = glade_xml_get_widget (xml, "properties_item");
	g_signal_connect (item, "activate", 
			  G_CALLBACK (properties_selected_cb), xml);

	item = glade_xml_get_widget (xml, "queue_close_item");
	g_signal_connect (item, "activate",
			  G_CALLBACK (close_selected_cb), xml);

	item = glade_xml_get_widget (xml, "pause_jobs_item");
	g_signal_connect (item, "activate",
			  G_CALLBACK (pause_jobs_cb), xml);
	item = glade_xml_get_widget (xml, "resume_jobs_item");
	g_signal_connect (item, "activate",
			  G_CALLBACK (resume_jobs_cb), xml);
	item = glade_xml_get_widget (xml, "cancel_jobs_item");
	g_signal_connect (item, "activate",
			  G_CALLBACK (cancel_jobs_cb), xml);
}

static GtkWidget *
create_popup_menu (GladeXML *xml)
{
	GtkWidget *menu;
	GtkWidget *item;
	gboolean pause;
	gboolean resume;
	gboolean cancel;
	
	menu = gtk_menu_new ();
	
	get_menu_sensitivities (xml, &pause, &resume, &cancel);

	item = gtk_menu_item_new_with_label (_("Pause"));
	g_signal_connect (item, "activate", 
			  G_CALLBACK (pause_jobs_cb), xml);
	gtk_widget_set_sensitive (item, pause);
	gtk_widget_show (item);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);

	item = gtk_menu_item_new_with_label (_("Resume"));
	g_signal_connect (item, "activate", 
			  G_CALLBACK (resume_jobs_cb), xml);
	gtk_widget_set_sensitive (item, resume);
	gtk_widget_show (item);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);

	item = gtk_separator_menu_item_new ();
	gtk_widget_show (item);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);

	item = gtk_menu_item_new_with_label (_("Cancel"));
	g_signal_connect (item, "activate", 
			  G_CALLBACK (cancel_jobs_cb), xml);
	gtk_widget_set_sensitive (item, cancel);
	gtk_widget_show (item);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);

	return menu;
}

static gboolean
tree_button_press_cb (GtkWidget *view, 
		      GdkEventButton *event, 
		      gpointer user_data)
{
	GtkTreePath *path;
	
	if (gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW (view),
					   event->x, event->y,
                                           &path, NULL, NULL, NULL)) {
                if (event->button == 3
                    && gtk_tree_selection_path_is_selected (gtk_tree_view_get_selection (GTK_TREE_VIEW (view)), path)) {
			/* Block button3 clicks so that popping up a
			 * context menu doesn't modify the selection */
			if (event->button == 3) {
				return TRUE;
			}
		}
	}
	
	return FALSE;
}

static gboolean
tree_button_release_cb (GtkWidget *tree, 
			GdkEventButton *event, 
			gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);
	
	if (event->button == 3) {
		GtkWidget *menu;

		menu = create_popup_menu (xml);

		gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL, 
				event->button, event->time);

		return TRUE;
	}

	return FALSE;
}

static void
selection_changed_cb (GtkTreeSelection *selection, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);
	
	update_menus (xml);
}

static void
setup_queue_tree (GladeXML *xml)
{
	GtkTreeView *view;
	GtkListStore *store;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeSelection *selection;
	
	view = GTK_TREE_VIEW (glade_xml_get_widget (xml, "queue_view"));

	renderer = gtk_cell_renderer_text_new ();
	column = gtk_tree_view_column_new_with_attributes (_("Name"),
							   renderer,
							   "text",
							   COLUMN_NAME,
							   NULL);
	gtk_tree_view_column_set_resizable (column, TRUE);
	gtk_tree_view_column_set_min_width (column, 100);
	gtk_tree_view_append_column (view, column);

	column = gtk_tree_view_column_new_with_attributes (_("Job Number"),
							   renderer,
							   "text",
							   COLUMN_ID,
							   NULL);
	gtk_tree_view_column_set_resizable (column, TRUE);
	gtk_tree_view_append_column (view, column);

	column = gtk_tree_view_column_new_with_attributes (_("Owner"),
							   renderer,
							   "text",
							   COLUMN_OWNER,
							   NULL);
	gtk_tree_view_column_set_min_width (column, 50);
	gtk_tree_view_column_set_resizable (column, TRUE);
	gtk_tree_view_append_column (view, column);

	column = gtk_tree_view_column_new_with_attributes (_("Size"),
							   renderer,
							   "text",
							   COLUMN_SIZE,
							   NULL);
	gtk_tree_view_column_set_min_width (column, 50);
	gtk_tree_view_column_set_resizable (column, TRUE);
	gtk_tree_view_append_column (view, column);
	column = gtk_tree_view_column_new_with_attributes (_("State"),
							   renderer,
							   "text",
							   COLUMN_STATE,
							   NULL);
	gtk_tree_view_column_set_resizable (column, TRUE);
	gtk_tree_view_append_column (view, column);

	store = gtk_list_store_new (LAST_COLUMN,
				    G_TYPE_STRING,
				    G_TYPE_STRING,
				    G_TYPE_INT,
				    G_TYPE_STRING,
				    G_TYPE_STRING,
				    G_TYPE_INT,
				    G_TYPE_STRING);
	
	gtk_tree_view_set_model (view, (GTK_TREE_MODEL (store)));

	g_signal_connect (view, "button_press_event", 
			  G_CALLBACK (tree_button_press_cb), xml);
	g_signal_connect (view, "button_release_event", 
			  G_CALLBACK (tree_button_release_cb), xml);

	selection = gtk_tree_view_get_selection (view);
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_MULTIPLE);

	g_signal_connect (selection, "changed", 
			  G_CALLBACK (selection_changed_cb), xml);

}

static gboolean
delete_event_cb (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);
	
	g_object_unref (xml);

	return FALSE;
}

static void
disconnect_view (gpointer user_data, GObject *object)
{
	GnomeCupsPrinter *printer = user_data;
	g_object_set_data (G_OBJECT (printer), "queue-view", NULL);
}

static void
is_default_changed_cb (GnomeCupsPrinter *printer, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);
	
	update_title (xml, printer);
	update_menus (xml);
}

static void
attributes_changed_cb (GnomeCupsPrinter *printer, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);
	
	update_title (xml, printer);
	update_menus (xml);
}

static void
jobs_added_cb (GnomeCupsQueue *queue, GList *jobs, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);
	
	add_jobs (xml, jobs);
}

static gboolean
iter_for_job (GtkTreeModel *model, GtkTreeIter *iter, GnomeCupsJob *job)
{
	gboolean have_iter;

	have_iter = gtk_tree_model_get_iter_first (model, iter);
	while (have_iter) {
		int id;
		gtk_tree_model_get (model, iter, COLUMN_ID, &id, -1);
		if (id == job->id) {
			return TRUE;
		}
		have_iter = gtk_tree_model_iter_next (model, iter);
	}

	return have_iter;
}

static void
jobs_removed_cb (GnomeCupsQueue *queue, GList *jobs, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);
	GtkTreeView *view;
	GtkTreeModel *model;
	GList *l;
 
	view = GTK_TREE_VIEW (glade_xml_get_widget (xml, "queue_view"));
	model = gtk_tree_view_get_model (view);
	
	for (l = jobs; l != NULL; l = l->next) {
		GtkTreeIter iter;
		GnomeCupsJob *job = l->data;
		if (iter_for_job (model, &iter, job)) {
			gtk_list_store_remove (GTK_LIST_STORE (model),
					       &iter);
		}
	}
}

static void
jobs_changed_cb (GnomeCupsQueue *queue, GList *jobs, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);
	GtkTreeView *view;
	GtkTreeModel *model;
	GList *l;

	view = GTK_TREE_VIEW (glade_xml_get_widget (xml, "queue_view"));
	model = gtk_tree_view_get_model (view);
	
	for (l = jobs; l != NULL; l = l->next) {
		GtkTreeIter iter;
		GnomeCupsJob *job = l->data;
		if (iter_for_job (model, &iter, job)) {
			list_store_set_job (GTK_LIST_STORE (model),
					    &iter, job);
		}
	}
}

void
gnome_cups_manager_view_queue (GnomeCupsPrinter *printer)
{
	GladeXML *xml;
	GtkWidget *window;
	GnomeCupsQueue *queue;
	char *icon_name;

	xml = g_object_get_data (G_OBJECT (printer), "queue-view");
	if (xml) {
		window = glade_xml_get_widget (xml, "queue_window");
		gtk_window_present (GTK_WINDOW (window));
		return;
	}
	
	xml = glade_xml_new (GNOME_CUPS_MANAGER_DATADIR "/gnome-cups-manager.glade",
			     "queue_window",
			     GETTEXT_PACKAGE);

	g_object_set_data (G_OBJECT (printer), "queue-view", xml);

	g_object_ref (printer);
	queue = gnome_cups_printer_get_queue (printer);
	g_signal_connect_object (queue, "gone", 
				 G_CALLBACK (queue_gone_cb), xml, 0);
	g_object_set_data_full (G_OBJECT (xml), "printer", 
				printer, g_object_unref);
	g_object_set_data_full (G_OBJECT (xml), "queue", 
				queue, g_object_unref);
	
	g_object_weak_ref (G_OBJECT (xml), disconnect_view, printer);

	window = glade_xml_get_widget (xml, "queue_window");
	g_signal_connect (window, "delete_event", 
			  G_CALLBACK (delete_event_cb), xml);

	g_signal_connect_object (printer, 
				 "is_default_changed",
				 G_CALLBACK (is_default_changed_cb),
				 xml, 0);
	g_signal_connect_object (printer, 
				 "attributes_changed",
				 G_CALLBACK (attributes_changed_cb),
				 xml, 0);
	g_signal_connect_object (queue, 
				 "jobs_added",
				 G_CALLBACK (jobs_added_cb),
				 xml, 0);
	g_signal_connect_object (queue, 
				 "jobs_changed",
				 G_CALLBACK (jobs_changed_cb),
				 xml, 0);
	g_signal_connect_object (queue, 
				 "jobs_removed",
				 G_CALLBACK (jobs_removed_cb),
				 xml, 0);
	
	setup_queue_tree (xml);
	setup_menus (xml);

	update_menus (xml);
	update_title (xml, printer);
	init_queue (xml, queue);
	
	gnome_cups_printer_get_icon (printer, &icon_name, NULL);
	set_window_icon (window, icon_name);
	g_free (icon_name);
	
	gtk_widget_show (window);

	watch_window (window);
}
