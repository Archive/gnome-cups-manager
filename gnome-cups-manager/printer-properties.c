/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#undef GTK_DISABLE_DEPRECATED

#include <config.h>
#include "printer-properties.h"

#include <cups/cups.h>
#include <cups/language.h>
#include <cups/http.h>
#include <cups/ipp.h>

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <libgnomeui/gnome-icon-theme.h>

#include <libgnomecups/gnome-cups-request.h>
#include <libgnomecups/gnome-cups-printer.h>
#include <libgnomecups/gnome-cups-queue.h>
#include <libgnomecups/gnome-cups-util.h>
#include <libgnomecups/gnome-cups-ui-print.h>
#include <libgnomecups/gnome-cups-ui-util.h>
#include <libgnomecups/gnome-cups-ui-driver.h>
#include <libgnomecups/gnome-cups-ui-connection.h>
#include <libgnomecups/gnome-cups-permission.h>

static void
unref_group (gpointer user_data, GObject *obj)
{
	g_object_unref (G_OBJECT (user_data));
}

static void
setup_size_group (GladeXML *xml,
		  GtkSizeGroupMode mode,
		  int n_items,
		  const char **items)
{
	int i;
	GtkSizeGroup *group = gtk_size_group_new (mode);

	for (i = 0; i < n_items; i++)
		gtk_size_group_add_widget (group,
			glade_xml_get_widget (xml, items[i]));
	g_object_weak_ref (G_OBJECT (xml), unref_group, group);
}

static void
disconnect_view (gpointer printer, GObject *object)
{
	g_object_set_data (G_OBJECT (printer), "properties-window", NULL);
}

static void
label_vs_entry (GladeXML *xml,
		char const *entry, char const *label, char const *val)
{
	GtkWidget *w = glade_xml_get_widget (xml, entry);
	if (gnome_cups_can_admin ())
		gtk_entry_set_text (GTK_ENTRY (w), val);
	else
		gtk_widget_hide (w);
	w = glade_xml_get_widget (xml, label);
	if (gnome_cups_can_admin ())
		gtk_widget_hide (w);
	else
		gtk_label_set_text (GTK_LABEL (w), val);
}

static void
update_general_page (GladeXML *xml)
{
	GnomeCupsPrinter *printer = g_object_get_data (G_OBJECT (xml), "printer");
	GtkWidget *w;
	GdkPixbuf *pixbuf;
	char	  *icon_name;

	gnome_cups_printer_get_icon (printer, &icon_name, NULL);
	pixbuf = gtk_icon_theme_load_icon (gtk_icon_theme_get_default (),
		icon_name, 48, GTK_ICON_LOOKUP_USE_BUILTIN, NULL);
	w = glade_xml_get_widget (xml, "printer_image");
	gtk_image_set_from_pixbuf (GTK_IMAGE (w), pixbuf);

	w = glade_xml_get_widget (xml, "name_label");
	gtk_label_set_text (GTK_LABEL (w),
		gnome_cups_printer_get_name (printer));
	label_vs_entry (xml, "description_entry", "description_label",
		gnome_cups_printer_get_description (printer));
	label_vs_entry (xml, "location_entry", "location_label",
		gnome_cups_printer_get_location (printer));

	w = glade_xml_get_widget (xml, "status_label");
	gtk_label_set_text (GTK_LABEL (w),
		gnome_cups_printer_get_full_state (printer));
}

typedef struct {
	GnomeCupsPrinter *printer;
	char *value;
	GtkWidget *entry;
} EntryHandlerInfo;

static gboolean
cb_entry_idle_handler (EntryHandlerInfo *data)
{
	gchar const *name = gtk_widget_get_name (data->entry);

	g_return_val_if_fail (name != NULL, FALSE);

	if (0 == strcmp (name, "description_entry"))
		gnome_cups_printer_set_description (data->printer, data->value, NULL);
	else if (0 == strcmp (name, "location_entry"))
		 gnome_cups_printer_set_location (data->printer, data->value, NULL);

	g_object_unref (data->printer);
	g_free (data->value);
	g_free (data);

	return FALSE;
}

/* You can't hit the main loop in a GtkEntry's focus_out_event, so queue this
 * in an idle handler */
static gboolean
cb_entry_activated (GtkEntry *entry, GladeXML *xml)
{
	EntryHandlerInfo *data = g_new0 (EntryHandlerInfo, 1);
	GnomeCupsPrinter *printer = g_object_get_data (G_OBJECT (xml), "printer");
	g_object_ref (printer);
	data->printer = printer;
	data->value = g_strdup (gtk_entry_get_text (entry));
	data->entry = GTK_WIDGET (entry);
	g_idle_add ((GSourceFunc)cb_entry_idle_handler, data);
	return FALSE;
}
static gboolean
cb_entry_lost_focus (GtkEntry *entry, G_GNUC_UNUSED GdkEventFocus *event,
		     GladeXML *xml)
{
	return cb_entry_activated (entry, xml);
}

static void
setup_entry (GladeXML *xml, char const *name)
{
	GtkWidget *w = glade_xml_get_widget (xml, name);
	g_signal_connect (w, "focus_out_event",
		G_CALLBACK (cb_entry_lost_focus), xml);
	g_signal_connect (w, "activate",
		G_CALLBACK (cb_entry_activated), xml);
}

static void
check_toggled_cb (GtkWidget *widget,
		  gpointer user_data)
{
	GnomeCupsPrinter *printer;
	GnomeCupsPrinterOption *option;
	char *value;

	printer = GNOME_CUPS_PRINTER (user_data);

	option = g_object_get_data (G_OBJECT (widget), "option");

	value = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget)) ? "True" : "False";

	gnome_cups_printer_set_option_value (printer,
					     option->id,
					     value);
}

static void
handle_option (GladeXML *xml,
	       const char *key)
{
	GHashTable *handled_options;

	handled_options = g_object_get_data (G_OBJECT (xml),
					     "handled_options");

	g_hash_table_insert (handled_options,
			     g_strdup (key),
			     GINT_TO_POINTER (TRUE));
}

static gboolean
option_is_handled (GladeXML *xml,
		   const char *key)
{
	GHashTable *handled_options;

	handled_options = g_object_get_data (G_OBJECT (xml),
					     "handled_options");
	return (gboolean)g_hash_table_lookup (handled_options, key);
}

static gboolean
hookup_check_button (GladeXML *xml,
		     GnomeCupsPrinter *printer,
		     GtkWidget *check,
		     const char *key)
{
	GnomeCupsPrinterOption *option;

	option = gnome_cups_printer_get_option (printer, key);
	if (option) {
		gboolean is_active;
		g_return_val_if_fail (option->n_choices == 2, FALSE);

		handle_option (xml, key);

		is_active = (strcmp (option->value, "True") == 0);
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check),
					      is_active);

		g_object_set_data_full (G_OBJECT (check), "option",
					option,
					(GDestroyNotify)gnome_cups_printer_option_free);
		g_signal_connect (check, "toggled",
				  G_CALLBACK (check_toggled_cb),
				  printer);


		return TRUE;
	} else {
		gtk_widget_set_sensitive (check, FALSE);
		return FALSE;
	}
}


static void
option_item_activate_cb (GtkWidget *item, gpointer user_data)
{
	GnomeCupsPrinter *printer = GNOME_CUPS_PRINTER (user_data);
	gnome_cups_printer_set_option_value (printer,
		g_object_get_data (G_OBJECT (item), "id"),
		g_object_get_data (G_OBJECT (item), "value"));
}

static gboolean
hookup_option_menu (GladeXML *xml,
		    GnomeCupsPrinter *printer,
		    GtkWidget *option_menu,
		    const char *key)
{
	GnomeCupsPrinterOption *option = gnome_cups_printer_get_option (printer, key);
	if (option) {
		GtkWidget *menu;
		int i;
		int selected = 0;

		handle_option (xml, key);

		menu = gtk_menu_new ();
		for (i = 0; i < option->n_choices; i++) {
			GtkWidget *item;
			GnomeCupsPrinterOptionChoice *choice = &option->choices[i];

			if (!strcmp (choice->value, option->value))
				selected = i;

			item = gtk_menu_item_new_with_label (choice->text);
			gtk_menu_shell_append (GTK_MENU_SHELL (menu),
					       item);
			g_object_set_data_full (G_OBJECT (item), "id",
						g_strdup (option->id),
						g_free);
			g_object_set_data_full (G_OBJECT (item), "value",
						g_strdup (choice->value),
						g_free);
			g_signal_connect (item, "activate",
					  G_CALLBACK (option_item_activate_cb),
					  printer);
		}
		gtk_widget_show_all (menu);
		gtk_option_menu_set_menu (GTK_OPTION_MENU (option_menu), menu);
		gtk_option_menu_set_history (GTK_OPTION_MENU (option_menu),
					     selected);
		gnome_cups_printer_option_free (option);

		return TRUE;
	} else {
		gtk_widget_set_sensitive (option_menu, FALSE);
		return FALSE;
	}
}

static gboolean
hookup_option_menu_xml (GladeXML *xml,
			const char *option_menu_name,
			const char *key)
{
	GtkWidget *option_menu;
	GnomeCupsPrinter *printer;

	printer = g_object_get_data (G_OBJECT (xml), "printer");
	option_menu = glade_xml_get_widget (xml, option_menu_name);
	g_return_val_if_fail (option_menu != NULL && GTK_IS_OPTION_MENU (option_menu), FALSE);

	return hookup_option_menu (xml, printer, option_menu, key);
}

static void
hookup_optional_option_menu_xml (GladeXML *xml,
				 const char *option_menu_name,
				 const char *key,
				 const char *hide_widget)
{
	if (!hookup_option_menu_xml (xml, option_menu_name, key)) {
		gtk_widget_hide (glade_xml_get_widget (xml, hide_widget));
		gtk_widget_hide (glade_xml_get_widget (xml, option_menu_name));
	}
}

static void
test_page_clicked_cb (GtkWidget *button, gpointer user_data)
{
	GladeXML *xml;
	GtkWidget *window;
	GnomeCupsPrinter *printer;

	xml = GLADE_XML (user_data);

	window = glade_xml_get_widget (xml, "properties_window");
	printer = g_object_get_data (G_OBJECT (xml), "printer");

	gnome_cups_print_test_page (printer, window);
}

static void
setup_general_page (GladeXML *xml)
{
	update_general_page (xml);

	if (gnome_cups_can_admin ()) {
		setup_entry (xml, "description_entry");
		setup_entry (xml, "location_entry");
	}

	hookup_option_menu_xml (xml, "resolution_option_menu", "Resolution");
}

static void
setup_paper_page (GladeXML *xml)
{
	static const char *labels[] =
		{ "paper_size_label", "paper_type_label",
		  "source_label", "orientation_label",
		  "double_sided_label", "binding_label" };
	static const char *widgets[] =
		{ "paper_size_option_menu", "paper_type_option_menu",
		  "source_option_menu", "orientation_option_menu",
		  "double_sided_option_menu", "binding_option_menu" };

	setup_size_group (xml, GTK_SIZE_GROUP_HORIZONTAL,
			  G_N_ELEMENTS (labels), labels);
	setup_size_group (xml, GTK_SIZE_GROUP_HORIZONTAL,
			  G_N_ELEMENTS (widgets), widgets);

	hookup_option_menu_xml (xml, "paper_size_option_menu", "PageSize");
	hookup_option_menu_xml (xml, "paper_type_option_menu", "MediaType");
	hookup_option_menu_xml (xml, "source_option_menu", "InputSlot");
	hookup_option_menu_xml (xml, "double_sided_option_menu", "Duplex");
	hookup_optional_option_menu_xml (xml,
		"binding_option_menu", "Binding", "binding_label");
}

static void
update_title (GladeXML *xml)
{
	GnomeCupsPrinter *printer;
	GtkWidget *window;
	char *title;

	printer = g_object_get_data (G_OBJECT (xml), "printer");

	title = g_strdup_printf ("%s Properties",
				 gnome_cups_printer_get_name (printer));
	window = glade_xml_get_widget (xml, "properties_window");
	gtk_window_set_title (GTK_WINDOW (window), title);
	g_free (title);
}

static void
close_window (GladeXML *xml)
{
	GtkWidget *window;

	window = glade_xml_get_widget (xml, "properties_window");
	gtk_widget_destroy (window);

	g_object_unref (xml);
}

static void
close_clicked_cb (GtkWidget *item, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);

	close_window (xml);
}

static gboolean
delete_event_cb (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);

	g_object_unref (xml);

	return FALSE;
}

static void
printer_gone_cb (GnomeCupsPrinter *printer, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);

	close_window (xml);
}

static void
set_atk_relation (GtkWidget *label, GtkWidget *widget)
{
	AtkObject *atk_widget;
	AtkObject *atk_label;
	AtkRelationSet *relation_set;
	AtkRelation *relation;
	AtkObject *targets[1];

	atk_widget = gtk_widget_get_accessible (widget);
	atk_label = gtk_widget_get_accessible (label);

	/* Create the label -> widget relation */
	targets[0] = atk_widget;
	relation_set = atk_object_ref_relation_set (atk_label);
	relation = atk_relation_new (targets, 1, ATK_RELATION_LABEL_FOR);
	atk_relation_set_add (relation_set, relation);
	g_object_unref (relation);
	g_object_unref (relation_set);

	/* Create the widget -> label relation */
	targets[0] = atk_label;
	relation_set = atk_object_ref_relation_set (atk_widget);
	relation = atk_relation_new (targets, 1, ATK_RELATION_LABELLED_BY);
	atk_relation_set_add (relation_set, relation);
	g_object_unref (relation);
	g_object_unref (relation_set);
}

static void
printer_attributes_changed_cb (GnomeCupsPrinter *printer, gpointer user_data)
{
	GladeXML *xml = GLADE_XML (user_data);

	update_general_page (xml);
}

static GtkWidget *
create_option_editor (GladeXML *xml,
		      GnomeCupsPrinter *printer,
		      GnomeCupsPrinterOption *option,
		      GtkSizeGroup *label_group,
		      GtkSizeGroup *editor_group)
{
	GtkWidget *hbox;
	GtkWidget *label;

	hbox = gtk_hbox_new (FALSE, 4);

	label = gtk_label_new (option->text);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);
	gtk_size_group_add_widget (label_group, label);

	gtk_box_pack_start (GTK_BOX (hbox), label,
			    FALSE, FALSE, 0);


	if (option->type == GNOME_CUPS_PRINTER_OPTION_BOOLEAN) {
		GtkWidget *check;
		check = gtk_check_button_new ();
		gtk_widget_show (check);
		gtk_box_pack_start (GTK_BOX (hbox), check,
				    FALSE, FALSE, 0);
		set_atk_relation (label, check);
		hookup_check_button (xml, printer, check, option->id);
	} else if (option->type == GNOME_CUPS_PRINTER_OPTION_PICK_ONE) {
		GtkWidget *option_menu;

		option_menu = gtk_option_menu_new ();
		gtk_widget_show (option_menu);
		gtk_size_group_add_widget (editor_group, option_menu);
		gtk_box_pack_start (GTK_BOX (hbox), option_menu,
				    FALSE, FALSE, 0);
		set_atk_relation (label, option_menu);
		hookup_option_menu (xml, printer, option_menu, option->id);
	}

	return hbox;
}

static void
setup_advanced_page (GladeXML *xml)
{
	GnomeCupsPrinter *printer;
	GtkSizeGroup *label_group;
	GtkSizeGroup *editor_group;
	GtkWidget *scrolled;
	GtkWidget *vbox;
	GList *options;
	GList *l;
	int num_editors;

	printer = g_object_get_data (G_OBJECT (xml), "printer");
	scrolled = glade_xml_get_widget (xml, "advanced_scrolled");

	vbox = gtk_vbox_new (FALSE, 4);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 6);

	label_group = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);
	editor_group = gtk_size_group_new (GTK_SIZE_GROUP_HORIZONTAL);

	g_object_weak_ref (G_OBJECT (xml), unref_group, label_group);
	g_object_weak_ref (G_OBJECT (xml), unref_group, editor_group);

	options = gnome_cups_printer_get_options (printer);

	num_editors = 0;
	for (l = options; l != NULL; l = l->next) {
		GnomeCupsPrinterOption *option = l->data;

		if (!option_is_handled (xml, option->id)) {
			GtkWidget *widget;

			widget = create_option_editor (xml,
						       printer, option,
						       label_group, editor_group);
			gtk_widget_show (widget);

			gtk_box_pack_start (GTK_BOX (vbox), widget, FALSE, FALSE, 0);
			num_editors++;
		}
	}

	gtk_widget_show (vbox);

	gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (scrolled),
					       vbox);

	gnome_cups_printer_option_list_free (options);
}

static void
cb_driver_changed (GCupsDriverSelector *ds, GnomeCupsPrinter *printer)
{
	GCupsPPD const *ppd = gcups_driver_selector_get (ds);

	if (ppd != NULL) {
		ipp_t *request = gnome_cups_request_new_for_printer (
			CUPS_ADD_PRINTER, printer);
		ippAddString (request, IPP_TAG_PRINTER, IPP_TAG_NAME,
			"ppd-name", NULL, gnome_cups_strdup (ppd->filename));
		gnome_cups_request_execute_async (request, NULL, "/admin/", NULL, NULL, NULL);
	}
}

static void
setup_driver_page (GladeXML *xml, GnomeCupsPrinter *printer)
{
	GtkWidget  *w, *ds;
	ppd_file_t *ppd;
	ppd_attr_t *nickname;

	if (!gnome_cups_can_admin ())
		return;
	if (printer == NULL)
		return;
	ppd = gnome_cups_printer_get_ppd (printer);
	if (ppd == NULL)
		return;

	nickname = ppdFindAttr (ppd, "NickName", NULL);
	if (nickname != NULL) {
		ds = gcups_driver_selector_new ();
		w = glade_xml_get_widget (xml, "prop_notebook");
		gtk_notebook_append_page (GTK_NOTEBOOK (w),
			ds, gtk_label_new (_("Driver")));
		gcups_driver_selector_set_nickname (GCUPS_DRIVER_SELECTOR (ds),
			nickname->value);
		gtk_widget_show (ds);

		g_signal_connect_object (ds,
			"changed",
			G_CALLBACK (cb_driver_changed), printer, 0);
	}
	ppdClose (ppd);
}

static void
cb_connection_changed (GCupsConnectionSelector *cs, GnomeCupsPrinter *printer)
{
	char *uri = gcups_connection_selector_get_uri (cs);

	if (uri != NULL) {
		ipp_t *request = gnome_cups_request_new_for_printer (
			CUPS_ADD_PRINTER, printer);
		ippAddString (request, IPP_TAG_PRINTER, IPP_TAG_URI,
			"device-uri", NULL, gnome_cups_strdup (uri));
		gnome_cups_request_execute_async (request, NULL, "/admin/", NULL, NULL, NULL);
		g_free (uri);
	}
}

static void
cb_display_uri (GnomeCupsPrinter *printer, GCupsConnectionSelector *cs)
{
	char const *uri = gnome_cups_printer_get_device_uri (printer);
	if (uri == NULL)
		return;
	gcups_connection_selector_set_uri (cs, uri);
}

static void
setup_connection_page (GladeXML *xml, GnomeCupsPrinter *printer)
{
	GtkWidget  *w, *cs;
	if (!gnome_cups_can_admin ())
		return;
	if (printer == NULL)
		return;

	cs = gcups_connection_selector_new ();
	w = glade_xml_get_widget (xml, "prop_notebook");
	gtk_notebook_append_page (GTK_NOTEBOOK (w),
		cs, gtk_label_new (_("Connection")));
	if (gnome_cups_printer_get_attributes_initialized (printer))
		cb_display_uri (printer, GCUPS_CONNECTION_SELECTOR (cs));
	else if (NULL == g_object_get_qdata (G_OBJECT (cs), g_quark_from_static_string ("pending"))) {
		gulong handle = g_signal_connect_object (printer, "attributes-changed",
			G_CALLBACK (cb_display_uri), cs, 0);
		g_object_set_qdata (G_OBJECT (cs),
			g_quark_from_static_string ("pending"),
			GUINT_TO_POINTER (handle));
	}
	gtk_widget_show (cs);
	g_signal_connect_object (cs,
		"changed",
		G_CALLBACK (cb_connection_changed), printer, 0);
}

static void
cb_become_admin (GnomeCupsPrinter *printer)
{
	char const *argv[] = { "-p", NULL };
	GladeXML *xml = g_object_get_data (G_OBJECT (printer), "properties-window");
	argv[1] = gnome_cups_printer_get_name (printer);
	if (gnome_cups_spawn ("gnome-cups-manager", G_N_ELEMENTS (argv), argv,
			      TRUE, glade_xml_get_widget (xml, "properties_window")))
		gtk_main_quit ();
}

void
gnome_cups_manager_printer_properties (GnomeCupsPrinter *printer)
{
	GladeXML *xml;
	GtkWidget *window;
	GtkWidget *button;
	GHashTable *handled_options;
	char *icon_name;

	xml = g_object_get_data (G_OBJECT (printer), "properties-window");
	if (xml) {
		window = glade_xml_get_widget (xml, "properties_window");
		gtk_window_present (GTK_WINDOW (window));
		return;
	}

	xml = glade_xml_new (GNOME_CUPS_MANAGER_DATADIR "/gnome-cups-manager.glade",
			     "properties_window",
			     GETTEXT_PACKAGE);
	g_object_set_data (G_OBJECT (printer), "properties-window", xml);

	g_object_ref (printer);
	g_object_set_data_full (G_OBJECT (xml), "printer",
				printer, g_object_unref);
	g_signal_connect_object (printer, "gone",
				 G_CALLBACK (printer_gone_cb), xml, 0);
	g_signal_connect_object (printer, "attributes_changed",
				 G_CALLBACK (printer_attributes_changed_cb), xml, 0);

	g_object_weak_ref (G_OBJECT (xml), disconnect_view, printer);

	/* Handled options.  As ppd options are hooked into the ui they
	 * are added to this hash table so that they aren't duplicated
	 * in the advanced tab */
	handled_options = g_hash_table_new_full (g_str_hash,
						 g_str_equal,
						 g_free,
						 NULL);
	g_object_set_data_full (G_OBJECT (xml), "handled_options",
				handled_options,
				(GDestroyNotify)g_hash_table_destroy);

	update_title (xml);
	setup_general_page (xml);
	setup_paper_page (xml);
	setup_advanced_page (xml);
	setup_driver_page (xml, printer);
	setup_connection_page (xml, printer);

	button = glade_xml_get_widget (xml, "admin_button");
	if (gnome_cups_can_admin ())
		gtk_widget_hide (button);
	else
		g_signal_connect_swapped (button,
			"clicked",
			G_CALLBACK (cb_become_admin), printer);

	button = glade_xml_get_widget (xml, "test_page_button");
	g_signal_connect (button,
		"clicked",
		G_CALLBACK (test_page_clicked_cb), xml);

	window = glade_xml_get_widget (xml, "properties_window");
	g_signal_connect (window,
		"delete_event",
		G_CALLBACK (delete_event_cb), xml);

	button = glade_xml_get_widget (xml, "close_button");
	g_signal_connect (button,
		"clicked",
		G_CALLBACK (close_clicked_cb), xml);

	gnome_cups_printer_get_icon (printer, &icon_name, NULL);
	set_window_icon (window, icon_name);
	g_free (icon_name);

	gtk_widget_show (window);

	watch_window (window);
}
