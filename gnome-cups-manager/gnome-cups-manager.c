/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* gnome-printers-view
 * 
 *  Copyright (C) 2002 Ximian Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this library; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Author: Dave Camp <dave@ximian.com>
 *
 */

#include <config.h>

#include "gnome-printer-list.h"
#include "view-printers.h"
#include "view-queue.h"
#include "printer-properties.h"

#include <cups/cups.h>

#include <stdlib.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-listener.h>
#include <bonobo/bonobo-main.h>

#include <glib.h>
#include <glib/gi18n.h>
#include <glade/glade.h>
#include <gtk/gtk.h>
#include <libgnomeui/gnome-client.h>
#include <libgnomeui/gnome-ui-init.h>
#include <libgnomeui/gnome-icon-theme.h>
#include <libgnomecups/gnome-cups-util.h>
#include <libgnomecups/gnome-cups-ui-init.h>
#include <libgnomecups/gnome-cups-printer.h>
#include <libgnomecups/gnome-cups-request.h>
#include <libgnomecups/gnome-cups-ui-print.h>
#include <libgnomecups/gnome-cups-ui-util.h>

static void
check_cups (void) 
{
	if (!gnome_cups_check_daemon ()) {
		GtkWidget *dialog;
		
		dialog = gtk_message_dialog_new (NULL, 
						 GTK_DIALOG_DESTROY_WITH_PARENT, 
						 GTK_MESSAGE_ERROR,
						 GTK_BUTTONS_OK,
						 _("The CUPS server could not be contacted."));
		gtk_dialog_run (GTK_DIALOG (dialog));
		gtk_widget_destroy (dialog); 
		exit (1);
	}
}

static void
event_cb (BonoboListener *listener, const char *event_name, 
	  const BonoboArg *arg, CORBA_Environment *ev,
	  gpointer user_data)
{
	char *printer_name;
	GnomeCupsPrinter *printer;

	if (!strcmp (event_name, "view_printers")) {
		gnome_cups_manager_view_printers ();
	} else {
		printer_name = BONOBO_ARG_GET_STRING (arg);
		
		printer = gnome_cups_printer_get (printer_name);
		
		if (printer) {
			if (!strcmp (event_name, "show_printer")) {
				gnome_cups_manager_view_queue (printer);
			} else  if (!strcmp (event_name, "printer_properties")) {
				gnome_cups_manager_printer_properties (printer);
			}
			g_object_unref (printer);
		} else {
			GtkWidget *dialog;
			
			dialog = gtk_message_dialog_new (NULL, 
							 0,
							 GTK_MESSAGE_ERROR,
							 GTK_BUTTONS_OK,
							 _("Printer not found: %s"), 
							 printer_name);
			gtk_dialog_run (GTK_DIALOG (dialog));
			gtk_widget_destroy (dialog); 
		}
	}
}

static Bonobo_Listener
get_listener (gboolean *is_new)
{
	Bonobo_Listener ret;
	CORBA_Environment ev;
	
	CORBA_exception_init (&ev);
	ret = bonobo_activation_activate_from_id ("OAFIID:gnome-printer-view",
						  Bonobo_ACTIVATION_FLAG_EXISTING_ONLY,
						  NULL, &ev);
	if (!BONOBO_EX (&ev) && !CORBA_Object_is_nil (ret, &ev)) {
		*is_new = FALSE;
	} else {
		Bonobo_RegistrationResult reg_result;
		BonoboListener *listener;
		
		listener = bonobo_listener_new (event_cb, NULL);
		
		*is_new = TRUE;
		ret = BONOBO_OBJREF (listener);

		reg_result = bonobo_activation_register_active_server
			("OAFIID:gnome-printer-view", ret, NULL);
		switch (reg_result) {
		case Bonobo_ACTIVATION_REG_SUCCESS :
			break;
		case Bonobo_ACTIVATION_REG_NOT_LISTED :
			g_print ("not listed\n");
			break;
		case Bonobo_ACTIVATION_REG_ALREADY_ACTIVE :
			g_print ("already active\n");
			break;
		case Bonobo_ACTIVATION_REG_ERROR :
			g_print ("error\n");
			break;
		}
	}

	CORBA_exception_free (&ev);

	return ret;
}


static void
show_printer (Bonobo_Listener listener, const char *printer)
{
	CORBA_Environment ev;
	BonoboArg *arg;
	
	arg = bonobo_arg_new (BONOBO_ARG_STRING);
	BONOBO_ARG_SET_STRING (arg, printer);

	CORBA_exception_init (&ev);
	Bonobo_Listener_event (listener, "show_printer", arg, &ev);
	CORBA_exception_free (&ev);

	CORBA_free (arg);
}	

static void
printer_properties (Bonobo_Listener listener, const char *printer)
{
	CORBA_Environment ev;
	BonoboArg *arg;
	
	arg = bonobo_arg_new (BONOBO_ARG_STRING);
	BONOBO_ARG_SET_STRING (arg, printer);

	CORBA_exception_init (&ev);
	Bonobo_Listener_event (listener, "printer_properties", arg, &ev);
	CORBA_exception_free (&ev);

	CORBA_free (arg);
}

static void
view_printers (Bonobo_Listener listener)
{
	CORBA_Environment ev;
	BonoboArg *arg;
	
	arg = bonobo_arg_new (BONOBO_ARG_STRING);
	BONOBO_ARG_SET_STRING (arg, "");

	CORBA_exception_init (&ev);
	Bonobo_Listener_event (listener, "view_printers", arg, &ev);
	CORBA_exception_free (&ev);

	CORBA_free (arg);
}

int
main (int argc, char *argv[])
{
	GnomeProgram *program;
	Bonobo_Listener listener;
	GOptionContext *context;
	int i;
	gboolean is_new;
	gboolean properties = FALSE;
	gboolean view = FALSE;
	char **printers = NULL;

	const GOptionEntry options[] = {
		{ "properties", 'p', 0, G_OPTION_ARG_NONE,  &properties,
		  N_("Show printer properties for printers listed on the command line"), NULL
		},
		{ "view", 'v', 0, G_OPTION_ARG_NONE, &view,
		  N_("View the queues of printers listed on the command line"), NULL
		},
		{ G_OPTION_REMAINING, 0, 0, G_OPTION_ARG_STRING_ARRAY, &printers,
		  NULL, N_("[PRINTER...]") },
		{ NULL }
	};

	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

        context = g_option_context_new (NULL);
#if GLIB_CHECK_VERSION (2, 12, 0)
	g_option_context_set_translation_domain (context, GETTEXT_PACKAGE);
#endif

	g_option_context_add_main_entries (context, options, GETTEXT_PACKAGE);

	program = gnome_program_init ("gnome-printer-view", 
				      VERSION,
				      LIBGNOMEUI_MODULE, argc, argv,
				      GNOME_PROGRAM_STANDARD_PROPERTIES,
				      GNOME_PARAM_GOPTION_CONTEXT, context,
				      NULL);

	g_set_application_name (_("Printers View"));

	if (!properties && !view) {
		view = TRUE;
	}
	gnome_cups_ui_init (argv[0]);

	bonobo_activate ();

	listener = get_listener (&is_new);

	if (is_new) {
		check_cups ();
	}

	if (printers) {
		for (i = 0; printers[i] != NULL; i++) {
			if (view) {
				show_printer (listener, printers[i]);
			}
			if (properties) {
				printer_properties (listener, printers[i]);
			}
		}

		g_strfreev (printers);
	} else {
		view_printers (listener);
	}

	if (is_new) {
		gtk_main ();
	}

	return 0;
}
