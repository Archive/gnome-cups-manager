#include <config.h>

#include <cups/cups.h>

#include <stdlib.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-listener.h>
#include <bonobo/bonobo-main.h>

#include <glib/gi18n.h>
#include <glade/glade.h>
#include <gtk/gtk.h>

#include <libgnomeui/gnome-client.h>
#include <libgnomeui/gnome-ui-init.h>

#include "view-queue.h"
#include "printer-properties.h"
#include "tray.h"

#include <libgnomecups/gnome-cups-util.h>
#include <libgnomecups/gnome-cups-ui-init.h>
#include <libgnomecups/gnome-cups-printer.h>
#include <libgnomecups/gnome-cups-request.h>
#include <libgnomecups/gnome-cups-ui-print.h>	

static void
die_cb (GnomeClient *client, gpointer data)
{
	gtk_main_quit ();
}

static void
add_to_session (void)
{
	GnomeClient *client;

	client = gnome_master_client ();
	
	gnome_client_set_priority (client, 50);
	
	gnome_client_set_restart_style (client, GNOME_RESTART_IMMEDIATELY);

	g_signal_connect (client, "die", G_CALLBACK (die_cb), NULL);
}

static void
remove_from_session (void)
{
	GnomeClient *client;

	client = gnome_master_client ();
	
	gnome_client_set_restart_style (client, GNOME_RESTART_NEVER);
}

static gboolean
check_cups_again (gpointer user_data)
{
	static int tries = 0;
	
	if (gnome_cups_check_daemon ()) {
		gnome_cups_manager_start_icon ();
		return FALSE;
	} else {		
		if (++tries == 3) {
			GtkWidget *dialog;
			/* FIXME: dialog text */
			dialog = gtk_message_dialog_new (NULL, 
							 GTK_DIALOG_DESTROY_WITH_PARENT, 
							 GTK_MESSAGE_ERROR,
							 GTK_BUTTONS_OK,
							 _("Could not start the printer tray icon, because the CUPS server could not be contacted."));
			gtk_dialog_run (GTK_DIALOG (dialog));
			gtk_widget_destroy (dialog); 
			
			remove_from_session ();
			
			gtk_main_quit ();
		}
		
	}

	return TRUE;	
}

static void
check_cups (void)
{
	if (gnome_cups_check_daemon ()) {
		gnome_cups_manager_start_icon ();
	} else {
		g_timeout_add (30000, check_cups_again, NULL);
	}
}

static void
event_cb (BonoboListener *listener, const char *event_name, 
	  const BonoboArg *arg, CORBA_Environment *ev,
	  gpointer user_data)
{
}

static gboolean
already_running (void)
{
	BonoboListener *listener;
	Bonobo_RegistrationResult reg_result;

	listener = bonobo_listener_new (event_cb, NULL);

	reg_result = bonobo_activation_register_active_server ("OAFIID:gnome-cups-icon",
		BONOBO_OBJREF (listener), NULL);
	
	if (reg_result == Bonobo_ACTIVATION_REG_SUCCESS) {
		return FALSE;
	} else {
		bonobo_object_unref (listener);
		return TRUE;
	}
}

int 
main (int argc, char *argv[])
{
	gnome_program_init ("gnome-cups-icon", 
			    VERSION,
			    LIBGNOMEUI_MODULE, argc, argv,
			    GNOME_PROGRAM_STANDARD_PROPERTIES,
			    GNOME_PARAM_HUMAN_READABLE_NAME, _("Print Monitor"),
			    NULL);

	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	bonobo_init (&argc, argv);
	bonobo_activate ();
	
	if (!already_running ()) {
		gnome_cups_ui_init (argv[0]);
		
		check_cups ();
		add_to_session ();
		gtk_main ();
	}
	
	return 0;
}
