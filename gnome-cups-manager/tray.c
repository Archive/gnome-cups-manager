#undef GTK_DISABLE_DEPRECATED

#include <config.h>
#include "tray.h"

#include <cups/cups.h>
#include <cups/language.h>
#include <cups/http.h>
#include <cups/ipp.h>

#include "eggtrayicon.h"

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include <libgnomecups/gnome-cups-printer.h>
#include <libgnomecups/gnome-cups-request.h>
#include <libgnomecups/gnome-cups-ui-util.h>

#include "printer-properties.h"
#include "view-queue.h"

static GtkTooltips *tooltips = NULL;
static GHashTable *tray_icons = NULL;
static const char *user_name;

static void
launch_queue (const char *printer_name)
{
	char *argv[] = { "gnome-cups-manager", "-v", NULL, NULL };
        GError *error;
        
        error = NULL;

	argv[2] = (char*)printer_name;

        g_spawn_async (NULL, argv, NULL, 
                       G_SPAWN_SEARCH_PATH, NULL, NULL, NULL, &error);

        if (error) {
		gnome_cups_error_dialog (NULL, _("Could not launch printer view"), 
					 error);
		g_error_free (error);
        }
}

static void 
launch_properties (const char *printer_name)
{
	char *argv[] = { "gnome-cups-manager", "-p", NULL, NULL };
        GError *error;
        
        error = NULL;

	argv[2] = (char*)printer_name;

        g_spawn_async (NULL, argv, NULL, 
                       G_SPAWN_SEARCH_PATH, NULL, NULL, NULL, &error);

        if (error) {
		gnome_cups_error_dialog (NULL, _("Could not launch printer properties"),
					 error);
		g_error_free (error);
        }
}

static void
open_activate_cb (GtkWidget *item, gpointer *user_data)
{
	GnomeCupsPrinter *printer = GNOME_CUPS_PRINTER (user_data);

	launch_queue (gnome_cups_printer_get_name (printer));
}

static void
pause_or_resume_activate_cb (GtkWidget *item, gpointer *user_data)
{
	GnomeCupsPrinter *printer = GNOME_CUPS_PRINTER (user_data);
	GError *error = NULL;
	
	if (gnome_cups_printer_get_state (printer) == IPP_PRINTER_STOPPED) {
		gnome_cups_printer_resume (printer, &error);
		if (error) {
			gnome_cups_error_dialog (NULL, 
						 _("Could not resume printer"), 
						 error);
			g_error_free (error);
		}
	} else {
		gnome_cups_printer_pause (printer, &error);
		if (error) {
			gnome_cups_error_dialog (NULL, 
						 _("Could not resume printer"), 
						 error);
			g_error_free (error);
		}
	}
}

static void
properties_activate_cb (GtkWidget *item, gpointer user_data)
{
	GnomeCupsPrinter *printer = GNOME_CUPS_PRINTER (user_data);
	
	launch_properties (gnome_cups_printer_get_name (printer));
}

static void
popup_printer_menu (GnomeCupsPrinter *printer, GdkEventButton *event)
{
	GtkWidget *menu;
	GtkWidget *item;
	
	menu = gtk_menu_new ();
	
	item = gtk_image_menu_item_new_from_stock (GTK_STOCK_OPEN, NULL);
	g_signal_connect (item, "activate", 
			  G_CALLBACK (open_activate_cb), printer);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	item = gtk_separator_menu_item_new ();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	
	if (gnome_cups_printer_get_state (printer) == IPP_PRINTER_STOPPED) {
		item = gtk_menu_item_new_with_mnemonic (_("_Resume Printer"));
	} else {
		item = gtk_menu_item_new_with_mnemonic (_("P_ause Printer"));
	}
	g_signal_connect (item, "activate", 
			  G_CALLBACK (pause_or_resume_activate_cb), printer);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);	

	item = gtk_image_menu_item_new_from_stock (GTK_STOCK_PROPERTIES, NULL);
	g_signal_connect (item, "activate", 
			  G_CALLBACK (properties_activate_cb), printer);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	
	gtk_widget_show_all (menu);
	gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL, 
			event->button, event->time);
}

static gboolean
button_press_cb (GtkWidget *event_box,
		 GdkEventButton *event,
		 gpointer user_data)
{
	GnomeCupsPrinter *printer = GNOME_CUPS_PRINTER (user_data);	

	if (event->type != GDK_BUTTON_PRESS) {
		return FALSE;
	}

	if (event->button == 1) {
		launch_queue (gnome_cups_printer_get_name (printer));
		return TRUE;
	} else if (event->button == 3) {
		popup_printer_menu (printer, event);
		return TRUE;
	}
	
	return FALSE;
}

static char *
get_icon_path (GnomeCupsPrinter *printer)
{
	if (gnome_cups_printer_get_state (printer) == IPP_PRINTER_STOPPED) {
		return g_strdup (GNOME_CUPS_MANAGER_PIXMAPDIR "/printer-tray-stopped.png");
	} else {
		return g_strdup (GNOME_CUPS_MANAGER_PIXMAPDIR "/printer-tray-normal.png");
	}
}

static void
update_tray_icon (GnomeCupsPrinter *printer)
{
	char *title;
	char *filename;
	EggTrayIcon *tray_icon;
	GtkWidget *image;
	GtkWidget *event_box;
	
	tray_icon = g_hash_table_lookup (tray_icons, printer);
	g_return_if_fail (tray_icon != NULL);

	image = g_object_get_data (G_OBJECT (tray_icon), "image");
	event_box = g_object_get_data (G_OBJECT (tray_icon), "event-box");

	filename = get_icon_path (printer);
	gtk_image_set_from_file (GTK_IMAGE (image), filename);
	g_free (filename);

	title = g_strdup_printf (_("%s - %s"),
				 gnome_cups_printer_get_name (printer),
				 gnome_cups_printer_get_state_name (printer));
	gtk_tooltips_set_tip (tooltips, event_box, title, NULL);
	g_free (title);
}

static void
set_tray_icon (GnomeCupsPrinter *printer)
{
	EggTrayIcon *tray_icon;
	GtkWidget *image;
	GtkWidget *event_box;
	
	tray_icon = g_hash_table_lookup (tray_icons, printer);
	
	if (tray_icon) {
		update_tray_icon (printer);
	} else {
		tray_icon = egg_tray_icon_new (gnome_cups_printer_get_name (printer));
		
		/* FIXME */
		event_box = gtk_event_box_new ();
		image = gtk_image_new ();
		
		gtk_container_add (GTK_CONTAINER (event_box), image);
		gtk_container_add (GTK_CONTAINER (tray_icon), event_box);

		g_object_set_data (G_OBJECT (tray_icon), "printer", printer);
		g_object_set_data (G_OBJECT (tray_icon), "image", image);
		g_object_set_data (G_OBJECT (tray_icon), "event-box", event_box);
		g_hash_table_insert (tray_icons, printer, tray_icon);

		
		g_signal_connect (G_OBJECT (event_box), 
				  "button_press_event",
				  G_CALLBACK (button_press_cb),
				  printer);		

		update_tray_icon (printer);
		
		gtk_widget_show_all (GTK_WIDGET (tray_icon));
		
	}
}

static void
remove_tray_icon (GnomeCupsPrinter *printer)
{
	g_hash_table_remove (tray_icons, printer);
}

static void
jobs_changed_cb (GnomeCupsQueue *queue, 
		    gpointer *user_data)
{
	GnomeCupsPrinter *printer;
	const GList *jobs;
	const GList *l;

	printer = gnome_cups_printer_get(
			gnome_cups_queue_get_name (queue));

	jobs = gnome_cups_queue_get_jobs (queue);
	for (l = jobs; l != NULL; l = l->next) {
		GnomeCupsJob *job = l->data;
		if ((job->owner) && (strcmp (job->owner, user_name) == 0)) {
			set_tray_icon (printer);
			return;
		}
	}

	remove_tray_icon (printer);
}

static void
printer_gone_cb (GnomeCupsPrinter *printer, gpointer user_data)
{
	remove_tray_icon (printer);
	g_object_unref (printer);
}

static void
watch_printer (const char *printer_name)
{
	GnomeCupsPrinter *printer;
	GnomeCupsQueue *queue;
	
	printer = gnome_cups_printer_get (printer_name);
	queue = gnome_cups_printer_get_queue (printer);
	
	g_signal_connect (printer, "gone", 
			  G_CALLBACK (printer_gone_cb), 
			  NULL);
	g_signal_connect (queue, "jobs_removed", 
			  G_CALLBACK (jobs_changed_cb), 
			  NULL);
	g_signal_connect (queue, "jobs_added", 
			  G_CALLBACK (jobs_changed_cb), 
			  NULL);
	g_signal_connect (queue, "jobs_changed", 
			  G_CALLBACK (jobs_changed_cb), 
			  NULL);
}

static void
printer_add_cb (const char *printer_name, gpointer user_data)
{
	watch_printer (printer_name);
}

void
gnome_cups_manager_start_icon (void)
{
	GList *printers;
	GList *l;
	
	tooltips = gtk_tooltips_new ();
	tray_icons = g_hash_table_new_full (g_direct_hash,
					    g_direct_equal,
					    NULL,
					    (GDestroyNotify)gtk_widget_destroy);
	user_name = g_get_user_name ();

	/* Watch the intial printers */
	printers = gnome_cups_get_printers ();
	for (l = printers; l != NULL; l = l->next) {
		watch_printer (l->data);
	}

	gnome_cups_printer_new_printer_notify_add (printer_add_cb, 
						   NULL);

	gnome_cups_printer_list_free (printers);
}
