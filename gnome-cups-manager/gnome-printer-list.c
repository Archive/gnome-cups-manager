/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/* 
 * gnome-printer-list.c
 * Copyright (C) 2003-2004, Ximian, Inc.
 *
 * Authors:
 *   Dave Camp <dave@ximian.com>
 *   Jody Goldberg <jody@novell.com>
 *
 * This file is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU Library General Public
 * License as published by the Free Software Foundation.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this file; if not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 **/

#include <config.h>

#include "gnome-printer-list.h"

#include "printer-properties.h"
#include "view-queue.h"

#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include <libgnomecups/gnome-cups-printer.h>
#include <libgnomecups/gnome-cups-ui-util.h>
#include <libgnomecups/gnome-cups-permission.h>

struct _GnomePrinterListDetails {
	EggIconListItem *add_item;

	GList *printers;
	
	guint new_printer_notify;
};

/* Tool to help debug lifecycle */
#if 0
#define d(code)	do { code } while (0)
#else
#define d(code)
#endif

static void disconnect_printer (GnomePrinterList *list, GnomeCupsPrinter *printer);

G_DEFINE_TYPE (GnomePrinterList, gnome_printer_list, egg_icon_list_get_type ())

static GdkPixbuf *
load_icon (GtkWidget *w, char const *icon_name)
{
	GError *err = NULL;
	GdkPixbuf *res = gtk_icon_theme_load_icon (gtk_icon_theme_get_default (),
		icon_name, 48, 0, NULL);

	if (res == NULL) {
		if (err != NULL) {
			g_warning ("%s", err->message);
			g_error_free (err);
			err = NULL;
		}
		res = gtk_widget_render_icon (w, icon_name, GTK_ICON_SIZE_DIALOG, NULL);

		if (err != NULL) {
			g_warning ("%s", err->message);
			g_error_free (err);
			return NULL;
		}
	}

	return res;
}

static void
add_new_printer_icon (GnomePrinterList *list)
{
	EggIconListItem *item;
	GdkPixbuf *pixbuf = load_icon (GTK_WIDGET (list), "gnome-dev-printer-new");
	if (pixbuf == NULL)
		pixbuf = load_icon (GTK_WIDGET (list), GTK_STOCK_PRINT);

	item = egg_icon_list_item_new (pixbuf, _("New Printer"));
	list->details->add_item = item;

	g_object_unref (pixbuf);

	egg_icon_list_append_item (EGG_ICON_LIST (list), item);
}

static GdkPixbuf *
get_printer_icon (GtkWidget *list, GnomeCupsPrinter *printer)
{
	char *icon_name;
	GList *ptr, *emblems;
	GdkPixbuf *pixbuf, *emblem;
	GError *err = NULL;

	gnome_cups_printer_get_icon (printer,
				     &icon_name,
				     &emblems);
	pixbuf = load_icon (GTK_WIDGET (list), icon_name);
	g_free (icon_name);
	if (pixbuf == NULL)
		pixbuf = load_icon (GTK_WIDGET (list), GTK_STOCK_PRINT);

	for (ptr = emblems; ptr != NULL; ptr = ptr->next) {
		if (ptr->data == NULL)
			continue;
		emblem = gtk_icon_theme_load_icon (gtk_icon_theme_get_default (),
			ptr->data, 48, 0, &err);
		if (emblem == NULL) {
			g_warning ("unable to load emblem '%s'", (char *)ptr->data);
			if (err != NULL) {
				g_warning ("%s", err->message);
				g_error_free (err);
				err = NULL;
			}
		} else {
			GdkPixbuf *res = gdk_pixbuf_copy (pixbuf);
			gdk_pixbuf_composite (emblem,  res, 0, 0, 
				gdk_pixbuf_get_width (res), gdk_pixbuf_get_height (res), 
				0., 0., 1., 1., GDK_INTERP_NEAREST, 255);
			g_object_unref (pixbuf);
			pixbuf = res;
		}
	}

	return pixbuf;
}

static char *
get_printer_label (GnomeCupsPrinter *printer)
{
	char const *name  = gnome_cups_printer_get_name (printer);
	char const *state = gnome_cups_printer_get_state_name (printer);
	int jobs = gnome_cups_printer_get_job_count (printer);

	/* FIXME: hard-coded additional text color */
	if (jobs > 0) {
		return g_strdup_printf ("%s\n<span color=\"#337F33\">%s\n%d Jobs</span>",
			name, state, jobs);
	} else {
		return g_strdup_printf ("%s\n<span color=\"#33337F\">%s\n</span>",
			name, state);
	}
}

static EggIconListItem *
create_printer_item (GtkWidget *list, GnomeCupsPrinter *printer)
{
	EggIconListItem *item;
	GdkPixbuf *pixbuf;
	char *label;
	
	pixbuf = get_printer_icon (list, printer);
	label = get_printer_label (printer);
	
	item = egg_icon_list_item_new (pixbuf, label);

	egg_icon_list_item_set_data (item, printer);

	g_object_unref (pixbuf);
	g_free (label);

	return item;
}

static void
printer_activated_cb (GnomePrinterList *list,
		      GnomeCupsPrinter *printer)
{	
	gnome_cups_manager_view_queue (printer);
}

static void
item_activated_cb (GnomePrinterList *list,
		   EggIconListItem *item,
		   gpointer user_data)
{
	gnome_printer_list_selection_activate (list);
}

static void
open_activated_cb (GtkWidget *item,
		   gpointer user_data)
{
	gnome_printer_list_selection_activate (GNOME_PRINTER_LIST (user_data));
}

static void
pause_activated_cb (GtkWidget *item,
		    gpointer user_data)
{
	gnome_printer_list_selection_pause (GNOME_PRINTER_LIST (user_data));
}

static void
resume_activated_cb (GtkWidget *item,
		     gpointer user_data)
{
	gnome_printer_list_selection_resume (GNOME_PRINTER_LIST (user_data));
}

static void
make_default_activated_cb (GtkWidget *menu_item,
			   gpointer user_data)
{
	gnome_printer_list_selection_make_default (GNOME_PRINTER_LIST (user_data));
}

static void
delete_activated_cb (GtkWidget *menu_item,
		     gpointer user_data)
{
	gnome_printer_list_selection_delete (GNOME_PRINTER_LIST (user_data));
}

static void
properties_activated_cb (GtkWidget *menu_item,
			 gpointer user_data)
{
	gnome_printer_list_selection_properties (GNOME_PRINTER_LIST (user_data));
}

/* This is so lame */
void
gnome_printer_list_get_command_sensitivities (GnomePrinterList *list,
					      gboolean *activate,
					      gboolean *pause,
					      gboolean *resume,
					      gboolean *make_default,
					      gboolean *del,
					      gboolean *properties,
					      gboolean *status)
{
	GList *l;
	GList *selection;
	int num_selected;
	gboolean new_printer_selected;
	gboolean all_paused;
	gboolean has_default;
	gboolean all_running;

	selection = egg_icon_list_get_selected (EGG_ICON_LIST (list));
	
	num_selected = g_list_length (selection);

	if (num_selected == 0) {
		g_list_free (selection);
		if (activate) {
			*activate = FALSE;
		}
		if (pause) {
			*pause = FALSE;
		}
		if (resume) {
			*resume = FALSE;
		}
		if (make_default) {
			*make_default = FALSE;
		}
		if (del) {
			*del = FALSE;
		}
		if (properties) {
			*properties = FALSE;
		}
		if (status) {
			*status = FALSE;
		}
		return;
	}
	
	new_printer_selected = FALSE;
	all_paused = TRUE;
	all_running = TRUE;
	has_default = FALSE;

	for (l = selection; l != NULL; l = l->next) {
		EggIconListItem *item;
		
		item = l->data;
		
		if (item == list->details->add_item) {
			new_printer_selected = TRUE;
		} else {
			GnomeCupsPrinter *printer;

			printer = GNOME_CUPS_PRINTER (egg_icon_list_item_get_data (item));
			if (gnome_cups_printer_get_state (printer) == IPP_PRINTER_STOPPED) {
				all_running = FALSE;
			} else {
				all_paused = FALSE;
			}

			if (gnome_cups_printer_get_is_default (printer)) {
				has_default = TRUE;
			}
		}
	}
	
	if (activate) {
		*activate = TRUE;
	}
	
	if (pause) {
		*pause = !new_printer_selected && !all_paused && gnome_cups_can_admin ();
	}
	
	if (resume) {
		*resume = !new_printer_selected && !all_running && gnome_cups_can_admin ();
	}
	
	if (make_default) {
		*make_default = !new_printer_selected && num_selected == 1 && !has_default;
	}
	
	if (del) {
		*del = !new_printer_selected && gnome_cups_can_admin ();
	}
	
	if (properties) {
		*properties = !new_printer_selected;
	}

	if (status) {
		*status = !new_printer_selected;
	}

	g_list_free (selection);
}

static GtkWidget *
create_selection_menu (GnomePrinterList *list)
{
	GtkWidget *menu;
	GtkWidget *item;
	GList *selection;
	gboolean activate;
	gboolean pause;
	gboolean resume;
	gboolean make_default;
	gboolean del;
	gboolean properties;
	gboolean status;

	gnome_printer_list_get_command_sensitivities (list,
		&activate, &pause, &resume, &make_default,
		&del, &properties, &status);
	
	selection = egg_icon_list_get_selected (EGG_ICON_LIST (list));

	menu = gtk_menu_new ();
	gtk_widget_show (menu);

	if (g_list_length (selection) == 1 && selection->data == list->details->add_item) {
		g_list_free (selection);

		item = gtk_image_menu_item_new_from_stock (GTK_STOCK_ADD, NULL);
		gtk_widget_show (item);
		g_signal_connect (item, "activate", 
				  G_CALLBACK (open_activated_cb),
				  list);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);

		return menu;
	}
	g_list_free (selection);

	item = gtk_menu_item_new_with_label (_("Jobs"));
	gtk_widget_show (item);
	g_signal_connect (item, "activate", 
			  G_CALLBACK (open_activated_cb),
			  list);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);

	item = gtk_separator_menu_item_new ();
	gtk_widget_show (item);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);	

	item = gtk_menu_item_new_with_label (_("Pause"));
	gtk_widget_show (item);
	gtk_widget_set_sensitive (item, pause);
	g_signal_connect (item, "activate", 
			  G_CALLBACK (pause_activated_cb),
			  list);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);

	item = gtk_menu_item_new_with_label (_("Resume"));
	gtk_widget_show (item);
	gtk_widget_set_sensitive (item, resume);
	g_signal_connect (item, "activate", 
			  G_CALLBACK (resume_activated_cb),
			  list);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);	

	item = gtk_menu_item_new_with_label (_("Make Default"));
	gtk_widget_show (item);
	gtk_widget_set_sensitive (item, make_default);
	g_signal_connect (item, "activate", 
			  G_CALLBACK (make_default_activated_cb),
			  list);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);	

	item = gtk_separator_menu_item_new ();
	gtk_widget_show (item);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);	

	item = gtk_image_menu_item_new_from_stock (GTK_STOCK_REMOVE, NULL);
	gtk_widget_show (item);
	gtk_widget_set_sensitive (item, del);
	g_signal_connect (item, "activate", 
			  G_CALLBACK (delete_activated_cb),
			  list);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);

	item = gtk_separator_menu_item_new ();
	gtk_widget_show (item);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);	

	item = gtk_image_menu_item_new_from_stock (GTK_STOCK_PROPERTIES, NULL);
	gtk_widget_show (item);
	gtk_widget_set_sensitive (item, properties);
	g_signal_connect (item, "activate", 
			  G_CALLBACK (properties_activated_cb),
			  list);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	
	return menu;
}

static void
context_menu_popup (GnomePrinterList *list,
		    GdkEventButton *event)
{
	GtkWidget *menu;
	
	menu = create_selection_menu (list);

	gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL, event->button, event->time);
}

static gboolean
button_press_event_cb (GnomePrinterList *list,
		       GdkEventButton *event,
		       gpointer user_data)
{
	if (event->button == 3) {
		EggIconListItem *item;
		
		item = egg_icon_list_get_item_at_pos (EGG_ICON_LIST (list), 
						      event->x, event->y);
		if (!item) {
			return FALSE;
		}

		if (!egg_icon_list_item_is_selected (item)) {
			egg_icon_list_unselect_all (EGG_ICON_LIST (list));
			egg_icon_list_select_item (EGG_ICON_LIST (list),
						   item);
		}
		
		context_menu_popup (list, event);
		
		return TRUE;
	}

	return FALSE;
}

static void
printer_changed_cb (GnomeCupsPrinter *printer,
		    GnomePrinterList *list)
{
	EggIconListItem *item;
	GdkPixbuf *pixbuf;
	char *label;

	d (g_warning ("changed %s", gnome_cups_printer_get_name (printer)););

	item = g_object_get_data (G_OBJECT (printer), "icon_item");
	
	pixbuf = get_printer_icon (GTK_WIDGET (list), printer);
	label = get_printer_label (printer);
	
	egg_icon_list_item_set_icon (item, pixbuf);
	egg_icon_list_item_set_label (item, label);

	g_signal_emit_by_name (list, "selection_changed");

	g_object_unref (pixbuf);
	g_free (label);
}

static void
printer_gone_cb (GnomeCupsPrinter *printer,
		 gpointer user_data)
{
	GnomePrinterList *list;
	EggIconListItem *item;

	d (g_warning ("removed %s", gnome_cups_printer_get_name (printer)););

	list = GNOME_PRINTER_LIST (user_data);
	item = g_object_get_data (G_OBJECT (printer), "icon_item");

	egg_icon_list_remove_item (EGG_ICON_LIST (list), item);
	
	list->details->printers = g_list_remove (list->details->printers, printer);

	disconnect_printer (list, printer);
}

static void
disconnect_printer (GnomePrinterList *list,
                    GnomeCupsPrinter *printer)
{
        g_signal_handlers_disconnect_by_func (printer,
                                              G_CALLBACK (printer_changed_cb),
                                              printer);
        g_signal_handlers_disconnect_by_func (printer,
                                              G_CALLBACK (printer_gone_cb),
                                              printer);
        g_object_unref (printer);
}

static void
watch_printer (GnomePrinterList *list,
	       const char *printer_name)
{
	EggIconListItem *item;
	GnomeCupsPrinter *printer = gnome_cups_printer_get (printer_name);

	if (!printer)
		return;

	list->details->printers = g_list_append (list->details->printers, 
						 printer);
	g_object_ref (printer);
	
	item = create_printer_item (GTK_WIDGET (list), printer);
	g_object_set_data_full (G_OBJECT (printer), "icon_item", item,
		(GDestroyNotify)egg_icon_list_item_unref);

	egg_icon_list_append_item (EGG_ICON_LIST (list), item);

	g_signal_connect_object (printer, "is_default_changed",
				 G_CALLBACK (printer_changed_cb),
				 list, 0);
	g_signal_connect_object (printer, "attributes_changed",
				 G_CALLBACK (printer_changed_cb),
				 list, 0);
	g_signal_connect_object (printer, "gone",
				 G_CALLBACK (printer_gone_cb),
				 list, 0);
	printer_changed_cb (printer, list);
}

static void
printer_added_cb (const char *name, gpointer user_data)
{
	GnomePrinterList *list;
	
	list = GNOME_PRINTER_LIST (user_data);
	
	d (g_warning ("added %s", name););
	watch_printer (list, name);
}

static int
icon_compare (EggIconList *icon_list,
	      EggIconListItem *a,
	      EggIconListItem *b,
	      gpointer user_data)
{
	GnomePrinterList *list = GNOME_PRINTER_LIST (icon_list);
	GnomeCupsPrinter *printer_a;
	GnomeCupsPrinter *printer_b;
	
	if (a == b) {
		return 0;
	}

	if (a == list->details->add_item) {
		return -1;
	}
	
	if (b == list->details->add_item) {
		return 1;
	}

	printer_a = egg_icon_list_item_get_data (a);
	printer_b = egg_icon_list_item_get_data (b);

	return strcmp (gnome_cups_printer_get_name (printer_a),
		       gnome_cups_printer_get_name (printer_b));
}

void
gnome_printer_list_new_printer (GnomePrinterList *list)
{
	GtkWidget *toplevel = NULL;
	if (list != NULL)
		toplevel = gtk_widget_get_toplevel (GTK_WIDGET (list));
	gnome_cups_spawn ("gnome-cups-add", 0, NULL, TRUE, toplevel);
}

void
gnome_printer_list_selection_activate (GnomePrinterList *list)
{
	GList *selection;
	GList *l;
	
	selection = egg_icon_list_get_selected (EGG_ICON_LIST (list));

	for (l = selection; l != NULL; l = l->next) {
		EggIconListItem *item = l->data;

		if (item == list->details->add_item) {
			gnome_printer_list_new_printer (list);
		} else {
			GnomeCupsPrinter *printer;
			
			printer = egg_icon_list_item_get_data (item);
			printer_activated_cb (list, printer);
		}
	}

	g_list_free (selection);
}

void
gnome_printer_list_selection_pause (GnomePrinterList *list)
{
	GList *selection;
	GList *l;
	
	selection = egg_icon_list_get_selected (EGG_ICON_LIST (list));

	for (l = selection; l != NULL; l = l->next) {
		EggIconListItem *item = l->data;

		if (item != list->details->add_item) {
			GnomeCupsPrinter *printer;
			GError *error = NULL;
			
			printer = egg_icon_list_item_get_data (item);

			gnome_cups_printer_pause (printer, &error);
			if (error) {
				char *msg;
				msg = g_strdup_printf ("Could not pause '%s'", 
						       gnome_cups_printer_get_name (printer));
				gnome_cups_error_dialog (GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (list))), msg, error);
				g_free (msg);
				g_error_free (error);
			}
		}
	}

	g_list_free (selection);
}

void
gnome_printer_list_selection_resume (GnomePrinterList *list)
{
	GList *selection;
	GList *l;
	
	selection = egg_icon_list_get_selected (EGG_ICON_LIST (list));

	for (l = selection; l != NULL; l = l->next) {
		EggIconListItem *item = l->data;

		if (item != list->details->add_item) {
			GnomeCupsPrinter *printer;
			GError *error = NULL;
			
			printer = egg_icon_list_item_get_data (item);

			gnome_cups_printer_resume (printer, &error);
			if (error) {
				char *msg;
				msg = g_strdup_printf ("Could not resume '%s'", 
						       gnome_cups_printer_get_name (printer));
				gnome_cups_error_dialog (GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (list))), msg, error);
				g_free (msg);
				g_error_free (error);
			}
		}
	}

	g_list_free (selection);
}

void
gnome_printer_list_selection_make_default (GnomePrinterList *list)
{
	
	GList *selection;
	GnomeCupsPrinter *printer;
	EggIconListItem *item;
	GError *error = NULL;

	selection = egg_icon_list_get_selected (EGG_ICON_LIST (list));
        g_return_if_fail (g_list_length (selection) == 1);

	item = selection->data;
        g_return_if_fail (item != list->details->add_item);

	printer = GNOME_CUPS_PRINTER (egg_icon_list_item_get_data (item));
        gnome_cups_printer_set_default (printer, &error);
        if (error) {
                char *msg;
                msg = g_strdup_printf ("Could not make '%s' the default printer", 
                                       gnome_cups_printer_get_name (printer));
				gnome_cups_error_dialog (GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (list))), msg, error);
                g_free (msg);
                g_error_free (error);
        }

	g_list_free (selection);
}

void
gnome_printer_list_selection_properties (GnomePrinterList *list)
{
	GList *selection;
	GList *l;
	
	selection = egg_icon_list_get_selected (EGG_ICON_LIST (list));

	for (l = selection; l != NULL; l = l->next) {
		EggIconListItem *item = l->data;

		if (item != list->details->add_item) {
			GnomeCupsPrinter *printer;
			
			printer = egg_icon_list_item_get_data (item);

			gnome_cups_manager_printer_properties (printer);
		}
	}

	g_list_free (selection);
}

void
gnome_printer_list_selection_delete (GnomePrinterList *list)
{
	GList *selection;
	GList *l;

	selection = egg_icon_list_get_selected (EGG_ICON_LIST (list));

	for (l = selection; l != NULL; l = l->next) {
		EggIconListItem *item = l->data;

		if (item != list->details->add_item) {
			GnomeCupsPrinter *printer;
			GError *error = NULL;
			
			printer = egg_icon_list_item_get_data (item);

			gnome_cups_printer_delete (printer, &error);
			if (error) {
				char *msg;
				msg = g_strdup_printf ("Could not delete '%s'", 
						       gnome_cups_printer_get_name (printer));
				gnome_cups_error_dialog (GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (list))), msg, error);
				g_free (msg);
				g_error_free (error);
			}
		}
	}

	g_list_free (selection);
}


static void
gnome_printer_list_init (GnomePrinterList *list)
{
	GList *printer_names;
	GList *l;
	
	list->details = g_new0 (GnomePrinterListDetails, 1);


	egg_icon_list_set_sort_func (EGG_ICON_LIST (list),
				     icon_compare,
				     list,
				     NULL);
	egg_icon_list_set_sorted (EGG_ICON_LIST (list), TRUE);

	add_new_printer_icon (list);
	
	printer_names = gnome_cups_get_printers ();
	for (l = printer_names; l != NULL; l = l->next) {
		watch_printer (list, l->data);
	}
	gnome_cups_printer_list_free (printer_names);

	list->details->new_printer_notify = 
		gnome_cups_printer_new_printer_notify_add (printer_added_cb,
							   list);

	egg_icon_list_set_selection_mode (EGG_ICON_LIST (list),
					  GTK_SELECTION_MULTIPLE);

	g_signal_connect (list, "item_activated", 
			  G_CALLBACK (item_activated_cb), NULL);

	g_signal_connect (list, "button_press_event",
			  G_CALLBACK (button_press_event_cb), NULL);
}

static void
gnome_printer_list_destroy (GtkObject *object)
{
	GnomePrinterList *list;
	GList *l;

	list = GNOME_PRINTER_LIST (object);

	for (l = list->details->printers; l != NULL; l = l->next) {
		disconnect_printer (list, GNOME_CUPS_PRINTER (l->data));
	}

	g_list_free (list->details->printers);
	list->details->printers = NULL;
	
	if (list->details->new_printer_notify) {
		gnome_cups_printer_new_printer_notify_remove (list->details->new_printer_notify);
		list->details->new_printer_notify = 0;
	}
	
        GTK_OBJECT_CLASS (gnome_printer_list_parent_class)-> destroy, (object);
}

static void
gnome_printer_list_finalize (GObject *object)
{
	GnomePrinterList *list = GNOME_PRINTER_LIST (object);

	g_free (list->details);
}

static void
gnome_printer_list_class_init (GnomePrinterListClass *class)
{
	GTK_OBJECT_CLASS (class)->destroy = gnome_printer_list_destroy;
	G_OBJECT_CLASS (class)->finalize = gnome_printer_list_finalize;
}

GtkWidget *
gnome_printer_list_new (void)
{
	return GTK_WIDGET (g_object_new (gnome_printer_list_get_type (), NULL));
	
}
