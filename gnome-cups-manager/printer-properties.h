#ifndef CONFIGURE_PRINTER_H
#define CONFIGURE_PRINTER_H

#include <libgnomecups/gnome-cups-printer.h>

void gnome_cups_manager_printer_properties (GnomeCupsPrinter *printer);

#endif
