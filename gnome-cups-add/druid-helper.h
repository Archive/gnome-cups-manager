#ifndef DRUID_HELPER_H
#define DRUID_HELPER_H

#include <glade/glade-xml.h>

typedef void (*DruidSetupFunc) (GladeXML *xml);
typedef void (*DruidPrepareFunc) (GladeXML *xml);
typedef void (*DruidSensitivityFunc) (GladeXML *xml, gboolean *back, gboolean *next);
typedef GtkWidget *(*DruidNavigationFunc) (GladeXML *xml);
typedef void (*DruidApplyFunc) (GladeXML *xml);

typedef struct
{
	const char *name;
	DruidSetupFunc setup;
	DruidPrepareFunc prepare;
	DruidSensitivityFunc sensitivity;
	DruidNavigationFunc back;
	DruidNavigationFunc next;
	GladeXML *xml;
	const char *druid_name;
} DruidPageDescription;

void druid_pages_setup (GladeXML *xml, DruidPageDescription *descriptions);
void druid_update_sensitivities (GladeXML *xml);
void druid_watch_sensitivity_widget (GladeXML *xml, const char *name);

/* Some useful predicates */
gboolean tree_view_has_selection (GladeXML   *xml,
				  const char *name);

#endif
