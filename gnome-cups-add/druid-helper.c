/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#include <config.h>

#include <string.h>
#include <libgnomecups/gnome-cups-ui-driver.h>

#include <glib.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <libgnome/gnome-i18n.h>
#include <libgnomeui/gnome-druid.h>

#include "druid-helper.h"

static DruidPageDescription *
get_page_description (GladeXML *xml, const char *name)
{
	DruidPageDescription *pages;
	
	for (pages = g_object_get_data (G_OBJECT (xml), "pages");
	     pages && pages->name != NULL;
	     pages++) {
		if (!strcmp (pages->name, name)) {
			return pages;
		}
	}

	return NULL;
}

static void
druid_page_prepare_cb (GnomeDruidPage *page,
		       GtkWidget *druid,
		       gpointer user_data) 
{
	DruidPageDescription *desc = user_data;
	
	/* This is dumb. */
	g_object_set_data (G_OBJECT (desc->xml), "current_page", page);

	if (desc->prepare) {
		desc->prepare (desc->xml);
	}
	
	druid_update_sensitivities (desc->xml);
}

static gboolean 
druid_page_back_cb (GnomeDruidPage *page,
		    GtkWidget *druid,
		    gpointer user_data)
{
	DruidPageDescription *desc = user_data;
	GtkWidget *new_page;
	
	g_return_val_if_fail (desc->back != NULL, TRUE);
	
	new_page = desc->back (desc->xml);
	if (new_page) {
		gnome_druid_set_page (GNOME_DRUID (druid), 
				      GNOME_DRUID_PAGE (new_page));
	}
	
	return TRUE;
}

static gboolean 
druid_page_next_cb (GnomeDruidPage *page,
		    GtkWidget *druid,
		    gpointer user_data)
{
	GtkWidget *new_page;
	DruidPageDescription *desc = user_data;
	
	g_return_val_if_fail (desc->next != NULL, TRUE);
	
	new_page = desc->next (desc->xml);
	if (new_page) {
		gnome_druid_set_page (GNOME_DRUID (druid), 
				      GNOME_DRUID_PAGE (new_page));
	}
	
	return TRUE;
}

void
druid_pages_setup (GladeXML *xml, DruidPageDescription *descriptions)
{
	DruidPageDescription *p;

	g_object_set_data (G_OBJECT (xml), "pages", descriptions);
	
	for (p = descriptions; p->name != NULL; p++) {
		GtkWidget *page;

		page = glade_xml_get_widget (xml, p->name);
		if (!page) {
			g_warning ("unknown page %s\n", p->name);
			continue;
		}

		p->xml = xml;
		
		if (p->setup) 
			p->setup (xml);

		g_signal_connect_after (page, "prepare", 
					G_CALLBACK (druid_page_prepare_cb),
					p);

		/* initialize the first page */
		if (p == descriptions)
			druid_page_prepare_cb (GNOME_DRUID_PAGE (page), NULL, p);

		if (p->back) {
			g_signal_connect (page, "back", 
					  G_CALLBACK (druid_page_back_cb),
					  p);
		}
		if (p->next) {
			g_signal_connect (page, "next", 
					  G_CALLBACK (druid_page_next_cb),
					  p);
			g_signal_connect (page, "finish", 
					  G_CALLBACK (druid_page_next_cb),
					  p);
		}
	}
}

void
druid_update_sensitivities (GladeXML *xml)
{
	GtkWidget *current_page;
	GtkWidget *druid;
	DruidPageDescription *desc;
	const char *name;
	gboolean back_sensitivity = TRUE;
	gboolean next_sensitivity = TRUE;
	
	/* FIXME: not generic */
	druid = glade_xml_get_widget (xml, "add_printer_druid");

	current_page = GTK_WIDGET (g_object_get_data (G_OBJECT (xml),
						      "current_page"));
	
	name = glade_get_widget_name (current_page);
	desc = get_page_description (xml, name);
	if (desc && desc->sensitivity)
		desc->sensitivity (xml, &back_sensitivity, &next_sensitivity);
	gnome_druid_set_buttons_sensitive (GNOME_DRUID (druid),
					   back_sensitivity, next_sensitivity, 
					   TRUE, TRUE);
}

gboolean 
tree_view_has_selection (GladeXML *xml, const char *name)
{
	GtkTreeView *tree_view;
	GtkTreeSelection *selection;
	
	tree_view = GTK_TREE_VIEW (glade_xml_get_widget (xml, name));
	selection = gtk_tree_view_get_selection (tree_view);
	
	return gtk_tree_selection_get_selected (selection, NULL, NULL);
}
