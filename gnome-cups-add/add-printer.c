/* vim: set sw=8: -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#include <config.h>

#include <cups/cups.h>
#include <cups/http.h>
#include <cups/ipp.h>
#include <unistd.h>
#include <sys/types.h>

#include <glib.h>
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <glib/gi18n.h>
#include <libgnomeui/gnome-druid.h>
#include <libgnomeui/gnome-ui-init.h>

#include <libgnomecups/gnome-cups-ui-init.h>
#include <libgnomecups/gnome-cups-request.h>
#include <libgnomecups/gnome-cups-util.h>
#include <libgnomecups/gnome-cups-ui-util.h>
#include <libgnomecups/gnome-cups-ui-driver.h>
#include <libgnomecups/gnome-cups-ui-connection.h>
#include <libgnomecups/gnome-cups-permission.h>
#include <libgnomecups/gnome-cups-printer.h>

#include "druid-helper.h"

static GCupsDriverSelector *
driver_selector (GladeXML *xml)
{
	return (GCupsDriverSelector *) glade_xml_get_widget (xml, "driver_selector");
}
static GCupsConnectionSelector *
connection_selector (GladeXML *xml)
{
	return (GCupsConnectionSelector *) glade_xml_get_widget (xml, "connection_selector");
}

static void
set_selected_uri (GladeXML *xml, char const *uri)
{
	g_object_set_data_full (G_OBJECT (xml), "selected_uri", g_strdup (uri), g_free);
}

static char *
get_selected_uri (GladeXML *xml)
{
	char *str = g_object_get_data (G_OBJECT (xml), "selected_uri");
	
	g_return_val_if_fail (str != NULL, NULL);

	return g_strdup (str);
}

static void
cb_model_guess (G_GNUC_UNUSED GCupsConnectionSelector *cs,
		char const *model, GCupsDriverSelector *ds)
{
	gcups_driver_selector_set_nickname (ds, model);
}

static void
connection_page_setup (GladeXML *xml)
{
	GtkWidget *w = glade_xml_get_widget (xml, "connection_selector");
	
	g_signal_connect_swapped (w, "changed", G_CALLBACK (druid_update_sensitivities), xml);
	g_signal_connect_object (w, "model-guess", G_CALLBACK (cb_model_guess), driver_selector (xml), 0);
}

static void
connection_page_sensitivity (GladeXML *xml, gboolean *back, gboolean *next)
{
	char *uri = gcups_connection_selector_get_uri (connection_selector (xml));
	
	*next = (uri != NULL);
	g_free (uri);
	
	*back = FALSE;
}

static GtkWidget *
connection_page_next (GladeXML *xml)
{
	char *uri = gcups_connection_selector_get_uri (connection_selector (xml));
	GtkWidget *res = NULL;

	if (uri != NULL) {
		gcups_connection_selector_queue_guess (connection_selector (xml));
		res = glade_xml_get_widget (xml, "driver_page");
	}

	set_selected_uri (xml, uri);
	g_free (uri);
	return res;
}

/****************************************************************************/

static void
driver_page_setup (GladeXML *xml)
{
	GtkWidget *ds;
	
	ds = glade_xml_get_widget (xml, "driver_selector");
	g_signal_connect_swapped (ds, "changed", G_CALLBACK (druid_update_sensitivities), xml);
}

static void
driver_page_sensitivity (GladeXML *xml, gboolean *back, gboolean *next)
{
	GtkWidget *druid = glade_xml_get_widget (xml, "add_printer_druid");
	
	*back = TRUE;
	*next = NULL != gcups_driver_selector_get (driver_selector (xml));
	
	/* see driver_page_prepare for an explanation of the druid hack */
	gnome_druid_set_show_finish (GNOME_DRUID (druid), *next);
}

static GtkWidget *
driver_page_back (GladeXML *xml)
{
	return glade_xml_get_widget (xml, "connection_page");
}

static gboolean
add_cups_printer (GladeXML *xml, char const *device_uri, GCupsPPD  const *ppd, char const *printer_name)
{
	GError *err = NULL;
	char local_uri [HTTP_MAX_URI+1];
	ipp_t *request = gnome_cups_request_new (CUPS_ADD_PRINTER);
	
	g_snprintf (local_uri, sizeof local_uri - 1,
		    "ipp://localhost/printers/%s", printer_name);
	ippAddString (request, IPP_TAG_OPERATION, IPP_TAG_URI,
		      "printer-uri", NULL, local_uri);
	ippAddString (request,	IPP_TAG_PRINTER, IPP_TAG_NAME,
		      "printer-name", NULL, gnome_cups_strdup (printer_name));
	ippAddString (request, IPP_TAG_PRINTER, IPP_TAG_NAME,
		      "ppd-name", NULL, gnome_cups_strdup (ppd->filename));
	ippAddString (request, IPP_TAG_PRINTER, IPP_TAG_URI,
		      "device-uri", NULL, gnome_cups_strdup (device_uri));
	ippAddBoolean (request, IPP_TAG_PRINTER, "printer-is-accepting-jobs", 1);
	ippAddInteger(request, IPP_TAG_PRINTER, IPP_TAG_ENUM, "printer-state",
		      IPP_PRINTER_IDLE);
	ippDelete (gnome_cups_request_execute (request, NULL, "/admin/", &err));

	if (err != NULL) {
		gnome_cups_error_dialog ((GtkWindow *)glade_xml_get_widget (xml, "add_printer_window"),
					 _("Couldn't add printer"), err);
		g_error_free (err);
		return FALSE;
	}

	return TRUE;
}

static void
driver_page_prepare (GladeXML *xml)
{
#if 0
	/* Blah.  GnomeDruid is stupid.  finish and next are distinct buttons internally
	 * and there is no way to desensitize finish.  We'll need to cheat */
	GtkWidget *druid = glade_xml_get_widget (xml, "add_printer_druid");
	gnome_druid_set_show_finish (GNOME_DRUID (druid), TRUE);
#endif
}

static GtkWidget *
driver_page_next (GladeXML *xml)
{
	GList *existing;
	char  *name, *uri, *ptr;
	unsigned i = 0;
	GCupsPPD const  *ppd  = gcups_driver_selector_get (driver_selector (xml));

	if (ppd == NULL)
		return NULL;

	uri  = get_selected_uri (xml);

	name = g_strdup (ppd->model);
	/* strip out the spaces */
	for (ptr = name ; *ptr ; ptr++)
		if (*ptr == ' ' || *ptr == '/' || *ptr == '(' || *ptr == ')')
			*ptr = '-';

	existing = gnome_cups_get_printers ();
	while (NULL != g_list_find_custom (existing, name, (GCompareFunc)strcasecmp )) {
		g_free (name);
		name = g_strdup_printf ("%s-%d", ppd->model, ++i);
	}
	g_list_foreach (existing, (GFunc)g_free, NULL);
	g_list_free (existing);

	if (add_cups_printer (xml, uri, ppd, name)) {
		GtkWidget *toplevel = glade_xml_get_widget (xml, "add_printer_window");
		char const *args[] = { "-p", NULL };
		args[1] = name;
		gnome_cups_spawn ("gnome-cups-manager", G_N_ELEMENTS (args), args, FALSE, toplevel);
		gtk_widget_destroy (toplevel);
		g_object_unref (xml);
		gtk_main_quit ();
	}

	g_free (uri);
	g_free (name);

	return NULL;
}

/****************************************************************************/

static DruidPageDescription pages[] = {
	{
		"connection_page",
		connection_page_setup,
		NULL,
		connection_page_sensitivity,
		NULL,
		connection_page_next,
	},
	{
		"driver_page",
		driver_page_setup,
		driver_page_prepare,
		driver_page_sensitivity,
		driver_page_back,
		driver_page_next
	},
	{ NULL }
};

static int
delete_event_cb (GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	gtk_main_quit ();
	return FALSE;
}

static void
cancel_cb (GtkWidget *widget, gpointer user_data)
{
	gtk_main_quit ();
}


struct _check_printer_t {
	GMainLoop *main_loop;
	GSList *printers;
	int num_printers;
	const char *uri;
	gboolean found;
};

static void
attributes_changed_cb (GnomeCupsPrinter *printer, gpointer user_data)
{
	struct _check_printer_t *cp = user_data;
	const char *uri;
	
	uri = gnome_cups_printer_get_device_uri (printer);
	if (!strcmp (uri, cp->uri)) {
		g_main_loop_quit (cp->main_loop);
		cp->found = TRUE;
	}
	
	cp->num_printers--;
	if (cp->num_printers == 0)
		g_main_loop_quit (cp->main_loop);
}

static void
check_printer (const char *name, gpointer user_data)
{
	struct _check_printer_t *cp = user_data;
	GnomeCupsPrinter *printer;
	
	printer = gnome_cups_printer_get (name);
	if (!gnome_cups_printer_get_attributes_initialized (printer)) {
		cp->printers = g_slist_prepend (cp->printers, printer);
		g_signal_connect (printer, "attributes-changed",
				  G_CALLBACK (attributes_changed_cb),
				  user_data);
		cp->num_printers++;
	} else {
		cp->num_printers++;
		attributes_changed_cb (printer, user_data);
	}
}

static void
once_cb (void *user_data)
{
	struct _check_printer_t *cp = user_data;
	
	if (cp->num_printers == 0)
		g_main_loop_quit (cp->main_loop);
}

/* NOTE: this must be called before any gnome_cups_init() calls */
static gboolean
check_printer_configured (const char *uri)
{
	struct _check_printer_t *cp;
	gboolean found;
	GSList *n;
	gulong id;
	
	cp = g_new0 (struct _check_printer_t, 1);
	cp->main_loop = g_main_loop_new (NULL, FALSE);
	cp->uri = uri;
	
	id = gnome_cups_printer_new_printer_notify_add_only_once (check_printer, once_cb, cp);
	
	gnome_cups_init (NULL);
	
	g_main_loop_run (cp->main_loop);
	g_main_loop_unref (cp->main_loop);
	
	while (cp->printers) {
		n = cp->printers->next;
		g_object_unref (cp->printers->data);
		g_slist_free_1 (cp->printers);
		cp->printers = n;
	}
	
	found = cp->found;
	g_free (cp);
	
	return found;
}

int
main (int argc, char *argv[])
{
	GtkWidget *window, *druid, *page;
	GnomeProgram *program;
	GOptionContext *context;
	GladeXML *xml;
	gboolean print_version = FALSE;
	char *printer_uri = NULL;

	const GOptionEntry options[] = {
		{ "version", 'v', 0, G_OPTION_ARG_NONE, &print_version,
		  N_("Print version and exit"), NULL },
		{ "printer", 'p', 0, G_OPTION_ARG_STRING, &printer_uri,
		  N_("CUPS Printer URI"), "URI" },
		{ NULL }
	};

	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

        context = g_option_context_new (NULL);
#if GLIB_CHECK_VERSION (2, 12, 0)
	g_option_context_set_translation_domain (context, GETTEXT_PACKAGE);
#endif

	g_option_context_add_main_entries (context, options, GETTEXT_PACKAGE);

	program = gnome_program_init ("gnome-cups-add", VERSION,
				      LIBGNOMEUI_MODULE,
				      argc, argv,
				      GNOME_PROGRAM_STANDARD_PROPERTIES,
				      GNOME_PARAM_GOPTION_CONTEXT, context,
				      NULL);

	g_set_application_name (_("Add a Printer"));

	if (print_version) {
		fprintf (stdout, "gnome-printer-add version %s\n", VERSION);
		exit (0);
	}
	
	if (printer_uri && check_printer_configured (printer_uri)) {
		fprintf (stdout, "printer already configured.\n");
		g_free (printer_uri);
		exit (0);
	}
	
	if (!gnome_cups_can_admin ()) {
		if (gnome_cups_spawn ("gnome-cups-add", argc - 1,
				      (char const **)(argv + 1), TRUE, NULL))
			exit (0);
		else
			exit (1);
	}

	gnome_cups_ui_init (argv[0]);
	
	xml = glade_xml_new (GNOME_CUPS_MANAGER_DATADIR "/gnome-cups-add.glade", "add_printer_window", GETTEXT_PACKAGE);
	
	window = glade_xml_get_widget (xml, "add_printer_window");
	set_window_icon (window, "gnome-dev-printer-new");
	
	g_signal_connect (window, "delete_event", G_CALLBACK (delete_event_cb), NULL);
	g_signal_connect (glade_xml_get_widget (xml, "add_printer_druid"), "cancel", G_CALLBACK (cancel_cb), NULL);
	
	druid_pages_setup (xml, pages);
	if (printer_uri != NULL) {
		gcups_connection_selector_set_uri (connection_selector (xml), printer_uri);
		gcups_connection_selector_queue_guess (connection_selector (xml));
		set_selected_uri (xml, printer_uri);
		page = glade_xml_get_widget (xml, "driver_page");
		druid = glade_xml_get_widget (xml, "add_printer_druid");
		gnome_druid_set_page (GNOME_DRUID (druid), GNOME_DRUID_PAGE (page));
		g_free (printer_uri);
		printer_uri = NULL;
	}

	gtk_widget_show (window);
	
	gtk_main ();
	
	g_object_unref (program);
	
	return 0;
}
